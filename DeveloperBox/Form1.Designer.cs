﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pDFReadingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readInSpellsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readInKitsForSpellsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readInKitFeaturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kitTeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kitTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readInMonstersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pDFReadingToolStripMenuItem,
            this.kitTeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(664, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pDFReadingToolStripMenuItem
            // 
            this.pDFReadingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readInSpellsToolStripMenuItem,
            this.readInKitsForSpellsToolStripMenuItem,
            this.readInKitFeaturesToolStripMenuItem,
            this.readInMonstersToolStripMenuItem});
            this.pDFReadingToolStripMenuItem.Name = "pDFReadingToolStripMenuItem";
            this.pDFReadingToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.pDFReadingToolStripMenuItem.Text = "PDF reading";
            // 
            // readInSpellsToolStripMenuItem
            // 
            this.readInSpellsToolStripMenuItem.Name = "readInSpellsToolStripMenuItem";
            this.readInSpellsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.readInSpellsToolStripMenuItem.Text = "Read in Spells";
            this.readInSpellsToolStripMenuItem.Click += new System.EventHandler(this.readInSpellsToolStripMenuItem_Click);
            // 
            // readInKitsForSpellsToolStripMenuItem
            // 
            this.readInKitsForSpellsToolStripMenuItem.Name = "readInKitsForSpellsToolStripMenuItem";
            this.readInKitsForSpellsToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.readInKitsForSpellsToolStripMenuItem.Text = "Read In Kits for Spells";
            //this.readInKitsForSpellsToolStripMenuItem.Click += new System.EventHandler(this.readInKitsForSpellsToolStripMenuItem_Click);
            // 
            // readInKitFeaturesToolStripMenuItem
            // 
            this.readInKitFeaturesToolStripMenuItem.Name = "readInKitFeaturesToolStripMenuItem";
            this.readInKitFeaturesToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.readInKitFeaturesToolStripMenuItem.Text = "Read in Kit Features";
            this.readInKitFeaturesToolStripMenuItem.Click += new System.EventHandler(this.readInKitFeaturesToolStripMenuItem_Click);
            // 
            // kitTeToolStripMenuItem
            // 
            this.kitTeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kitTestToolStripMenuItem});
            this.kitTeToolStripMenuItem.Name = "kitTeToolStripMenuItem";
            this.kitTeToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.kitTeToolStripMenuItem.Text = "Kit Functions";
            // 
            // kitTestToolStripMenuItem
            // 
            this.kitTestToolStripMenuItem.Name = "kitTestToolStripMenuItem";
            this.kitTestToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.kitTestToolStripMenuItem.Text = "Kit test";
            this.kitTestToolStripMenuItem.Click += new System.EventHandler(this.kitTestToolStripMenuItem_Click);
            // 
            // readInMonstersToolStripMenuItem
            // 
            this.readInMonstersToolStripMenuItem.Name = "readInMonstersToolStripMenuItem";
            this.readInMonstersToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.readInMonstersToolStripMenuItem.Text = "Read in Monsters";
            this.readInMonstersToolStripMenuItem.Click += new System.EventHandler(this.readInMonstersToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 514);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pDFReadingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readInSpellsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readInKitsForSpellsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kitTeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kitTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readInKitFeaturesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readInMonstersToolStripMenuItem;
    }
}

