﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.ObjectModel;

using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text.RegularExpressions;

using DMSTCore;
using DMSTCore.Actions;
using DMSTCore.Core;
using DMSTCore.Spells;
using DMSTCore.Effects;
using DMSTCore.Kits;
using DMSTCore.Monsters;
using LIBS.Toolbox;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            ApplicationData.LoadDatabase();
        }

        private string[] GetLinesOfText(int startPage, int stopPage)
        {
            string filename = @"C:\Users\Greg\Desktop\SRD5E.pdf";

            string pdftext = ReadPdfFile(filename, startPage, stopPage);

           // Console.WriteLine(pdftext);

            int startindex = pdftext.IndexOf("Spell Descriptions") + "Spell Descriptions".Length;
            string substring = pdftext.Substring(startindex);

            substring = substring.Replace("\t", " ");
            substring = substring.Replace("\r", " ");
            substring = substring.Replace(Char.ConvertFromUtf32(160).ToCharArray()[0], ' ');
            substring = substring.Replace(Char.ConvertFromUtf32(173).ToCharArray()[0], '-');
            while (substring.Contains("  ")) substring = substring.Replace("  ", " ");
            while (substring.Contains("--")) substring = substring.Replace("--", "-");

            string textToRemove = "Not for resale. Permission granted to print or photocopy this document for personal use only. System Reference Document 5.0";

            while (substring.Contains(textToRemove))
            {
                int start = substring.IndexOf("Not for resale. Permission granted to print or photocopy this document for personal use only. System Reference Document 5.0", 0);
                substring = substring.Remove(start, textToRemove.Length + 5);
            }

            string[] lines = substring.Split(new string[] { "\n" }, StringSplitOptions.None);

            return lines;
        }

        private string[] GetParagraphsOfText(int startPage, int stopPage)
        {
            List<string> paragraphs = new List<string>();

            string[] lines = GetLinesOfText(startPage, stopPage);

            string paragraph = String.Empty;
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i] == " " && paragraph != String.Empty)
                {
                    paragraphs.Add(paragraph);
                    paragraph = String.Empty;
                }
                else
                {
                    paragraph += lines[i];
                }
            }

            return paragraphs.ToArray();
        }

        //private void readInKitsForSpellsToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    string[] lines = GetLinesOfText(105, 113);

        //    List<string> kits = EnumHelper.EnumToListofStrings<Kit>();
        //    Kit kit = Kit.Bard;
        //    int levelCount = 0;

        //    for (int i = 3; i < lines.Length; i++)
        //    {
        //        Spell spell = ApplicationData.GetSpellBook().GetSpell(lines[i]);

        //        if (spell == null)
        //        {
        //            if (lines[i].Contains((levelCount+1).ToString()))
        //            {
        //                levelCount++;
        //            }
        //            else
        //            {
        //                for (int k=0;k<kits.Count; k++)
        //                {
        //                    if (lines[i].Contains(kits[k]))
        //                    {
        //                        kit = (Kit)k;
        //                        levelCount = 0;
        //                    }
        //                }
        //            }

        //        }
        //        else
        //        {
        //            spell.KitsThatHaveAccess.Add(kit);
        //        }
        //    }

        //    var spells = ApplicationData.GetSpellBook().Spells;

        //    ApplicationData.SaveDatabase();
        //}

        private void readInSpellsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] lines = GetLinesOfText(114,191);

            List<int> indecies = new List<int>();
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("Casting Time"))
                {
                    indecies.Add(i - 2);
                }
            }

            List<string> missed = new List<string>();
            for (int i = 0; i < indecies.Count; i++)
            {
                Spell spell = new Spell();

                if (i == indecies.Count - 1)
                {
                    spell = ReadSpell(lines, indecies[i], lines.Length - 1);
                }
                else
                {
                    spell = ReadSpell(lines, indecies[i], indecies[i + 1] - 1);
                }

                ApplicationData.AddToDatabase(spell);

            }

            var spells = ApplicationData.GetSpellBook().Spells;
        }

        private Spell ReadSpell(string[] lines, int startIndex, int stopIndex)
        {
            Spell spell = new Spell();

            int currentLineIndex = startIndex;

            // Name
            spell.Name = lines[currentLineIndex];

            if (spell.Name == "Magic Missle")
            {
                ;
            }

            // Spell level and school
            currentLineIndex++;
            string[] strings = lines[currentLineIndex].Split(' ');
            if (!Int32.TryParse(lines[currentLineIndex].Substring(0, 1), out spell.SpellLevel))
            {
                spell.SpellLevel = 0;
                spell.MagicSchool = (MagicSchool)Enum.Parse(typeof(MagicSchool), strings[0], true);
            }
            else
            {
                spell.MagicSchool = (MagicSchool)Enum.Parse(typeof(MagicSchool), strings[1], true);
            }

            // Casting time and units
            currentLineIndex++;
            strings = lines[currentLineIndex].Split(' ');
            int castingTime;
            Int32.TryParse(strings[2], out castingTime);
            spell.CastingTime = castingTime;
            if (strings[3] == "bonus")
            {
                spell.CastingTimeUnits = TimeUnit.Action;
                spell.CastingTime = 0;
            }
            else
            {
                spell.CastingTimeUnits = HandlePlural<TimeUnit>(strings[3]);
            }

            // Range and units
            
            currentLineIndex++;
            strings = lines[currentLineIndex].Split(' ');

            while (strings[0] != "Range:")
            {
                currentLineIndex++;
                strings = lines[currentLineIndex].Split(' ');
            }

            int range;
            if (!Int32.TryParse(strings[1], out range))
            {
                spell.Range = 0;
                spell.RangeUnit = HandlePlural<DistanceUnit>(strings[1]);
            }
            else
            {
                spell.Range = range;
                spell.RangeUnit = HandlePlural<DistanceUnit>(strings[2]);
            }

            // Components
            currentLineIndex++;
            strings = lines[currentLineIndex].Split(' ');
            for (int i = 1; i < 4; i++) 
            {
                if (strings[i].Length == 0)
                {
                    break;
                }

                if (strings[i].Substring(0, 1) == "V")
                {
                    spell.Verbal = true;
                    continue;
                }
                else if (strings[i].Substring(0, 1) == "S")
                {
                    spell.Somatic = true;
                }
                else if (strings[i].Substring(0, 1) == "M")
                {
                    spell.Material = true;
                }
            }

            // Duration
            currentLineIndex++;
            strings = lines[currentLineIndex].Split(' ');
            while (strings[0] != "Duration:")
            {
                currentLineIndex++;
                strings = lines[currentLineIndex].Split(' ');
            }

            int duration;
            if (strings.Length > 5)
            {
                spell.ConcentrationRequired = true;
                if (Int32.TryParse(strings[4], out duration))
                {
                    spell.Duration = duration;
                    spell.DurationUnits = HandlePlural<TimeUnit>(strings[5]);
                } 
                else if (strings[4] == "one")
                {
                    spell.Duration = 1;
                    spell.DurationUnits = HandlePlural<TimeUnit>(strings[5]);
                }
                else
                {
                    Int32.TryParse(strings[3], out duration);
                    spell.Duration = duration;

                    if (strings[4] == "triggered")
                    {
                        spell.DurationUnits = TimeUnit.Permanent;
                    }
                    else
                    {
                        spell.DurationUnits = HandlePlural<TimeUnit>(strings[4]);
                    }
                }
            }
            else
            {
                spell.ConcentrationRequired = false;

                if (!Int32.TryParse(strings[1], out duration))
                {
                    spell.Duration = 0;
                }
                else
                {
                    spell.Duration = duration;
                    spell.DurationUnits = HandlePlural<TimeUnit>(strings[2]);
                }
            }

            // Descriptions
            bool storeDescription = true;
            while (currentLineIndex != stopIndex)
            {
                currentLineIndex++;

                string atHigh = "a";
                if (lines[currentLineIndex].Length > 15)
                {
                    atHigh = lines[currentLineIndex].Substring(0, 16);
                }
                
                if (atHigh == " At Higher Level")
                {
                    lines[currentLineIndex] = lines[currentLineIndex].Substring(19);
                    storeDescription = false;
                }

                if (storeDescription)
                {
                    spell.Description += lines[currentLineIndex];
                }
                else
                {
                    spell.AtHigherLevelDescription += lines[currentLineIndex];
                }
            }

            return spell;
        }

        public T HandlePlural<T>(string enumAsString)
        {
            try
            {
                T output = (T)Enum.Parse(typeof(T), enumAsString, true);
                return output;
            }
            catch
            {
                T output = (T)Enum.Parse(typeof(T), enumAsString.Substring(0, enumAsString.Length - 1), true);
                return output;
            }
        }

        public string ReadPdfFile(string fileName, int startPage, int endPage)
        {
            StringBuilder text = new StringBuilder();

            if (File.Exists(fileName))
            {
                PdfReader pdfReader = new PdfReader(fileName);

                for (int page = startPage; page <= endPage; page++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                    string currentText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy);

                    currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
                    text.Append(currentText);
                }
                pdfReader.Close();
            }
            return text.ToString();
        }

        private void kitTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ApplicationData.LoadDatabase();

            var database = ApplicationData.Database;
        }

        private void readInKitFeaturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<KitFeature> features = new List<KitFeature>();

            string[] lines = GetLinesOfText(53, 54);

            KitFeature feature = null;
            bool start = false;
            foreach (string line in lines)
            {
                if (line.Contains("Arcane Recovery") || start)
                {
                    start = true;
                }
                else
                {
                    continue;
                }

                DialogResult result = MessageBox.Show(line, "new feature", MessageBoxButtons.YesNoCancel);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    if (feature != null)
                    {
                        features.Add(feature);
                    }

                    feature = new KitFeature();
                    feature.Name = line;
                }
                else if (result == System.Windows.Forms.DialogResult.No)
                {
                    if (feature == null) { continue; }

                    feature.Description = String.Format("{0}{1}",feature.Description,line);
                }
                else
                {
                    continue;
                }
            }

            features.Add(feature);

            string path = System.IO.Path.Combine(Environment.CurrentDirectory, "temp.xml");
            FileTools.WriteToFile<List<KitFeature>>(features, path);
        }

        private void readInMonstersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] keyWords = new string[] 
            { 
                "Challenge", 
                "Armor Class",
                "Hit Points",
                "Saving Throws",
                "Damage Resistances",
                "Damage Immunities",
                "Condition Immunities",
                "Speed",
                "Skills",
                "Senses",
                "Languages",
                "STR DEX CON INT WIS CHA",
                "Damage Vulnerabilities"
            };

            string[] lines = GetLinesOfText(264, 389);
            //string[] lines = GetLinesOfText(265, 266);

            List<int> indecies = new List<int>();
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("Armor Class"))
                {
                    indecies.Add(i - 2);
                }
            }

            ObservableCollection<Monster> monsters = new ObservableCollection<Monster>();

            for (int monsterIndex = 0; monsterIndex < indecies.Count;monsterIndex++)
            {
                int startIndex = indecies[monsterIndex];

                Monster monster = new Monster();
                monster.Name = lines[startIndex];
                
                int cnt = startIndex;
                string line = lines[cnt];

                while(!line.Contains("Challenge")) 
                {
                    cnt = cnt + 1;
                    line = lines[cnt];

                    // check for additional lines
                    bool keyWordFound = SearchForKeyWord(keyWords, lines[cnt+1]);

                    while (!keyWordFound && !line.Contains("Challenge"))
                    {
                        cnt = cnt + 1;
                        line = String.Format("{0}{1}", line, lines[cnt]);
                        keyWordFound = SearchForKeyWord(keyWords, lines[cnt+1]);
                    }

                    if (line.Contains("Armor Class"))
                    {
                        ParseMonsterArmorClass(monster, line);
                    }
                    else if (line.Contains("Hit Points"))
                    {
                        ParseMonsterHitPoints(monster, line);
                    }
                    else if (line.Contains("Saving Throws"))
                    {
                        ParseMonsterSavingThrows(monster, line);
                    }
                    else if(line.Contains("STR DEX CON INT WIS CHA"))
                    {
                        ParseMonsterStats(monster, line);
                    }
                    else if (line.Contains("Speed"))
                    {
                        ParseMonsterSpeed(monster, line);
                    }
                    else if (line.Contains("Skills"))
                    {
                        ParseMonsterSkills(monster, line);
                    }
                    else if (line.Contains("Senses"))
                    {
                        ParseMonsterSense(monster, line);
                    }
                    else if (line.Contains("Languages"))
                    {
                        ParseMonsterLanguages(monster, line);
                    }
                    else if (line.Contains("Challenge"))
                    {
                        ParseMonsterChallenge(monster, line);
                    }
                    else if (line.Contains("Damage Resistances"))
                    {
                        ParseMonsterDamageResistances(monster, line);
                    }
                    else if (line.Contains("Damage Immunities"))
                    {
                        ParseMonsterDamageImmunities(monster, line);
                    }
                    else if (line.Contains("Condition Immunities"))
                    {
                        ParseMonsterConditionImmunities(monster, line);
                    }
                    else
                    {
                        ;
                    }
                }

                int ln = monsterIndex + 1 == indecies.Count ? lines.Length : indecies[monsterIndex + 1];

                string passiveAbilities = String.Empty;
                string actions = String.Empty;
                string legenedaryActions = String.Empty;
                cnt = cnt + 1;

                bool captureActions = false;
                bool captureLegendaryActions = false;
                for (int i = cnt; i < ln; i++)
                {
                    if (lines[i].Contains("Actions") && captureActions == false)
                    {
                        captureActions = true;
                        continue;
                    }

                    if (lines[i].Contains("Legendary Actions") && captureLegendaryActions == false)
                    {
                        captureLegendaryActions = true;
                        continue;
                    }

                    if (captureLegendaryActions)
                    {
                        legenedaryActions += lines[i];
                    }
                    else if (captureActions)
                    {
                        actions += lines[i];
                    }
                    else
                    {
                        passiveAbilities += lines[i];
                    }
                }

                ParseMonsterPassiveAbilities(monster, passiveAbilities);
                ParseMonsterActions(monster, actions);
                ParseMonsterLegendaryActions(monster, legenedaryActions);

                monsters.Add(monster);
            }

            ApplicationData.LoadDatabase();

            ApplicationData.Database.Monsters = monsters;

            ApplicationData.SaveDatabase();
        }

        private void ParseMonsterPassiveAbilities(Monster monster, string line)
        {
            string[] sentences = line.Split('.');

            PassiveAbility ability = null;
            foreach (string sentence in sentences)
            {
                string[] words = sentence.Split(' ');
                bool newAbility = true;

                foreach (string word in words) 
                {
                    if (string.IsNullOrWhiteSpace(word)) { continue; }
                    if (word.Contains('(') && word.Contains(')')) { continue; }
                    if (word.Contains("and")) { continue; }
                    if (word.Contains("of")) { continue; }

                    char[] letters = word.ToCharArray();
                    if (!char.IsUpper(letters[0]))
                    {
                        newAbility = false;
                        break;
                    }
                }

                if (newAbility)
                {
                    if (ability != null)
                    {
                        monster.PassiveAbilities.Add(ability);
                    }

                    ability = new PassiveAbility();
                    ability.Name = sentence.Trim();
                }
                else
                {
                    ability.Description += sentence + ".";
                }
            }
        }

        private void ParseMonsterLegendaryActions(Monster monster, string line)
        {
            monster.LegendaryActions = ParseMonsterActions(line);
        }

        private void ParseMonsterActions(Monster monster, string line)
        {
            monster.AdditionalActions = ParseMonsterActions(line);
        }

        private ObservableCollection<MyAction> ParseMonsterActions(string line)
        {
            ObservableCollection<MyAction> actions = new ObservableCollection<MyAction>();

            string[] sentences = line.Split('.');

            MyAction action = null;
            bool skipFirstSentence = false;
            foreach (string sentence in sentences)
            {
                string[] words = sentence.Split(' ');
                bool newAbility = true;

                foreach (string word in words)
                {
                    if (string.IsNullOrWhiteSpace(word)) { continue; }
                    if (word.Contains('(')) { break; }
                    if (word.Contains("and")) { continue; }
                    if (word.Contains("of")) { continue; }

                    char[] letters = word.ToCharArray();
                    if (!char.IsUpper(letters[0]))
                    {
                        newAbility = false;
                        break;
                    }
                }

                if (skipFirstSentence)
                {
                    newAbility = false;
                    skipFirstSentence = false;
                }

                if (newAbility)
                {
                    if (action != null)
                    {
                        actions.Add(action);
                    }

                    action = new MyAction();
                    action.Name = sentence.Trim();
                    skipFirstSentence = true;
                }
                else
                {
                    if (action != null)
                    {
                        action.Description += sentence + ".";
                    }
                }
            }

            return actions;
        }

        private bool SearchForKeyWord(string[] keyWords, string line)
        {
            bool keyWordFound = false;
            foreach (string s in keyWords)
            {
                if (line.Contains(s))
                {
                    keyWordFound = true;
                    break;
                }
            }
            return keyWordFound;
        }

        private void ParseMonsterChallenge(Monster monster, string line)
        {
            string[] lines = line.Split(' ');

            monster.ChallengeRating = FractionToDouble(lines[1]);
        }

        private float FractionToDouble(string fraction)
        {
            float result;

            if (float.TryParse(fraction, out result))
            {
                return result;
            }

            string[] split = fraction.Split(new char[] { ' ', '/' });

            if (split.Length == 2 || split.Length == 3)
            {
                int a, b;

                if (int.TryParse(split[0], out a) && int.TryParse(split[1], out b))
                {
                    if (split.Length == 2)
                    {
                        return (float)a / b;
                    }

                    int c;

                    if (int.TryParse(split[2], out c))
                    {
                        return a + (float)b / c;
                    }
                }
            }

            throw new FormatException("Not a valid fraction.");
        }

        private void ParseMonsterDamageResistances(Monster monster, string line)
        {
            int firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            line = line.Trim();

            firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            line = line.Trim();

            char[] delimiters = new[] { ';'};  // List of your delimiters
            string[] lines = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (string l in lines)
            {
                NameDescription res = new NameDescription();
                if (l.Contains("nonmagical"))
                {
                    res.Name = l;

                    monster.DamageResistances.Add(res);
                }
                else
                {
                    string[] parts = l.Split(',');

                    foreach (string part in parts)
                    {
                        if (String.IsNullOrWhiteSpace(l)) { continue; }

                        res = new NameDescription();
                        res.Name = part;

                        monster.DamageResistances.Add(res);
                    }
                }
            }
        }

        private void ParseMonsterDamageImmunities(Monster monster, string line)
        {
            int firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            line = line.Trim();

            firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            line = line.Trim();

            char[] delimiters = new[] { ';' };  // List of your delimiters
            string[] lines = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (string l in lines)
            {
                NameDescription res = new NameDescription();
                if (l.Contains("nonmagical"))
                {
                    res.Name = l;

                    monster.DamageResistances.Add(res);
                }
                else
                {
                    string[] parts = l.Split(',');

                    foreach (string part in parts)
                    {
                        if (String.IsNullOrWhiteSpace(l)) { continue; }

                        res = new NameDescription();
                        res.Name = part;

                        monster.DamageImmunities.Add(res);
                    }
                }
            }
        }

        private void ParseMonsterConditionImmunities(Monster monster, string line)
        {
            int firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            line = line.Trim();

            firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            line = line.Trim();

            char[] delimiters = new[] { ';' };  // List of your delimiters
            string[] lines = line.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            foreach (string l in lines)
            {
                NameDescription res = new NameDescription();
                if (l.Contains("nonmagical"))
                {
                    res.Name = l;

                    monster.DamageResistances.Add(res);
                }
                else
                {
                    string[] parts = l.Split(',');

                    foreach (string part in parts)
                    {
                        if (String.IsNullOrWhiteSpace(l)) { continue; }

                        res = new NameDescription();
                        res.Name = part;

                        monster.ConditionImmunities.Add(res);
                    }
                }
            }
        }

        private void ParseMonsterLanguages(Monster monster, string line)
        {
            int firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            string[] lines = line.Split(',');

            foreach (string l in lines)
            {
                if (String.IsNullOrWhiteSpace(l)) { continue; }

                NameDescription language = new NameDescription();
                language.Name = l;
                monster.Languages.Add(language);
            }
        }

        private void ParseMonsterSense(Monster monster, string line)
        {
            int firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            string[] lines = line.Split(',');

            foreach (string l in lines)
            {
                if (String.IsNullOrWhiteSpace(l)) { continue; }

                Sense sense = new Sense();
                sense.Name = l;
                monster.Senses.Add(sense);
            }
        }

        private void ParseMonsterSkills(Monster monster, string line)
        {
            int firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            string[] lines = line.Split(',');

            for (int j = 0; j < lines.Length;j++ )
            {
                lines[j] = lines[j].Trim();
                string[] parts = lines[j].Split(' ');

                Skill skill = new Skill();
                skill.Name = parts[0].Trim();

                string l = parts[1].Replace(",", "");
                l = l.Replace("+", "");
                l = l.Replace("-", "");

                int bonus = int.Parse(l);
                skill.Bonus = bonus;

                for (int i = 2; i < parts.Length; i++)
                {
                    skill.Description += String.Format("{0} ", parts[i]);
                }

                monster.Skills.Add(skill);
            }
        }

        private void ParseMonsterSavingThrows(Monster monster, string line)
        {
            string[] lines = line.Split(' ');

            int cnt = 2;
            while (cnt < lines.Length-1)
            {
                AttributeEnum att = (AttributeEnum)Enum.Parse(typeof(AttributeEnum), lines[cnt], true);
                monster.SavingThrowsThatReceiveBonus.Add(att);

                if (cnt == 2)
                {
                    string l = lines[3];
                    l = l.Replace(",","");
                    l = l.Replace("+", "");

                    int bonus = int.Parse(l);
                    bonus = bonus - monster.GetModifier(att);
                    monster.SavingThrowBonus = bonus;
                }

                cnt = cnt + 2;
            }
        }

        private void ParseMonsterStats(Monster monster, string line)
        {
            string[] lines = line.Split(' ');
            monster.Stats.Str = int.Parse(lines[8]);
            monster.Stats.Dex = int.Parse(lines[10]);
            monster.Stats.Con = int.Parse(lines[12]);
            monster.Stats.Int = int.Parse(lines[14]);
            monster.Stats.Wis = int.Parse(lines[16]);
            monster.Stats.Cha = int.Parse(lines[18]);
        }

        private void ParseMonsterHitPoints(Monster monster, string line)
        {
            line = Regex.Match(line, @"\(([^)]*)\)").Groups[1].Value;
            string[] lines = line.Split('d');
            int hpDice = int.Parse(lines[0]);
            lines = lines[1].Split('+');

            if (lines.Length == 1)
            {
                lines = lines[0].Split('-');
            }

            int hpDie = int.Parse(lines[0]);

            int hpModifier = 0;
            if (lines.Length > 1)
            {
                hpModifier = int.Parse(lines[1]);
            }

            monster.HpDice = hpDice;
            monster.HpDie = hpDie;
            monster.HpModifier = hpModifier;
        }

        private void ParseMonsterArmorClass(Monster monster, string line)
        {
            string[] lines = line.Split(' ');
            monster.BaseAC = int.Parse(lines[2]);
        }

        private void ParseMonsterSpeed(Monster monster, string line) 
        {
            int firstWhiteSpace = line.IndexOf(' ');
            line = line.Substring(firstWhiteSpace);
            line = Regex.Replace(line, @" ?\(.*?\)", string.Empty);
            string[] lines = line.Split(',');

            for (int i = 0; i < lines.Length; i++)
            {
                string l = lines[i];
                l = l.Trim();
                string[] parts = l.Split(' ');

                FloatDistance floatDistance = new FloatDistance();

                int cnt = parts.Length > 3 ? 3 : parts.Length;

                floatDistance.Value = int.Parse(parts[cnt-2]);
                if (parts[cnt - 1].Contains("ft"))
                {
                    floatDistance.Unit = DistanceUnit.Feet;
                }
                else
                {
                    ;
                }

                if (l.Contains("swim"))
                {
                    monster.SwimSpeed = floatDistance;
                }
                else if (l.Contains("fly"))
                {
                    monster.FlySpeed = floatDistance;
                } 
                else if (l.Contains("burrow"))
                {
                    monster.BurrowSpeed = floatDistance;
                }
                else if (l.Contains("climb"))
                {
                    monster.ClimbSpeed = floatDistance;
                }
                else
                {
                    monster.GroundSpeed = floatDistance;
                }
            }
        }
    }
}
