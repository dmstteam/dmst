﻿#pragma checksum "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "560F349F1F97670E2179C2772DCE9ACB"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Dungeon_Master_Support_Tool_WPF.CharacterSheetControls;
using Dungeon_Master_Support_Tool_WPF.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Dungeon_Master_Support_Tool_WPF.CharacterSheetControls {
    
    
    /// <summary>
    /// CharacterSheetControl
    /// </summary>
    public partial class CharacterSheetControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetHeaderControl characterSheetHeaderControl;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetInformationControl characterSheetInformationControl;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetCombatControl characterSheetCombatControl;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetClassControl characterSheetClassControl;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetProfienciesAndFeats characterSheetProficienciesAndFeats;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetEquipmentControl characterSheetEquipmentControl;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.Controls.SpellBookControl spellBookControl;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetNotesControl characterSheetNotesControl;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetHPControl characterSheetHPControl;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetAttributeControl characterSheetAttributeControl;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Dungeon Master Support Tool WPF;component/charactersheetcontrols/charactersheetc" +
                    "ontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\CharacterSheetControls\CharacterSheetControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.characterSheetHeaderControl = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetHeaderControl)(target));
            return;
            case 2:
            this.characterSheetInformationControl = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetInformationControl)(target));
            return;
            case 3:
            this.characterSheetCombatControl = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetCombatControl)(target));
            return;
            case 4:
            this.characterSheetClassControl = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetClassControl)(target));
            return;
            case 5:
            this.characterSheetProficienciesAndFeats = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetProfienciesAndFeats)(target));
            return;
            case 6:
            this.characterSheetEquipmentControl = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetEquipmentControl)(target));
            return;
            case 7:
            this.spellBookControl = ((Dungeon_Master_Support_Tool_WPF.Controls.SpellBookControl)(target));
            return;
            case 8:
            this.characterSheetNotesControl = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetNotesControl)(target));
            return;
            case 9:
            this.characterSheetHPControl = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetHPControl)(target));
            return;
            case 10:
            this.characterSheetAttributeControl = ((Dungeon_Master_Support_Tool_WPF.CharacterSheetControls.CharacterSheetAttributeControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

