﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Characters;
using DMSTCore.Core;
using DMSTCore.Encounters;

namespace Dungeon_Master_Support_Tool_WPF.EncounterControls
{
    /// <summary>
    /// Interaction logic for TeamControl.xaml
    /// </summary>
    public partial class TeamControl : UserControl
    {
        public TeamControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentEncounter;
        }

        private void AddTeam_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentEncounter.Teams.Add(new Team());
        }

        private void RemoveTeam_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentEncounter.Teams.Remove(ApplicationData.CurrentEncounter.CurrentTeam);
        }

        private void TeamListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.CurrentEncounter.CurrentTeam = (TeamListBox.SelectedItem as Team);
        }

        private void MemberListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.CurrentCombatant = (MemberListBox.SelectedItem as Combatant);
        }
    }
}
