﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore.Encounters;

namespace Dungeon_Master_Support_Tool_WPF.EncounterControls
{
    /// <summary>
    /// Interaction logic for TeamMicroControl.xaml
    /// </summary>
    public partial class TeamMicroControl : UserControl
    {
        public TeamMicroControl()
        {
            InitializeComponent();
        }

        private void Grid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            MenuItem MenuItem1 = MenuItemHelper.CreateNPCMenuItem(npcItem_Click, "Add Member");

            ContextMenu = new System.Windows.Controls.ContextMenu();
            ContextMenu = new System.Windows.Controls.ContextMenu();
            ContextMenu.Items.Clear();
            ContextMenu.Items.Add(MenuItem1);
        }

        void npcItem_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as Team).AddMember((sender as NPCMenuItem).Character);
        }
    }
}
