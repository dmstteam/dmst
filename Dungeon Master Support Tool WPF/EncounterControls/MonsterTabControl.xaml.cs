﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Core;
using DMSTCore.Monsters;

namespace Dungeon_Master_Support_Tool_WPF.EncounterControls
{
    /// <summary>
    /// Interaction logic for MonsterTabControl.xaml
    /// </summary>
    public partial class MonsterTabControl : UserControl
    {
        public MonsterTabControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.Database;
        }

        private void MonsterListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentMonster = MonsterListBox.SelectedItem as Monster;
        }

        private void Grid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ContextMenu = new System.Windows.Controls.ContextMenu();
            ContextMenu.Items.Clear();
            MenuItem addTeamItem = MenuItemHelper.CreateTeamMenuItem(teamItem_Click, "Add To Team");
            ContextMenu.Items.Add(addTeamItem);
        }

        private void teamItem_Click(object sender, RoutedEventArgs e)
        {
            Monster monster = (MonsterListBox.SelectedItem as Monster).Clone() as Monster;

            monster.Name += String.Format(" 1");
            monster.MaxHP = Dice.Roll(monster.HpDie, monster.HpDice, monster.HpModifier);
            monster.HP = monster.MaxHP;

            (sender as TeamMenuItem).Team.AddMonster(monster);
        }
    }
}
