﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

using DMSTCore;

namespace Dungeon_Master_Support_Tool_WPF.EncounterControls
{
    /// <summary>
    /// Interaction logic for TurnControl.xaml
    /// </summary>
    public partial class TurnControl : UserControl
    {
        public TurnControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentRound;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ApplicationData.CurrentRound.Turns.Count == 0)
            {
                ApplicationData.CurrentRound.StartNewRound();
                EndTurnButton.Content = "End Turn";
            }
            else
            {
                ApplicationData.CurrentRound.NextTurn();
            }

            if (ApplicationData.CurrentRound.Turns.Count == 0)
            {
                EndTurnButton.Content = "New Round";
            }
        }
    }

    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return Visibility.Hidden;
            }

            if (value is bool)
            {
                if ((bool)value)
                {
                    return Visibility.Visible;
                }
            }

            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType,
                          object parameter, CultureInfo culture)
        {
            return true;
        }
    }
}
