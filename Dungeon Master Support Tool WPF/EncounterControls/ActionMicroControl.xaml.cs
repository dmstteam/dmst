﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Actions;

namespace Dungeon_Master_Support_Tool_WPF.EncounterControls
{
    /// <summary>
    /// Interaction logic for ActionMicroControl.xaml
    /// </summary>
    public partial class ActionMicroControl : UserControl
    {
        public ActionMicroControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((MyAction)this.DataContext).Execute(ApplicationData.CurrentEncounter.CurrentTurn.Combatant);
        }
    }
}
