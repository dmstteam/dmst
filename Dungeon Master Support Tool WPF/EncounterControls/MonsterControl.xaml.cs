﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Actions;

namespace Dungeon_Master_Support_Tool_WPF.EncounterControls
{
    /// <summary>
    /// Interaction logic for MonsterControl.xaml
    /// </summary>
    public partial class MonsterControl : UserControl
    {
        public MonsterControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.Database;
        }

        private void PassiveAbilityListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentMonster.CurrentPassiveAbility = (sender as ListBox).SelectedItem as NameDescription;
        }

        private void ActionsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentMonster.CurrentAction = (sender as ListBox).SelectedItem as MyAction;
        }

        private void LegendaryActionsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentMonster.CurrentLegendaryAction = (sender as ListBox).SelectedItem as MyAction;
        }
    }
}
