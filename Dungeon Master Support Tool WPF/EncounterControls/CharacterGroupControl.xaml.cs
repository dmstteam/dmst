﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Characters;

namespace Dungeon_Master_Support_Tool_WPF.EncounterControls
{
    /// <summary>
    /// Interaction logic for CharacterGroupControl.xaml
    /// </summary>
    public partial class CharacterGroupControl : UserControl
    {
        public CharacterGroupControl()
        {
            InitializeComponent();
        }

        private void AddCharacter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void RemoveCharacterFromGroup_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void ListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCombatant = CharacterListBox.SelectedItem as Character;
        }
    }
}
