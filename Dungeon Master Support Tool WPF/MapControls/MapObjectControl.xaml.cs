﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Maps;
using Dungeon_Master_Support_Tool_WPF.Controls;

namespace Dungeon_Master_Support_Tool_WPF.MapControls
{
    /// <summary>
    /// Interaction logic for CityIconControl.xaml
    /// </summary>
    public partial class MapObjectControl : DraggableControl
    {
        public MapObjectControl()
        {
            InitializeComponent();
        }

        public event EventHandler ImageDoubleClicked;
        public event EventHandler DeleteMapIcon;

        protected void OnImageDoubleClicked()
        {
            ImageDoubleClicked?.Invoke(this, new EventArgs());
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                OnImageDoubleClicked();
            }

            base.OnMouseLeftButtonDown(e);
        }

        protected override void OnMouseRightButtonUp(MouseButtonEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Would you like to delete this map and icon?", "Delete Map", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                DeleteMapIcon?.Invoke(this, e);
            }

            base.OnMouseRightButtonUp(e);
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            MapObject mapObject = ApplicationData.CurrentCampaign.CurrentMap.FindMapObject((this.DataContext as MapObject).Key);

            var transform = RenderTransform as TranslateTransform;
            if (mapObject != null && transform != null)
            {
                mapObject.X = mapObject.X + transform.X;
                mapObject.Y = mapObject.Y + transform.Y;

                Canvas.SetLeft(this, mapObject.X);
                Canvas.SetTop(this, mapObject.Y);
            }

            base.OnMouseLeftButtonUp(e);
        }
    }
}
