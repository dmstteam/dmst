﻿using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Ink;
using DMSTCore;
using DMSTCore.Maps;

namespace Dungeon_Master_Support_Tool_WPF.MapControls
{
    /// <summary>
    /// Interaction logic for MapControl.xaml
    /// </summary>
    public partial class MapControl : UserControl
    {
        public MapControl()
        {
            this.AllowDrop = true;
            InitializeComponent();

            CreateGridOfColor();
            LoadInkFile();
            DrawMapObjects();
            ApplicationData.System.SaveCampaignRequest += SaveCurrentInkEvent;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
            {
                SetNewMap((Map)(sender as ComboBox).SelectedItem);
            }

            DrawMapObjects();
        }

        private void SetNewMap(Map map)
        {
            SaveInkFile();
            ApplicationData.CurrentCampaign.CurrentMap = map;
            LoadInkFile();
        }

        private void SaveCurrentInkEvent(Object sender, EventArgs e)
        {
            SaveInkFile();
        }

        private void MapLevelUpButton_Click(object sender, RoutedEventArgs e)
        {
            string key = ApplicationData.CurrentCampaign.CurrentMap.ParentMapKey;

            if (String.IsNullOrEmpty(key)) { return; }

            Map map = ApplicationData.CurrentCampaign.WorldMap.FindMapInSubMaps(key);

            SetNewMap(map);

            DrawMapObjects();
        }

        private void DeleteMapButton_Click(object sender, RoutedEventArgs e)
        {
            string parentKey = ApplicationData.CurrentCampaign.CurrentMap.ParentMapKey;

            if (String.IsNullOrEmpty(parentKey)) { return; }

            string key = ApplicationData.CurrentCampaign.CurrentMap.Key;
            ApplicationData.CurrentCampaign.WorldMap.DeleteSubMap(key);

            Map map = ApplicationData.CurrentCampaign.WorldMap.FindMapInSubMaps(parentKey);
            for (int i=0;i<map.MapObjects.Count;i++)
            {
                if (map.MapObjects[i].MapKey == key)
                {
                    map.MapObjects.RemoveAt(i);
                    break;
                }
            }

            SetNewMap(map);
            DrawMapObjects();
        }

        private void SaveInkFile()
        {
            if (ApplicationData.CurrentCampaign == null ||
                ApplicationData.CurrentCampaign.CurrentMap == null) { return; }

            string filename = String.Format("{0}.isf", ApplicationData.CurrentCampaign.CurrentMap.Key);
            string filepath = System.IO.Path.Combine(ApplicationData.System.MapStrokeFileSaveLocation, filename);

            var fs = new FileStream(filepath, FileMode.Create);
            inkCanvasControl.Strokes.Save(fs);
            fs.Close();
        }

        private void LoadInkFile()
        {
            string filename = String.Format("{0}.isf", ApplicationData.CurrentCampaign.CurrentMap.Key);
            string filepath = System.IO.Path.Combine(ApplicationData.System.MapStrokeFileSaveLocation, filename);

            if (!File.Exists(filepath))
            {
                inkCanvasControl.Strokes.Clear();
                return;
            }

            var fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
            StrokeCollection strokes = new StrokeCollection(fs);
            inkCanvasControl.Strokes = strokes;
            fs.Close();
        }

        private void AddMapObject(string initialName, MapObjectType type)
        {
            MapObject mapObject = new MapObject();
            
            MessageBoxResult result = MessageBox.Show("Would you like to add a map to this icon?", "Add Map", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                ApplicationData.CurrentCampaign.CurrentMap.AddSubMap(initialName, String.Format("Map of {0}",initialName));
                mapObject.MapKey = ApplicationData.CurrentCampaign.CurrentMap.SubMaps.Last().Key;
            }

            mapObject.Name = initialName;
            mapObject.Type = type;
            mapObject.X = 0;
            mapObject.Y = 0;
            ApplicationData.CurrentCampaign.CurrentMap.MapObjects.Add(mapObject);

            DrawMapObject(mapObject);
        }

        private void DrawMapObjects()
        {
            homeCanvas.Children.Clear();
            foreach (MapObject mapObject in ApplicationData.CurrentCampaign.CurrentMap.MapObjects)
            {
                DrawMapObject(mapObject);
            }
        }
        
        private void DrawMapObject(MapObject mapObject)
        {
            MapObjectControl mapObjectControl = new MapObjectControl();
            mapObjectControl.DataContext = mapObject;
            mapObjectControl.SetMinMaxBoundaries(0, ApplicationData.CurrentCampaign.CurrentMap.Width, 0, ApplicationData.CurrentCampaign.CurrentMap.Height);
            mapObjectControl.ImageDoubleClicked += mapObjectControl_ImageDoubleClicked;
            mapObjectControl.DeleteMapIcon += MapObjectControl_DeleteMapIcon;
            homeCanvas.Children.Add(mapObjectControl);
            Canvas.SetLeft(mapObjectControl, mapObject.X);
            Canvas.SetTop(mapObjectControl, mapObject.Y);
        }

        private void MapObjectControl_DeleteMapIcon(object sender, EventArgs e)
        {
            MapObject mapObject = ((sender as MapObjectControl).DataContext as MapObject);
            ApplicationData.CurrentCampaign.CurrentMap.DeleteSubMap(mapObject.MapKey);
            ApplicationData.CurrentCampaign.CurrentMap.MapObjects.Remove(mapObject);
            DrawMapObjects();
        }

        void mapObjectControl_ImageDoubleClicked(object sender, EventArgs e)
        {
            Map map = ApplicationData.CurrentCampaign.CurrentMap.FindMapInSubMaps(((sender as MapObjectControl).DataContext as MapObject).MapKey);

            SetNewMap(map);
            DrawMapObjects();
        }

        void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as MapObjectTextBox).MapOject.Name = (sender as MapObjectTextBox).Text;
        }

        private void CreateGridOfColor()
        {
            PropertyInfo[] props = typeof(Brushes).GetProperties(BindingFlags.Public |
                                                  BindingFlags.Static);
            // Create individual items
            foreach (PropertyInfo p in props)
            {
                Button b = new Button();
                b.Background = (SolidColorBrush)p.GetValue(null, null);
                b.Foreground = Brushes.Transparent;
                b.BorderBrush = Brushes.Transparent;
                b.Width = 16;
                b.Height = 16;
                b.Click += b_Click;
                this.UGColors.Children.Add(b);
            }
        }

        void b_Click(object sender, RoutedEventArgs e)
        {
            CurrentColorBorder.Background = (sender as Button).Background;
            inkCanvasControl.DefaultDrawingAttributes = CurrentDrawingAttributes;
            CurrentColorBorder.InvalidateVisual();
        }

        public DrawingAttributes CurrentDrawingAttributes
        {
            get
            {
                DrawingAttributes drawattr = new DrawingAttributes();
                drawattr.Width = _size;
                drawattr.Height = _size;

                if (CurrentColorBorder != null && CurrentColorBorder.Background != null)
                {
                    drawattr.Color = Color.FromArgb((CurrentColorBorder.Background as SolidColorBrush).Color.A,
                                                                 (CurrentColorBorder.Background as SolidColorBrush).Color.R,
                                                                 (CurrentColorBorder.Background as SolidColorBrush).Color.G,
                                                                 (CurrentColorBorder.Background as SolidColorBrush).Color.B);
                }
                else
                {
                    drawattr.Color = Colors.Black;
                }

                return drawattr;
            }
        }

        private int _size = 4;

        private void DrawRectangle()
        {
            StylusPointCollection points = new StylusPointCollection();
            points.Add(new StylusPoint(50, 50));
            points.Add(new StylusPoint(100, 70));
            inkCanvasControl.Strokes.Add(new RectangleStroke(points));
        }

        private void SizeButton_Click(object sender, RoutedEventArgs e)
        {
            _size = (int)(sender as Button).FontSize;
            inkCanvasControl.DefaultDrawingAttributes = CurrentDrawingAttributes;
        }

        private void homeCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DrawMapObjects();
        }

        private void cityButton_Click(object sender, RoutedEventArgs e)
        {
            AddMapObject("City", MapObjectType.City);
        }

        private void churchButton_Click(object sender, RoutedEventArgs e)
        {
            AddMapObject("Church", MapObjectType.Church);
        }

        private void dungeonButton_Click(object sender, RoutedEventArgs e)
        {
            AddMapObject("Dungeon", MapObjectType.Dungeon);
        }

        private void theaterButton_Click(object sender, RoutedEventArgs e)
        {
            AddMapObject("Theather", MapObjectType.Theater);
        }

        private void alchemistButton_Click(object sender, RoutedEventArgs e)
        {
            AddMapObject("Alchemist", MapObjectType.Alchemist);
        }

        private void beerButton_Click(object sender, RoutedEventArgs e)
        {
            AddMapObject("Tavern", MapObjectType.Tavern);
        }

        private void libraryButton_Click(object sender, RoutedEventArgs e)
        {
            AddMapObject("Library", MapObjectType.Library);
        }

        private void castleButton_Click(object sender, RoutedEventArgs e)
        {
            AddMapObject("Castle", MapObjectType.Castle);
        }
    }

    public class MapObjectTextBox : TextBox
    {
        public MapObject MapOject;
    }
}
