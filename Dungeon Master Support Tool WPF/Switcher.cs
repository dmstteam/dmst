﻿using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Generic;
using DMSTCore;
using Dungeon_Master_Support_Tool_WPF.Pages;
using Dungeon_Master_Support_Tool_WPF.XAML;

namespace Dungeon_Master_Support_Tool_WPF
{
    public static class Switcher
    {
        static List<Page> history = new List<Page>();

        public static MainWindow pageSwitcher;

        public static Frame currentFrame;

        public static void Switch(Page page)
        {
            Navigate(page);

            history.Add(page);
        }

        public static void Back()
        {
            if (history.Count <= 1) { return; }

            history.RemoveAt(history.Count - 1);
            Page page = history[history.Count - 1];

            Navigate(page);
        }

        private static void Navigate(Page page)
        {
            pageSwitcher.Navigate(page);

            if (page is CampaignPage)
            {
                ApplicationData.System.CurrentPage = "Campaign";
            }
            else if (page is QuestPage)
            {
                ApplicationData.System.CurrentPage = "Quest";
            }
            else if (page is EncounterPage)
            {
                ApplicationData.System.CurrentPage = "Encounter";
            }
            else if (page is EncouterCreationPage)
            {
                ApplicationData.System.CurrentPage = "Encounter Editor";
            }
        }
    }
}
