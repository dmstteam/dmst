﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Characters;
using DMSTCore.Campaigns;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF
{
    /// <summary>
    /// Interaction logic for CampaignPage.xaml
    /// </summary>
    public partial class CampaignPage : Page
    {
        public CampaignPage()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentCampaign;
        }

        private void DeleteCharacterButton_Click(object sender, RoutedEventArgs e)
        {
            CharacterSelectionControl.DeleteSelectedCharacter();
        }

        private void LevelDownButton_Click(object sender, RoutedEventArgs e)
        {
            CharacterSelectionControl.LevelDownSelectedCharacter();
        }

        private void LevelUpButton_Click(object sender, RoutedEventArgs e)
        {
            CharacterSelectionControl.LevelUpSelectedCharacter();
        }

        private void AddCharacterButton_Click(object sender, RoutedEventArgs e)
        {
            ApplicationData.CurrentCombatant = new Character();
            ApplicationData.CurrentCampaign.PCs.Add(ApplicationData.CurrentCombatant as Character);
            CharacterCreationWindow CharacterCreationWindow = new CharacterCreationWindow();
            CharacterCreationWindow.ShowDialog();
        }

        private void EditCharacterButton_Click(object sender, RoutedEventArgs e)
        {
            CharacterCreationWindow CharacterCreationWindow = new CharacterCreationWindow();
            CharacterCreationWindow.ShowDialog();
        }
    }
}
