﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Actions;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF
{
    public static class CommunicationManagerWPF
    {
        public static void SetUpEvents()
        {
            ApplicationData.Communication.YesNoRequestHandler += Communication_YesNoRequestHandler;
            ApplicationData.Communication.RollRequestHandler += Communication_RollRequestHandler;
            ApplicationData.Communication.RequestTargetHandler += Communication_RequestTargetHandler;
            ApplicationData.Communication.AttackRollRequestHandler += Communication_AttackRollRequestHandler;
        }

        static void Communication_AttackRollRequestHandler(AttackParameters attackParameters)
        {
            
        }

        static void Communication_RequestTargetHandler(object sender, RequestTargetEventArgs e)
        {
            RequestTargetWindow window = new RequestTargetWindow();
            window.DataContext = e;
            window.ShowDialog();
        }

        static void Communication_RollRequestHandler(Roll roll)
        {
            throw new NotImplementedException();
        }

        static bool Communication_YesNoRequestHandler(string text, string caption)
        {
            MessageBoxResult result = MessageBox.Show(text, caption, MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                return true;
            }

            return false;
        }
    }
}
