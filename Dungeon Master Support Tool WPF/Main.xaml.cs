﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;

using DMSTCore;
using DMSTCore.Campaigns;
using DMSTCore.Items;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF.XAML
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Timer _autoSaveTimer;
        Timer _checkClosableTimer;

        public MainWindow()
        {
            InitializeComponent();
            Switcher.pageSwitcher = this;

            ApplicationData.LoadDatabase();
            _autoSaveTimer = new Timer();
            _autoSaveTimer.Elapsed += _autoSaveTimer_Elapsed;
            _autoSaveTimer.Interval = 60000;
            _autoSaveTimer.Enabled = true;

            _checkClosableTimer = new Timer();
            _checkClosableTimer.Interval = 250;
            _checkClosableTimer.Enabled = false;
            _checkClosableTimer.Elapsed += _checkClosableTimer_Elapsed;

            campaignSelectionControl.DataContext = ApplicationData.System;

            this.PreviewKeyDown += MainWindow_PreviewKeyDown;

            CommunicationManagerWPF.SetUpEvents();
        }

        void MainWindow_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                EscapeMenuWindow escapeMenuWindow = new EscapeMenuWindow();
                escapeMenuWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
                escapeMenuWindow.ShowDialog();
            }
        }

        void _autoSaveTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //ApplicationData.System.SaveCampaign();
        }

        public void Navigate(Page page)
        {
            this.Content = page;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void NewCampaignButton_Click(object sender, EventArgs e)
        {
            ApplicationData.CurrentCampaign = new Campaign();

            CampaignPage campaignPage = new CampaignPage();
            campaignPage.DataContext = ApplicationData.CurrentCampaign;

            ApplicationData.System.SaveCampaign();

            Switcher.Switch(campaignPage);
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void KitEditorButton_Click(object sender, RoutedEventArgs e)
        {
            KitEditorWindow window = new KitEditorWindow();
            window.ShowDialog();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ApplicationData.System.IsSaving)
            {
                _checkClosableTimer.Start();
                e.Cancel = true;
            }
        }

        void _checkClosableTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _checkClosableTimer.Stop();
            if (ApplicationData.System.IsSaving)
            {
                _checkClosableTimer.Start();
            }
            else
            {
                Application.Current.Shutdown();
            }
        }
    }
}
