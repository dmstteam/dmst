﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Dungeon_Master_Support_Tool_WPF.Controls;

namespace Dungeon_Master_Support_Tool_WPF.QuestControls
{
    /// <summary>
    /// Interaction logic for StoryBoardObject.xaml
    /// </summary>
    public partial class StoryBoardObject : UserControl
    {
        public StoryBoardObject()
        {
            InitializeComponent();
        }
    }
}
