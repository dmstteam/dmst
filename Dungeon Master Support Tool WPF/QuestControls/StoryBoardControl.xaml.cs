﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Dungeon_Master_Support_Tool_WPF.Controls;
using DMSTCore.Campaigns;
using DMSTCore;
using Dungeon_Master_Support_Tool_WPF.Pages;

namespace Dungeon_Master_Support_Tool_WPF.QuestControls
{
    /// <summary>
    /// Interaction logic for StoryBoardControl.xaml
    /// </summary>
    public partial class StoryBoardControl : UserControl
    {
        public StoryBoardControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentQuest;
        }

        public void UpdateStoryBoard(StoryBoardEvent storyBoardEvent)
        {
            homeCanvas.Children.Clear();

            double arc = (2.0f*Math.PI) / storyBoardEvent.NextStoryBoardEvents.Count;
            double radius = 150;

            for (int i = 0; i < storyBoardEvent.NextStoryBoardEvents.Count; i++ )
            {
                StoryBoardEventButton button = new StoryBoardEventButton();
                button.StoryBoardEvent = storyBoardEvent.NextStoryBoardEvents[i];
                button.Click += button_Click;
                button.Content = storyBoardEvent.NextStoryBoardEvents[i].Name;
                button.FontSize = 18;
                button.Background = Brushes.Moccasin;
                Size size = MeasureButton(button);
                double y = homeCanvas.ActualHeight / 2 - radius * Math.Cos(i * arc) - size.Height / 2;
                double x = homeCanvas.ActualWidth / 2 + radius * Math.Sin(i * arc) - size.Width / 2;
                homeCanvas.Children.Add(button);
                Canvas.SetLeft(button, x);
                Canvas.SetTop(button, y);
            }

            ApplicationData.CurrentQuest.CurrentScene.CurrentStoryBoardEvent = storyBoardEvent;
        }

        void button_Click(object sender, RoutedEventArgs e)
        {
            UpdateStoryBoard((sender as StoryBoardEventButton).StoryBoardEvent);
        }

        private Size MeasureButton(Button button)
        {
            var formattedText = new FormattedText(
                button.Content.ToString(),
                CultureInfo.CurrentUICulture,
                FlowDirection.LeftToRight,
                new Typeface(button.FontFamily, button.FontStyle, button.FontWeight, button.FontStretch),
                button.FontSize,
                Brushes.Black);

            return new Size(formattedText.Width, formattedText.Height);
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (ApplicationData.CurrentQuest.CurrentScene == null) { return; }
            if (ApplicationData.CurrentQuest.CurrentScene.CurrentStoryBoardEvent == null) { return; }

            string key = ApplicationData.CurrentQuest.CurrentScene.CurrentStoryBoardEvent.PreviousStoryBoardEventKey;
            StoryBoardEvent sbEvent = ApplicationData.CurrentQuest.CurrentScene.StoryBoard.GetStoryBoardEvent(key);

            if (sbEvent != null)
            {
                UpdateStoryBoard(sbEvent);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            ApplicationData.CurrentQuest.CurrentScene.CurrentStoryBoardEvent.AddStoryBoardEvent();
            UpdateStoryBoard(ApplicationData.CurrentQuest.CurrentScene.CurrentStoryBoardEvent);
        }

        private void sceneSelectionListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Scene scene = sceneSelectionListBox.SelectedItem as Scene;
            ApplicationData.CurrentQuest.CurrentScene = scene;

            if (scene != null)
            {
                UpdateStoryBoard(scene.StoryBoard);
            }
        }

        private void addSceneButton_Click(object sender, RoutedEventArgs e)
        {
            ApplicationData.CurrentQuest.Scenes.Add(new Scene());
        }

        private void deleteSceneButton_Click(object sender, RoutedEventArgs e)
        {
            Scene scene = sceneSelectionListBox.SelectedItem as Scene;
            if (scene != null)
            {
                ApplicationData.CurrentQuest.Scenes.Remove(scene);
                ApplicationData.CurrentQuest.CurrentScene = null;
            }
        }

        private void encounterButton_Click(object sender, RoutedEventArgs e)
        {
            if (ApplicationData.CurrentQuest.CurrentScene.CurrentStoryBoardEvent.Encounter == null)
            {
                ApplicationData.CurrentQuest.CurrentScene.CurrentStoryBoardEvent.Encounter = ApplicationData.CurrentEncounter;
            }
            else
            {
                ApplicationData.CurrentEncounter = ApplicationData.CurrentQuest.CurrentScene.CurrentStoryBoardEvent.Encounter;
                Switcher.Switch(new EncounterPage());
            }
        }
    }

    public class StoryBoardEventButton : Button
    {
        public StoryBoardEvent StoryBoardEvent;
    }
}
