﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore;

namespace Dungeon_Master_Support_Tool_WPF.QuestControls
{
    /// <summary>
    /// Interaction logic for ClockCalenderControl.xaml
    /// </summary>
    public partial class ClockCalenderControl : UserControl
    {
        public ClockCalenderControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentCampaign;
        }

        private void PauseImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCampaign.CurrentTime.Pause();
        }

        private void PlayImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCampaign.CurrentTime.Start();
        }
    }
}
