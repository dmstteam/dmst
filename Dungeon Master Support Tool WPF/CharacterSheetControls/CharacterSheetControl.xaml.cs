﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Characters;

namespace Dungeon_Master_Support_Tool_WPF.CharacterSheetControls
{
    /// <summary>
    /// Interaction logic for CharacterSheetControl.xaml
    /// </summary>
    public partial class CharacterSheetControl : UserControl
    {
        public CharacterSheetControl()
        {
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }

            this.DataContext = ApplicationData.CurrentCombatant;

            spellBookControl.DataContext = (this.DataContext as Combatant).SpellBook;
            spellBookControl.SetSpellBook((this.DataContext as Combatant).SpellBook);
            spellBookControl.SetCombatant(this.DataContext as Combatant);
        }
    }
}
