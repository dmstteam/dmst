﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dungeon_Master_Support_Tool_WPF.CharacterSheetControls
{
    /// <summary>
    /// Interaction logic for CharacterSheetProfienciesAndFeats.xaml
    /// </summary>
    public partial class CharacterSheetProfienciesAndFeats : UserControl
    {
        public CharacterSheetProfienciesAndFeats()
        {
            InitializeComponent();

            proficiencyInformationControl.DataContext = null;
            featInformationControl.DataContext = null;
        }

        private void proficiencyListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            proficiencyInformationControl.DataContext = ProficienyListBox.SelectedItem;            
        }

        private void featListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            featInformationControl.DataContext = featListBox.SelectedItem;
        }
    }
}
