﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore.Characters;
using DMSTCore.Items;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF.CharacterSheetControls
{
    /// <summary>
    /// Interaction logic for CharacterSheetEquipmentControl.xaml
    /// </summary>
    public partial class CharacterSheetEquipmentControl : UserControl
    {
        public CharacterSheetEquipmentControl()
        {
            InitializeComponent();

            itemInformationControl.DataContext = null;
        }

        private void addItemButton_Click(object sender, RoutedEventArgs e)
        {
            ItemViewWindow itemViewWindow = new ItemViewWindow();
            itemViewWindow.ShowDialog();

            (this.DataContext as Character).Inventory.AddEquipment(itemViewWindow.SelectedItem);
        }

        private void ItemSelectionListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            itemInformationControl.DataContext = itemSelectionListBox.SelectedItem;
        }

        private void deleteItemButton_Click(object sender, RoutedEventArgs e)
        {
            Item item = itemInformationControl.DataContext as Item;
            (this.DataContext as Character).Inventory.RemoveEquipment(item);
        }

        private void equipItemButton_Click(object sender, RoutedEventArgs e)
        {
            EquippableItem item = itemInformationControl.DataContext as EquippableItem;

            if (item != null)
            {
                (this.DataContext as Character).EquipItem(item);
            }
        }

        private void unEquipItemButton_Click(object sender, RoutedEventArgs e)
        {
            EquippableItem item = itemInformationControl.DataContext as EquippableItem;

            if (item != null)
            {
                (this.DataContext as Character).UnEquipItem(item);
            }
        }
    }
}
