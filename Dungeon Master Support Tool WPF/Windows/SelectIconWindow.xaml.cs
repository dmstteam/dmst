﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMSTCore;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for SelectIconWindow.xaml
    /// </summary>
    public partial class SelectIconWindow : Window
    {
        public SelectIconWindow()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.Database;
        }

        public void SetToCharacterIcons()
        {
            iconListBox.ItemsSource = ApplicationData.Database.CharacterIcons;
        }

        private void iconListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (iconListBox.SelectedItem != null)
            {
                SelectedIconPath = (iconListBox.SelectedItem as Icon).IconPath;
                this.Close();
            }
        }

        public string SelectedIconPath;
    }
}
