﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Characters;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for CharacterCreationWindow.xaml
    /// </summary>
    public partial class CharacterCreationWindow : Window
    {
        public CharacterCreationWindow()
        {
            InitializeComponent();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                //Code that throws the exception
            }

            Height = SystemParameters.PrimaryScreenHeight * 0.9;
            double aspectRation = 1200.0 / 1000.0;
            Width = Height * aspectRation;

            this.DataContext = ApplicationData.CurrentCombatant;

            raceComboBox.ItemsSource = ApplicationData.Database.Races;
            kitComboBox.ItemsSource = ApplicationData.Database.KitTemplates;
            genderComboBox.ItemsSource = Enum.GetValues(typeof(Gender)).Cast<Gender>();
            alignmentComboBox.ItemsSource = Enum.GetValues(typeof(Alignment)).Cast<Alignment>();
        }

        private void Roll3Button_Click(object sender, RoutedEventArgs e)
        {
            List<int> rolls = Dice.Roll3Stats();
            ApplicationData.CurrentCombatant.SetStats(rolls);
        }

        private void Roll4Drop1Button_Click(object sender, RoutedEventArgs e)
        {
            List<int> rolls = Dice.Roll4Drop1Stats();
            ApplicationData.CurrentCombatant.SetStats(rolls);
        }

        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LevelUpButton_Click(object sender, RoutedEventArgs e)
        {
            (ApplicationData.CurrentCombatant as Character).LevelUp();
        }

        private void LevelDownButton_Click(object sender, RoutedEventArgs e)
        {
            (ApplicationData.CurrentCombatant as Character).LevelDown();
        }
    }
}
