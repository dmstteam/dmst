﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

using DMSTCore;
using DMSTCore.Characters;
using DMSTCore.Effects;
using DMSTCore.Kits;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for KitEditorWindow.xaml
    /// </summary>
    public partial class KitEditorWindow : Window
    {
        public KitEditorWindow()
        {
            InitializeComponent();

            Closing += KitEditorWindow_Closing;

            this.DataContext = ApplicationData.Database;
        }

        void KitEditorWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ApplicationData.SaveDatabase();
            ApplicationData.LoadDatabase();
        }

        private void NewClassButton_Click(object sender, RoutedEventArgs e)
        {
            KitTemplate kitTemplate = new KitTemplate();
            ApplicationData.Database.KitTemplates.Add(kitTemplate);
        }

        private void KitTemplateListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentKitTemplate = KitTemplateListBox.SelectedItem as KitTemplate;
        }

        private void databaseKitFeatures_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentKitFeature = databaseKitFeatures.SelectedItem as KitFeature;
        }

        private void addFeatureButton_Click(object sender, RoutedEventArgs e)
        {
            KeyLevelPair pair = new KeyLevelPair();
            pair.Key = ApplicationData.Database.CurrentKitFeature.Key;

            int level;
            if (Int32.TryParse(levelTextBox.Text, out level))
            {
                pair.GainedAtLevel = level;
                ApplicationData.Database.CurrentKitTemplate.AddKeyValuePair(pair);
            }
            else
            {
                return;
            }
        }

        private void removeFeatureButton_Click(object sender, RoutedEventArgs e)
        {
            string key = ApplicationData.Database.CurrentKitFeature.Key;
            ApplicationData.Database.CurrentKitTemplate.RemoveKeyValuePair(key);
        }
    }
}
