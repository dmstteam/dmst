﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Characters;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for RaceEditorWindow.xaml
    /// </summary>
    public partial class RaceEditorWindow : Window
    {
        public RaceEditorWindow()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.Database;

            distanceUnitComboBox.ItemsSource = Enum.GetValues(typeof(DistanceUnit)).Cast<DistanceUnit>();
            sizeUnitComboBox.ItemsSource = Enum.GetValues(typeof(SizeCategory)).Cast<SizeCategory>();
        }

        private void NewRaceButton_Click(object sender, RoutedEventArgs e)
        {
            ApplicationData.Database.Races.Add(new Race());
        }

        private void RaceListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentRace = (RaceListBox.SelectedItem as Race);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ApplicationData.SaveDatabase();
        }
    }
}
