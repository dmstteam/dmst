﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Items;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for ItemEditor.xaml
    /// </summary>
    public partial class ItemEditorWindow : Window
    {
        public ItemEditorWindow()
        {
            InitializeComponent();
        }

        public void InitializeWindow(Item item) 
        {
            propertyGrid.SelectedObject = item;
        }
    }
}
