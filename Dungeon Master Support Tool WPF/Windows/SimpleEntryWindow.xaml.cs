﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for SimpleEntryWindow.xaml
    /// </summary>
    public partial class SimpleEntryWindow : Window
    {
        public SimpleEntryWindow()
        {
            InitializeComponent();
        }

        public SimpleEntryWindow(string header, int initialValue, string option1, string option2)
        {
            InitializeComponent();
            titleTextBlock.Text = header;
            valueTextBox.Text = initialValue.ToString();
            HealButton.Content = option1;
            DamageButton.Content = option2;
        }

        public int Entry;

        public int Option;

        private void HealButton_Click(object sender, RoutedEventArgs e)
        {
            bool Success = int.TryParse(valueTextBox.Text, out Entry);

            if (Success)
            {
                Option = 1;
            }
            else
            {
                Option = 0;
            }

            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Option = 0;
            this.Close();
        }

        private void DamageButton_Click(object sender, RoutedEventArgs e)
        {
            bool Success = int.TryParse(valueTextBox.Text, out Entry);

            if (Success)
            {
                Option = 2;
            }
            else
            {
                Option = 0;
            }

            this.Close();
        }
    }
}
