﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMSTCore;
using Microsoft.Win32;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for EscapeMenuWindow.xaml
    /// </summary>
    public partial class EscapeMenuWindow : Window
    {
        public EscapeMenuWindow()
        {
            InitializeComponent();
        }

        private void SaveCampaignButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "NewCampaign"; // Default file name
            dlg.DefaultExt = ".cmp"; // Default file extension
            dlg.Filter = "*.cmp"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                ApplicationData.System.SaveCampaign();
            }
        }

        private void ClassEditorButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            KitEditorWindow kitEditorWindow = new KitEditorWindow();
            kitEditorWindow.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            ApplicationData.System.SaveCampaign();
            Application.Current.Shutdown();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void RaceEditorButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            RaceEditorWindow raceEditorWindow = new RaceEditorWindow();
            raceEditorWindow.ShowDialog();
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
