﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using DMSTCore.Spells;
using DMSTCore;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for SpellBookEditor.xaml
    /// </summary>
    public partial class SpellBookEditor : Window
    {
        private SpellBookEditor()
        {
            InitializeComponent();
        }

        public SpellBookEditor(SpellBook leftSpellBook, SpellBook rightSpellBook)
        {
            InitializeComponent();

            LeftSpellBook.SetSpellBook(leftSpellBook);
            LeftSpellBook.SetCombatant(ApplicationData.CurrentCombatant);

            RightSpellBook.SetSpellBook(rightSpellBook);
            RightSpellBook.SetCombatant(ApplicationData.CurrentCombatant);

            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        private void RightToLeftButton_Click(object sender, RoutedEventArgs e)
        {
            Spell spell = (RightSpellBook.DataContext as SpellBook).CurrentSpell;
            (LeftSpellBook.DataContext as SpellBook).AddSpell(spell);

            LeftSpellBook.UpdateSpellList();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
