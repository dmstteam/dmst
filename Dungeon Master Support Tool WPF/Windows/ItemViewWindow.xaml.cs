﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Items;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for ItemViewWindow.xaml
    /// </summary>
    public partial class ItemViewWindow : Window
    {
        public Item SelectedItem;

        public ItemViewWindow()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.Database;

            itemTypeComboBox.Items.Clear();
            List<string> itemTypes = EnumHelper.EnumToListofStrings<ItemType>();
            foreach (string s in itemTypes)
            {
                itemTypeComboBox.Items.Add(s);
            }

            itemTypeComboBox.SelectedIndex = 0;
        }

        private void ItemSelectionListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            itemInformationControl.DataContext = itemSelectionListBox.SelectedItem;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            SelectedItem = itemSelectionListBox.SelectedItem as Item;
            Close();
        }

        private void newItemButton_Click(object sender, RoutedEventArgs e)
        {
            ItemEditorWindow itemEditorWindow = new ItemEditorWindow();
            ItemType type = (ItemType)itemTypeComboBox.SelectedIndex;

            Item item;
            if (type == ItemType.Armor)
            {
                item = new Armor();
            }
            else if (type == ItemType.Shield)
            {
                item = new Shield();
            }
            else if (type == ItemType.Weapon)
            {
                item = new Weapon();
            }
            else if (type == ItemType.Item)
            {
                item = new Item();
            }
            else
            {
                item = new EquippableItem();
                (item as EquippableItem).EquipLocation = type;
            }

            itemEditorWindow.InitializeWindow(item);
            itemEditorWindow.ShowDialog();

            ApplicationData.Database.Items.Add(itemEditorWindow.propertyGrid.SelectedObject as Item);
            ApplicationData.SaveDatabase();
        }
    }
}
