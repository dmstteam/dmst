﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMSTCore;

namespace Dungeon_Master_Support_Tool_WPF.Windows
{
    /// <summary>
    /// Interaction logic for RequestTargetWindow.xaml
    /// </summary>
    public partial class RequestTargetWindow : Window
    {
        public RequestTargetWindow()
        {
            InitializeComponent();
        }

        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void PossibleTargetsListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (PossibleTargetsListBox.Items.Count == 0) { return; }

            RequestTargetEventArgs args = (this.DataContext as RequestTargetEventArgs);

            if (args.NumberOfTargetsToSelect == 0) { return; }

            Combatant combatant = PossibleTargetsListBox.SelectedItem as Combatant;
            args.SelectedTargets.Add(combatant);
            args.PossibleTargets.Remove(combatant);
            args.NumberOfTargetsToSelect--;
        }

        private void SelectedTargetsListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (SelectedTargetsListBox.Items.Count == 0) { return; }

            RequestTargetEventArgs args = (this.DataContext as RequestTargetEventArgs);

            Combatant combatant = SelectedTargetsListBox.SelectedItem as Combatant;
            args.SelectedTargets.Remove(combatant);
            args.PossibleTargets.Add(combatant);
            args.NumberOfTargetsToSelect++;
        }
    }
}
