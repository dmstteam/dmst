﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using DMSTCore;
using DMSTCore.Characters;
using DMSTCore.Campaigns;
using DMSTCore.Encounters;

namespace Dungeon_Master_Support_Tool_WPF
{
    public static class MenuItemHelper
    {
        public static MenuItem CreateNPCMenuItem(RoutedEventHandler npcItem_Click, string header)
        {
            MenuItem MenuItem1 = new MenuItem();
            MenuItem1.Header = header;

            foreach (Character npc in ApplicationData.CurrentCampaign.NPCs)
            {
                NPCMenuItem item = new NPCMenuItem();
                item.Header = npc.Name;
                item.Click += npcItem_Click;
                item.Character = npc;
                MenuItem1.Items.Add(item);
            }
            return MenuItem1;
        }

        public static MenuItem CreateTeamMenuItem(RoutedEventHandler teamItem_Click, string header)
        {
            MenuItem MenuItem1 = new MenuItem();
            MenuItem1.Header = header;

            foreach (Team team in ApplicationData.CurrentEncounter.Teams)
            {
                TeamMenuItem item = new TeamMenuItem();
                item.Header = team.Name;
                item.Click += teamItem_Click;
                item.Team = team;
                MenuItem1.Items.Add(item);
            }
            return MenuItem1;
        }

        public static MenuItem CreatePlaceMenuItem(RoutedEventHandler placeItem_Click, string header)
        {
            MenuItem MenuItem1 = new MenuItem();
            MenuItem1.Header = header;

            foreach (Place place in ApplicationData.CurrentCampaign.Places)
            {
                PlaceMenuItem item = new PlaceMenuItem();
                item.Header = place.Name;
                item.Click += placeItem_Click;
                item.Place = place;
                MenuItem1.Items.Add(item);
            }
            return MenuItem1;
        }

        public static MenuItem CreateOrganizationMenuItem(RoutedEventHandler organizationItem_Click, string header)
        {
            MenuItem organizationMenuItem = new MenuItem();
            organizationMenuItem.Header = header;

            foreach (Organization org in ApplicationData.CurrentCampaign.Organizations)
            {
                OrganizationMenuItem item = new OrganizationMenuItem();
                item.Header = org.Name;
                item.Click += organizationItem_Click;
                item.Organization = org;
                organizationMenuItem.Items.Add(item);
            }

            return organizationMenuItem;
        }
    }

    public class NPCMenuItem : MenuItem
    {
        public Character Character;
    }

    public class PlaceMenuItem : MenuItem
    {
        public Place Place;
    }

    public class OrganizationMenuItem : MenuItem
    {
        public Organization Organization;
    }

    public class TeamMenuItem : MenuItem
    {
        public Team Team;
    }
}
