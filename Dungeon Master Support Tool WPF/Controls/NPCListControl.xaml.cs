﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Characters;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for NPCListControl.xaml
    /// </summary>
    public partial class NPCListControl : UserControl
    {
        public NPCListControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentCampaign;
        }

        private void AddNPC_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCampaign.NPCs.Add(new DMSTCore.Characters.Character());
        }

        private void EditNPC_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            CharacterCreationWindow CharacterCreationWindow = new CharacterCreationWindow();
            CharacterCreationWindow.ShowDialog();
        }

        private void OpenNPC_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void LevelUpNPC_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                (ApplicationData.CurrentCombatant as Character).LevelUp();
            }
        }

        private void LevelDownNPC_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                (ApplicationData.CurrentCombatant as Character).LevelDown();
            }
        }

        private void DeleteNPC_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                ApplicationData.CurrentCampaign.NPCs.Remove(ApplicationData.CurrentCombatant as Character);
            }
        }

        private void NPCListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCombatant = NPCListBox.SelectedItem as Combatant;
        }
    }
}
