﻿using System;
using System.Windows.Markup;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Dungeon_Master_Support_Tool_WPF
{
    class EnumComboBox : ComboBox
    {
        public Enum currentValue {get; set;}

        public void LoadEnums<T>(T currentValue)
        {
            var names = Enum.GetValues(typeof(T));

            Items.Clear();
            int cnt = 0;
            foreach (var name in names)
            {
                Items.Add(name);

                if (name.Equals(currentValue))
                {
                    SelectedIndex = cnt;
                }
                cnt++;
            }
        }

        public T GetSelectedEnum<T>()
        {
            return (T)Enum.ToObject(typeof(T),SelectedIndex);
        }
    }
}
