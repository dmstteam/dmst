﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Campaigns;
using DMSTCore.Characters;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for OrganizationsControl.xaml
    /// </summary>
    public partial class OrganizationsControl : UserControl
    {
        public OrganizationsControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentCampaign;
        }

        private void OrganizationListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.CurrentCampaign.CurrentOrganization = OrganizationListBox.SelectedItem as Organization;
        }

        private void AddOrganization_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCampaign.Organizations.Add(new Organization());
        }

        private void MemberListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.CurrentCombatant = MemberListBox.SelectedItem as Character;
        }

        private void RemoveOrganization_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCampaign.Organizations.Remove(ApplicationData.CurrentCampaign.CurrentOrganization);
        }
    }
}
