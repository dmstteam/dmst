﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Campaigns;
using DMSTCore.Characters;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for OrganizationMicroDescriptionControl.xaml
    /// </summary>
    public partial class PlaceMicroDescriptionControl : UserControl
    {
        public PlaceMicroDescriptionControl()
        {
            InitializeComponent();
        }

        private void Grid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            MenuItem menuItem1 = MenuItemHelper.CreateNPCMenuItem(npcItem_Click, "Add NPC");
            MenuItem menuItem2 = MenuItemHelper.CreateOrganizationMenuItem(headQuartersItem_Click, "Set as Headquarters For");

            ContextMenu = new System.Windows.Controls.ContextMenu();
            ContextMenu.Items.Clear();
            ContextMenu.Items.Add(menuItem1);
            ContextMenu.Items.Add(menuItem2);
        }

        void npcItem_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as Place).AddMember((sender as NPCMenuItem).Character);
        }

        void headQuartersItem_Click(object sender, RoutedEventArgs e)
        {
            (sender as OrganizationMenuItem).Organization.HeadQuarters = (this.DataContext as Place);
        }
    }
}
