﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for CombatantNanoDescriptionControl.xaml
    /// </summary>
    public partial class CombatantNanoDescriptionControl : UserControl
    {
        public CombatantNanoDescriptionControl()
        {
            InitializeComponent();
        }
    }

    public class HPToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                              object parameter, CultureInfo culture)
        {
            Color color = new Color();
            color = Colors.Black;

            SolidColorBrush brush = new SolidColorBrush(color);

            if (value == null)
                return color;

            float percentFromFull = (float)value;

            if (percentFromFull > 0.75)
            {
                color = Colors.Black;
            }
            else if (percentFromFull > 0.50)
            {
                color = Colors.Green;
            }
            else if (percentFromFull > 0.25)
            {
                color = Colors.Orange;
            }
            else
            {
                color = Colors.Red;
            }

            brush.Color = color;

            return brush;
        }

        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
