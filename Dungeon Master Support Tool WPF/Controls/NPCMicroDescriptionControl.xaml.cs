﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Campaigns;
using DMSTCore.Characters;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for NPCMicroDescriptionControl.xaml
    /// </summary>
    public partial class NPCMicroDescriptionControl : CharacterContextMenu
    {
        public NPCMicroDescriptionControl()
        {
            InitializeComponent();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCombatant = this.DataContext as Combatant;

            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                CharacterSheetWindow characterSheet = new CharacterSheetWindow();
                characterSheet.ShowDialog();
            }
        }

        private void Grid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ContextMenu = CreateCharacterContextMenu(ContextMenu);

            MenuItem menuItem1 = MenuItemHelper.CreateOrganizationMenuItem(organizationItem_Click, "Add to Organization");
            MenuItem menuItem2 = MenuItemHelper.CreatePlaceMenuItem(placeItem_Click, "Add to Place");

            ContextMenu.Items.Insert(ContextMenu.Items.Count - 1, menuItem1);
            ContextMenu.Items.Insert(ContextMenu.Items.Count - 1, menuItem2);
        }

        private void organizationItem_Click(object sender, RoutedEventArgs e)
        {
            (sender as OrganizationMenuItem).Organization.AddMember(ApplicationData.CurrentCombatant);
        }

        private void placeItem_Click(object sender, RoutedEventArgs e)
        {
            (sender as PlaceMenuItem).Place.AddMember(ApplicationData.CurrentCombatant);
        }
    }
}
