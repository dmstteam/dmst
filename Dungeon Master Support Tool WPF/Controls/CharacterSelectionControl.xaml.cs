﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Characters;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF
{
    /// <summary>
    /// Interaction logic for CharacterSelectionControl.xaml
    /// </summary>
    public partial class CharacterSelectionControl : UserControl
    {
        public CharacterSelectionControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentCampaign;
        }

        public void DeleteSelectedCharacter() 
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                ApplicationData.CurrentCampaign.PCs.Remove(ApplicationData.CurrentCombatant as Character);
            }
        }

        public void LevelDownSelectedCharacter() 
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                (ApplicationData.CurrentCombatant as Character).LevelDown();
            }
        }

        public void LevelUpSelectedCharacter()
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                (ApplicationData.CurrentCombatant as Character).LevelUp();
            }
        }

        private void ListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCombatant = CharacterListBox.SelectedItem as Character;
        }

        private void AddCharacter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCombatant = new Character();
            ApplicationData.CurrentCampaign.PCs.Add(ApplicationData.CurrentCombatant as Character);
            CharacterCreationWindow CharacterCreationWindow = new CharacterCreationWindow();
            CharacterCreationWindow.ShowDialog();
        }

        private void EditCharacter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            CharacterCreationWindow CharacterCreationWindow = new CharacterCreationWindow();
            CharacterCreationWindow.ShowDialog();
        }

        private void LevelUpCharacter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            LevelUpSelectedCharacter();
        }

        private void LevelDownCharacter_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            LevelDownSelectedCharacter();
        }

        private void OpenCharacter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SpellBookEditor spellBookEditor = new SpellBookEditor(ApplicationData.CurrentCombatant.SpellBook, ApplicationData.GetSpellBook());
            spellBookEditor.ShowDialog();
        }

        private void DeleteCharacter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DeleteSelectedCharacter();
        }
    }
}
