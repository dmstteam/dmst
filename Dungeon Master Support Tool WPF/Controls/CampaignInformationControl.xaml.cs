﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Campaigns;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for CampaignInformationControl.xaml
    /// </summary>
    public partial class CampaignInformationControl : UserControl
    {
        public CampaignInformationControl()
        {
            InitializeComponent();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                ApplicationData.CurrentCampaign = this.DataContext as Campaign;
                CampaignPage page = new CampaignPage();
                page.DataContext = ApplicationData.CurrentCampaign;
                Switcher.Switch(page);
            }
        }
    }
}
