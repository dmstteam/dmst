﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore.Encounters;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for QuestMicroDescriptionControl.xaml
    /// </summary>
    public partial class EncounterMicroDescriptionControl : UserControl
    {
        public EncounterMicroDescriptionControl()
        {
            InitializeComponent();
        }
    }
}
