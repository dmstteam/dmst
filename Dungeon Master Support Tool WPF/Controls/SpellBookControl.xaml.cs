﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

using DMSTCore;
using DMSTCore.Characters;
using DMSTCore.Spells;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for SpellDatabaseViewer.xaml
    /// </summary>
    public partial class SpellBookControl : UserControl
    {
        private SpellBook _spellBook;
        private Combatant _combatant;

        public SpellBookControl()
        {
            InitializeComponent();

            SpellLevelComboBox.Items.Add("All");
            SpellLevelComboBox.SelectedIndex = 0;
            for (int i = 0; i < 10; i++)
            {
                SpellLevelComboBox.Items.Add(i.ToString());
            }

            MagicSchoolComboBox.Items.Add("All");
            MagicSchoolComboBox.SelectedIndex = 0;
            List<string> magicSchools = EnumHelper.EnumToListofStrings<MagicSchool>();
            foreach (string s in magicSchools)
            {
                MagicSchoolComboBox.Items.Add(s);
            }
        }

        public void SetSpellBook(SpellBook spellBook) 
        {
            _spellBook = spellBook;
            this.DataContext = _spellBook;

            UpdateSpellList();
        }

        public void SetCombatant(Combatant combatant)
        {
            _combatant = combatant;

            UpdateSpellList();
        }

        private void SpellLevelComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateSpellList();
        }

        private void MagicSchoolComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateSpellList();
        }

        public void UpdateSpellList()
        {
            if (_spellBook == null) { return; }

            int spellLevel = SpellLevelComboBox.SelectedIndex - 1;
            MagicSchool magicSchool = (MagicSchool)(MagicSchoolComboBox.SelectedIndex - 1);

            if (SpellLevelComboBox.SelectedIndex == 0 &&
                MagicSchoolComboBox.SelectedIndex == 0)
            {
                _spellBook.SortSpells(_combatant);
            }
            else if (SpellLevelComboBox.SelectedIndex == 0)
            {
                _spellBook.SortSpells(_combatant, magicSchool);
            }
            else if (MagicSchoolComboBox.SelectedIndex == 0)
            {
                _spellBook.SortSpells(_combatant, spellLevel);
            }
            else
            {
                _spellBook.SortSpells(_combatant, magicSchool, spellLevel);
            }
        }

        private void SpellListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _spellBook.CurrentSpell = SpellListBox.SelectedItem as Spell;
        }
    }
}
