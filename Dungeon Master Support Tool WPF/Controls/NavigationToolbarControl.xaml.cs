﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for NavigationToolbarControl.xaml
    /// </summary>
    public partial class NavigationToolbarControl : UserControl
    {
        public NavigationToolbarControl()
        {
            InitializeComponent();
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            Switcher.Back();
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            ApplicationData.System.SaveCampaign();
            System.Windows.Application.Current.Shutdown();
        }
    }
}
