﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Characters;
using Dungeon_Master_Support_Tool_WPF.Controls;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF
{
    /// <summary>
    /// Interaction logic for CharacterMicroDescriptionControl.xaml
    /// </summary>
    public partial class CharacterMicroDescriptionControl : CharacterContextMenu
    {
        public CharacterMicroDescriptionControl()
        {
            InitializeComponent();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                CharacterSheetWindow characterSheet = new CharacterSheetWindow();
                characterSheet.ShowDialog();
            }
        }

        private void Grid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ContextMenu = CreateCharacterContextMenu(ContextMenu);
        }
    }
}
