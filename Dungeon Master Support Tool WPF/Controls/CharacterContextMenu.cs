﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Characters;
using Dungeon_Master_Support_Tool_WPF.Windows;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    public class CharacterContextMenu : UserControl
    {
        protected ContextMenu CreateCharacterContextMenu(ContextMenu contextMenu)
        {
            contextMenu = new System.Windows.Controls.ContextMenu();
            contextMenu.Items.Clear();

            MenuItem iconMenuItem = new MenuItem();
            iconMenuItem.Header = "Change Icon";
            iconMenuItem.Click += AddIcon_Click;

            MenuItem levelUpMenuItem = new MenuItem();
            levelUpMenuItem.Header = "Level Up";
            levelUpMenuItem.Click += LevelUpMenuItem_Click;

            MenuItem levelDownMenuItem = new MenuItem();
            levelDownMenuItem.Header = "Level Down";
            levelDownMenuItem.Click += LevelDownMenuItem_Click;

            MenuItem classRaceMenuItem = new MenuItem();
            classRaceMenuItem.Header = "Character Sheet";
            classRaceMenuItem.Click += EditMenuItem_Click;

            MenuItem spellBookMenuItem = new MenuItem();
            spellBookMenuItem.Header = "Spellbook";
            spellBookMenuItem.Click += spellBookMenuItem_Click;

            MenuItem deleteMenuItem = new MenuItem();
            deleteMenuItem.Header = "Delete";
            deleteMenuItem.Click += DeleteMenuItem_Click;

            MenuItem healDamageMenuItem = new MenuItem();
            healDamageMenuItem.Header = "Heal/Damage";
            healDamageMenuItem.Click += healDamageMenuItem_Click;

            MenuItem experiencePointsItem = new MenuItem();
            experiencePointsItem.Header = "Experience";
            experiencePointsItem.Click += awardExperienceItem_Click;

            MenuItem inspirationPointItem = new MenuItem();
            inspirationPointItem.Header = "Inspiration Points";
            inspirationPointItem.Click += awardInspirationPoint_Click;

            MenuItem hitDiceItem = new MenuItem();
            hitDiceItem.Header = "Hit Dice";
            hitDiceItem.Click += HitDiceItem_Click;

            MenuItem editMenu = new MenuItem();
            editMenu.Header = "Edit";
            editMenu.Items.Clear();
            editMenu.Items.Add(iconMenuItem);
            editMenu.Items.Add(classRaceMenuItem);
            editMenu.Items.Add(spellBookMenuItem);

            MenuItem affectMenu = new MenuItem();
            affectMenu.Header = "Affect";
            affectMenu.Items.Clear();
            affectMenu.Items.Add(healDamageMenuItem);
            affectMenu.Items.Add(hitDiceItem);
            affectMenu.Items.Add(experiencePointsItem);
            affectMenu.Items.Add(inspirationPointItem);

            contextMenu.Items.Add(editMenu);
            contextMenu.Items.Add(affectMenu);
            contextMenu.Items.Add(levelUpMenuItem);
            contextMenu.Items.Add(levelDownMenuItem);
            contextMenu.Items.Add(deleteMenuItem);

            return contextMenu;
        }

        void HitDiceItem_Click(object sender, RoutedEventArgs e)
        {
            SimpleEntryWindow window = new SimpleEntryWindow("Hit Dice:", 1, "Add", "Spend");
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.ShowDialog();

            if (window.Option == 1)
            {
                ApplicationData.CurrentCombatant.HitDice += window.Entry;
            }
            else if (window.Option == 2)
            {
                ApplicationData.CurrentCombatant.SpendHitDice(window.Entry);
            }
        }

        void awardInspirationPoint_Click(object sender, RoutedEventArgs e)
        {
            SimpleEntryWindow window = new SimpleEntryWindow("Inspiration Points:", 1, "Add", "Spend");
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.ShowDialog();

            if (window.Option == 1)
            {
                ApplicationData.CurrentCombatant.Inspiration += window.Entry;
            }
            else if (window.Option == 2)
            {
                ApplicationData.CurrentCombatant.Inspiration -= window.Entry;
            }
        }

        void awardExperienceItem_Click(object sender, RoutedEventArgs e)
        {
            SimpleEntryWindow window = new SimpleEntryWindow("Experience Points:", 0, "Award", "Rescind");
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.ShowDialog();

            if (window.Option == 1)
            {
                ApplicationData.CurrentCombatant.ExperiencePoints += window.Entry;
            }
            else if (window.Option == 2)
            {
                ApplicationData.CurrentCombatant.ExperiencePoints -= window.Entry;
            }
        }

        void healDamageMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SimpleEntryWindow window = new SimpleEntryWindow("Heal/Damage Amount:", 0, "Heal", "Damage");
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            window.ShowDialog();

            if (window.Option == 1)
            {
                ApplicationData.CurrentCombatant.Heal(window.Entry);
            }
            else if (window.Option == 2)
            {
                ApplicationData.CurrentCombatant.Damage(window.Entry);
            }
        }

        void spellBookMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SpellBookEditor spellBookEditor = new SpellBookEditor(ApplicationData.CurrentCombatant.SpellBook, ApplicationData.GetSpellBook());
            spellBookEditor.ShowDialog();
        }

        void LevelUpMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                (ApplicationData.CurrentCombatant as Character).LevelUp();
            }
        }

        void LevelDownMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                (ApplicationData.CurrentCombatant as Character).LevelDown();
            }
        }

        void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (ApplicationData.CurrentCombatant != null)
            {
                ApplicationData.CurrentCampaign.PCs.Remove(ApplicationData.CurrentCombatant as Character);
                ApplicationData.CurrentCampaign.NPCs.Remove(ApplicationData.CurrentCombatant as Character);
            }
        }

        void AddIcon_Click(object sender, RoutedEventArgs e)
        {
            SelectIconWindow window = new SelectIconWindow();
            window.SetToCharacterIcons();
            window.ShowDialog();
            (this.DataContext as NameDescription).IconPath = window.SelectedIconPath;
        }

        void EditMenuItem_Click(object sender, RoutedEventArgs e)
        {
            CharacterCreationWindow CharacterCreationWindow = new CharacterCreationWindow();
            CharacterCreationWindow.ShowDialog();
        }
    }
}
