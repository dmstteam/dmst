﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPFCustomMessageBox;

using DMSTCore;
using DMSTCore.Campaigns;
using Dungeon_Master_Support_Tool_WPF.Pages;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for QuestSelectionControl.xaml
    /// </summary>
    public partial class EncounterSelectionControl : UserControl
    {
        public EncounterSelectionControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentQuest;
        }

        private void AddEncounter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentQuest.Encounters.Add(new Encounter());
        }

        private void OpenEncounter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void DeleteEncounter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ApplicationData.CurrentEncounter == null) { return; }

            ApplicationData.CurrentQuest.Encounters.Remove(ApplicationData.CurrentEncounter);
        }

        private void ListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentEncounter = EncounterListBox.SelectedItem as Encounter;
        }

        private void EncounterListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ApplicationData.CurrentEncounter != null)
            {
                MessageBoxResult result = CustomMessageBox.ShowYesNoCancel("Do you want to run or edit this encounter?", "Encounter Options", "Run", "Edit", "Cancel");

                if (result == MessageBoxResult.Yes)
                {
                    Switcher.Switch(new EncounterPage());
                }
                else if (result == MessageBoxResult.No)
                {
                    Switcher.Switch(new EncouterCreationPage());
                }
            }
        }

        private void UserControl_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {

        }
    }
}
