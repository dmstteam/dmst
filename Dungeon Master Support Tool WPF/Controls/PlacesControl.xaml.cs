﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Campaigns;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for PlacesControl.xaml
    /// </summary>
    public partial class PlacesControl : UserControl
    {
        public PlacesControl()
        {
            InitializeComponent();
        }

        private void AddPlace_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCampaign.Places.Add(new Place());
        }

        private void RemovePlace_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCampaign.Places.Remove(ApplicationData.CurrentCampaign.CurrentPlace);
        }

        private void PlaceListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Place place = PlaceListBox.SelectedItem as Place;

            if (place != null)
            {
                ApplicationData.CurrentCampaign.CurrentPlace = place;
            }
        }

        private void MemberListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
