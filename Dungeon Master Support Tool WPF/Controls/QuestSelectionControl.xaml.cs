﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Campaigns;
using Dungeon_Master_Support_Tool_WPF.Pages;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for QuestSelectionControl.xaml
    /// </summary>
    public partial class QuestSelectionControl : UserControl
    {
        public QuestSelectionControl()
        {
            InitializeComponent();

            this.DataContext = ApplicationData.CurrentCampaign;
        }

        private void AddQuest_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentCampaign.Quests.Add(new Quest());
        }

        private void OpenQuest_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void DeleteQuest_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ApplicationData.CurrentCampaign.CurrentQuest == null) { return; }

            ApplicationData.CurrentCampaign.Quests.Remove(ApplicationData.CurrentCampaign.CurrentQuest);
        }

        private void ListBox_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ApplicationData.CurrentQuest = QuestListBox.SelectedItem as Quest;
        }

        private void QuestListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ApplicationData.CurrentQuest != null)
            {
                Switcher.Switch(new QuestPage());
            }
        }
    }
}
