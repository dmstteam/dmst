﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSTCore;
using DMSTCore.Maps;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    public class DraggableControl : UserControl
    {
        public void SetMinMaxBoundaries(int xmin, int xmax, int ymin, int ymax)
        {
            XMin = xmin;
            XMax = xmax;
            YMin = ymin;
            YMax = ymax;

            MapObject map = (this.DataContext as MapObject);

            if (map.X < xmin) { map.X = xmin; }
            if (map.Y < ymin) { map.Y = ymin; }
            if (map.X > xmax) { map.X = xmax; }
            if (map.Y > ymax) { map.Y = ymax; }
        }

        protected int XMin;

        protected int XMax;

        protected int YMin;

        protected int YMax;

        protected bool isDragging;

        protected Point clickPosition;

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            isDragging = true;
            clickPosition = e.GetPosition(this.Parent as UIElement);
            CaptureMouse();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (isDragging)
            {
                Point currentPosition = e.GetPosition(this.Parent as UIElement);

                var transform = RenderTransform as TranslateTransform;
                if (transform == null)
                {
                    transform = new TranslateTransform();
                    RenderTransform = transform;
                }

                if (currentPosition.X > XMin && currentPosition.X < XMax)
                {
                    transform.X = currentPosition.X - clickPosition.X;
                }

                if (currentPosition.Y > YMin && currentPosition.Y < YMax)
                {
                    transform.Y = currentPosition.Y - clickPosition.Y;
                }
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            isDragging = false;
            Point currentPosition = e.GetPosition(this.Parent as UIElement);

            RenderTransform = null;
            ReleaseMouseCapture();
        }
    }
}
