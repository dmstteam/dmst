﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Campaigns;
using DMSTCore.Characters;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for OrganizationMicroDescriptionControl.xaml
    /// </summary>
    public partial class OrganizationMicroDescriptionControl : UserControl
    {
        public OrganizationMicroDescriptionControl()
        {
            InitializeComponent();
        }

        private void Grid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            MenuItem MenuItem1 = MenuItemHelper.CreateNPCMenuItem(npcItem_Click, "Add Member");
            MenuItem MenuItem2 = MenuItemHelper.CreatePlaceMenuItem(placeItem_Click, "Set Headquarters");

            ContextMenu = new System.Windows.Controls.ContextMenu();
            ContextMenu.Items.Clear();
            ContextMenu.Items.Add(MenuItem1);
            ContextMenu.Items.Add(MenuItem2);
        }

        void npcItem_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as Organization).AddMember((sender as NPCMenuItem).Character);
        }

        void placeItem_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as Organization).HeadQuarters = (sender as PlaceMenuItem).Place;
        }
    }
}
