﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore;
using DMSTCore.Effects;
using DMSTCore.Kits;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for KitEditorControl.xaml
    /// </summary>
    public partial class KitEditorControl : UserControl
    {
        public KitEditorControl()
        {
            InitializeComponent();

            Stat2ComboBox.Items.Clear();
            Stat1ComboBox.Items.Clear();
            var stats = EnumHelper.EnumToListofStrings<AttributeEnum>();
            foreach (string s in stats)
            {
                Stat1ComboBox.Items.Add(s);
                Stat2ComboBox.Items.Add(s);
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListBox.SelectedItem == null) { return; }

            ApplicationData.Database.CurrentKitFeature = (ListBox.SelectedItem as KeyLevelPair).KitFeature;
        }

        private void Stat1ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentKitTemplate.MajorStat1Index = Stat1ComboBox.SelectedIndex;
        }

        private void Stat2ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationData.Database.CurrentKitTemplate.MajorStat2Index = Stat2ComboBox.SelectedIndex;
        }
    }
}
