﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DMSTCore.Effects;
using DMSTCore.GameEngine;

namespace Dungeon_Master_Support_Tool_WPF.ContentEditorControls
{
    /// <summary>
    /// Interaction logic for GameEngineEditorControl.xaml
    /// </summary>
    public partial class GameEngineEditorControl : UserControl
    {
        public GameEngineEditorControl()
        {
            InitializeComponent();

            StepTypeComboBox.ItemsSource = Enum.GetValues(typeof(StepType)).Cast<StepType>();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            StepType type = (StepType)StepTypeComboBox.SelectedIndex;

            Step step;
            switch (type)
            {
                case StepType.Augmentation:
                    step = new AugmentationStep();
                    break;
                case StepType.Requirement:
                    step = new RequirementStep();
                    break;
                default:
                    return;
            }

            (this.DataContext as Engine).Steps.Add(step);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            (this.DataContext as Engine).Steps.Remove(stepListBox.SelectedItem as Step);
        }

        private void stepListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            (this.DataContext as Engine).CurrentStep = stepListBox.SelectedItem as Step;
        }
    }
}
