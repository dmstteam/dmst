﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using DMSTCore;
using DMSTCore.Effects;
using DMSTCore.EffectRequirements;
using DMSTCore.Actions;

namespace Dungeon_Master_Support_Tool_WPF.Controls
{
    /// <summary>
    /// Interaction logic for KitFeatureControl.xaml
    /// </summary>
    public partial class KitFeatureControl : UserControl
    {
        public KitFeatureControl()
        {
            InitializeComponent();

            EffectComboBox.Items.Clear();
            ActionEffectComboBox.Items.Clear();
            var list = (EnumHelper.EnumToListofStrings<EffectType>());
            foreach (string s in list)
            {
                EffectComboBox.Items.Add(s);
                ActionEffectComboBox.Items.Add(s);
            }

            list = (EnumHelper.EnumToListofStrings<EffectRequirementType>());
            PassiveEffectRequirementComboBox.Items.Clear();
            ActionEffectRequirementComboBox.Items.Clear();
            foreach (string s in list)
            {
                PassiveEffectRequirementComboBox.Items.Add(s);
                ActionEffectRequirementComboBox.Items.Add(s);
            }

            EffectComboBox.SelectedIndex = 0;
            ActionEffectComboBox.SelectedIndex = 0;
            PassiveEffectRequirementComboBox.SelectedIndex = 0;
            ActionEffectRequirementComboBox.SelectedIndex = 0;

            list = (EnumHelper.EnumToListofStrings<ActionType>());
            ActionSelectComboBox.Items.Clear();
            foreach (string s in list)
            {
                ActionSelectComboBox.Items.Add(s);
            }
        }

        private void AddEffectButton_Click(object sender, RoutedEventArgs e)
        {
            EffectType type = (EffectType)EffectComboBox.SelectedIndex;

            CombatantEffect effect = GetNewEffect(type);

            ApplicationData.Database.CurrentKitFeature.Effects.Add(effect);
            ApplicationData.Database.CurrentKitFeature.CurrentEffect = effect;
        }

        private void EffectListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (EffectListBox.SelectedIndex == -1) { return; }

            ActionEffectListBox.SelectedIndex = -1;
            ApplicationData.Database.CurrentKitFeature.CurrentEffect = (EffectListBox.SelectedItem as CombatantEffect);
        }

        private void ActionEffectListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ActionEffectListBox.SelectedIndex == -1) { return; }

            EffectListBox.SelectedIndex = -1;
            ApplicationData.Database.CurrentKitFeature.CurrentEffect = (ActionEffectListBox.SelectedItem as CombatantEffect);
        }

        private void ActionEffectRequirementListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ActionEffectRequirementListBox.SelectedIndex == -1) { return; }

            PassiveEffectRequirementListBox.SelectedIndex = -1;
            ApplicationData.Database.CurrentKitFeature.CurrentRequirement = (ActionEffectRequirementListBox.SelectedItem as EffectRequirement);
        }

        private void EffectRequirementListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PassiveEffectRequirementListBox.SelectedIndex == -1) { return; }

            ActionEffectRequirementListBox.SelectedIndex = -1;
            ApplicationData.Database.CurrentKitFeature.CurrentRequirement = (PassiveEffectRequirementListBox.SelectedItem as EffectRequirement);
        }

        private void AddActionEffectButton_Click(object sender, RoutedEventArgs e)
        {
            EffectType type = (EffectType)ActionEffectComboBox.SelectedIndex;

            CombatantEffect effect = GetNewEffect(type);

            ApplicationData.Database.CurrentKitFeature.Action.Effects.Add(effect);
            ApplicationData.Database.CurrentKitFeature.CurrentEffect = effect;
        }

        private CombatantEffect GetNewEffect(EffectType type)
        {
            CombatantEffect effect = null;
            switch (type)
            {
                case EffectType.AttackBonus:
                    effect = new AttackBonusEffect();
                    break;
                case EffectType.BonusDamage:
                    effect = new ExtraDamageEffect();
                    break;
                case EffectType.MoveSpeed:
                    effect = new MoveSpeedEffect();
                    break;
                case EffectType.AdvantageOnSave:
                    effect = new AdvantageOnSaveEffect();
                    break;
                case EffectType.DamageResistance:
                    effect = new DamageResistanceEffect();
                    break;
                case EffectType.AdvantageOnAttack:
                    effect = new AdvantageOnAttackEffect();
                    break;
                case EffectType.AdditionalAC:
                    effect = new AdditionalACEffect();
                    break;
                case EffectType.ImproveAbilityScore:
                    effect = new ImproveAbilityScoreEffect();
                    break;
                case EffectType.AdditionalAttack:
                    effect = new AdditionalAttackEffect();
                    break;
                case EffectType.InitiativeRoll:
                    effect = new InitiativeRollEffect();
                    break;
                case EffectType.SetHP:
                    effect = new SetHPEffect();
                    break;
            }

            return effect;
        }

        private EffectRequirement GetNewEffectRequirement(EffectRequirementType type)
        {
            EffectRequirement requirement = null;
            switch (type)
            {
                case EffectRequirementType.ArmorRequirement:
                    requirement = new ArmorRequirement();
                    break;
                case EffectRequirementType.ConditionRequirement:
                    requirement = new ConditionRequirement();
                    break;
                case EffectRequirementType.ActiveAction:
                    requirement = new ActiveActionRequirement();
                    break;
                case EffectRequirementType.SaveRequirement:
                    requirement = new SaveRequirement();
                    break;
                case EffectRequirementType.HPRequirement:
                    requirement = new HPRequirement();
                    break;
                case EffectRequirementType.WeaponRequirement:
                    requirement = new WeaponRequirement();
                    break;
            }

            return requirement;
        }

        private void DeleteEffectButton_Click(object sender, RoutedEventArgs e)
        {
            CombatantEffect effect = EffectListBox.SelectedItem as CombatantEffect;
            ApplicationData.Database.CurrentKitFeature.Effects.Remove(effect);
            ApplicationData.Database.CurrentKitFeature.CurrentEffect = null;
        }

        private void DeleteActionEffectButton_Click(object sender, RoutedEventArgs e)
        {
            CombatantEffect effect = ActionEffectListBox.SelectedItem as CombatantEffect;
            ApplicationData.Database.CurrentKitFeature.Action.Effects.Remove(effect);
            ApplicationData.Database.CurrentKitFeature.CurrentEffect = null;
        }

        private void AddPassiveEffectRequirementButton_Click(object sender, RoutedEventArgs e)
        {
            EffectRequirementType type = (EffectRequirementType)PassiveEffectRequirementComboBox.SelectedIndex;

            EffectRequirement requirement = GetNewEffectRequirement(type);

            ApplicationData.Database.CurrentKitFeature.Requirements.Add(requirement);
            ApplicationData.Database.CurrentKitFeature.CurrentRequirement = requirement;
        }

        private void DeletePassiveEffectRequirementButton_Click(object sender, RoutedEventArgs e)
        {
            EffectRequirement requirement = PassiveEffectRequirementListBox.SelectedItem as EffectRequirement;
            ApplicationData.Database.CurrentKitFeature.Requirements.Remove(requirement);
            ApplicationData.Database.CurrentKitFeature.CurrentEffect = null;
        }

        private void AddActionEffectRequirementButton_Click(object sender, RoutedEventArgs e)
        {
            EffectRequirementType type = (EffectRequirementType)ActionEffectRequirementComboBox.SelectedIndex;

            EffectRequirement requirement = GetNewEffectRequirement(type);

            ApplicationData.Database.CurrentKitFeature.Action.Requirements.Add(requirement);
            ApplicationData.Database.CurrentKitFeature.Action.CurrentRequirement = requirement;
        }

        private void DeleteActionEffectRequirementButton_Click(object sender, RoutedEventArgs e)
        {
            EffectRequirement requirement = ActionEffectRequirementListBox.SelectedItem as EffectRequirement;
            ApplicationData.Database.CurrentKitFeature.Action.Requirements.Remove(requirement);
            ApplicationData.Database.CurrentKitFeature.Action.CurrentEffect = null;
        }

        private void ActionSelectComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ActionType type = (ActionType)ActionSelectComboBox.SelectedIndex;

            switch (type)
            {
                case ActionType.AttackAction:
                    ApplicationData.Database.CurrentKitFeature.Action = new AttackAction();
                    return;
                default:
                    ApplicationData.Database.CurrentKitFeature.Action = new MyAction();
                    return;
            }
        }
    }
}
