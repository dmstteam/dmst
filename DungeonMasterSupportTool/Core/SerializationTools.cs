﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LIBS.Toolbox
{
    public static class SerializationTools
    {
        public static TObject ReadFromString<TObject>(string xmlData)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(TObject));
                XmlReader xmlReader = XmlReader.Create(new StringReader(xmlData));
                TObject created = ((TObject)serializer.Deserialize(xmlReader));
                return created;
            }
            catch
            {
                return default(TObject);
            }
        }

        public static void WriteToFile(string filePath, string data, bool overwriteReadOnly)
        {
            using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8))
            {
                writer.Write(data);
            }
        }

        public static void WriteToFile<TObject>(TObject obj, string filePath) where TObject : class
        {
            string contents = WriteToString<TObject>(obj);
            WriteToFile(filePath, contents, true);
        }

        public static string WriteToString<TObject>(TObject obj) where TObject : class
        {
            if (obj == null)
            {
                return String.Empty;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(TObject));
            using (Utf8StringWriter sw = new Utf8StringWriter())
            {
                serializer.Serialize(sw, obj);
                return sw.ToString();
            }
        }
    }

    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}
