﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMSTCore.Characters;
using DMSTCore.Items;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Actions;
using DMSTCore.Core;
using DMSTCore.Effects;
using DMSTCore.Kits;
using DMSTCore.Monsters;
using DMSTCore.Spells;

namespace DMSTCore
{
    [Serializable]
    public class Database : PropertyChangedNotifier
    {
        public Database()
        {
            //Race race = new Race();
            //race.Name = "Dwarf";
            //race.Size = SizeCategory.medium;
            //race.BaseGroundSpeed = new FloatDistance(25.0f, DistanceUnit.Feet);
            //race.AdditionalStats = new Attributes(0,0,2,0,0,0);
            //Races.Add(race);
        }

        [XmlIgnore]
        private ObservableCollection<Item> _items;

        [XmlElement]
        public ObservableCollection<Item> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                NotifyPropertyChanged();
            }
        }

        public Item GetItemFromKey(string key)
        {
            foreach (Item item in Items)
            {
                if (item.Key == key)
                {
                    return item;
                }
            }

            return null;
        }

        public Monster GetMonsterFromKey(string key) 
        {
            foreach (Monster monster in Monsters)
            {
                if (monster.Key == key)
                {
                    return monster;
                }
            }

            return null;
        }

        [XmlIgnore]
        public KitFeature _currentKitFeature = new KitFeature();

        [XmlIgnore]
        public KitFeature CurrentKitFeature
        {
            get
            {
                return _currentKitFeature;
            }
            set
            {
                _currentKitFeature = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public KitTemplate _currentKitTemplate = new KitTemplate();

        [XmlIgnore]
        public KitTemplate CurrentKitTemplate
        {
            get
            {
                return _currentKitTemplate;
            }
            set
            {
                _currentKitTemplate = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public SpellBook SpellBook = new SpellBook();

        [XmlIgnore]
        private ObservableCollection<Monster> _monsters;

        [XmlElement]
        public ObservableCollection<Monster> Monsters
        {
            get
            {
                return _monsters;
            }
            set
            {
                _monsters = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Monster _currentMonster;

        [XmlIgnore]
        public Monster CurrentMonster
        {
            get
            {
                return _currentMonster;
            }
            set
            {
                _currentMonster = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public List<PassiveAbility> PassiveAbilities;

        [XmlIgnore]
        private ObservableCollection<KitFeature> _kitFeatures;

        [XmlElement]
        public ObservableCollection<KitFeature> KitFeatures
        {
            get
            {
                return _kitFeatures;
            }
            set
            {
                _kitFeatures = value;
                NotifyPropertyChanged();
            }
        }

        public KitFeature GetKitFeature(string key)
        {
            foreach (KitFeature kitFeature in KitFeatures)
            {
                if (kitFeature.Key == key)
                {
                    return kitFeature;
                }
            }

            return null;
        }

        [XmlIgnore]
        private ObservableCollection<Icon> _characterIcons = new ObservableCollection<Icon>();

        [XmlElement]
        public ObservableCollection<Icon> CharacterIcons
        {
            get
            {
                return _characterIcons;
            }
            set
            {
                _characterIcons = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<KitTemplate> _kitTemplates;

        [XmlElement]
        public ObservableCollection<KitTemplate> KitTemplates
        {
            get
            {
                return _kitTemplates;
            }
            set
            {
                _kitTemplates = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<Race> _races = new ObservableCollection<Race>();

        [XmlElement]
        public ObservableCollection<Race> Races
        {
            get
            {
                return _races;
            }
            set
            {
                _races = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Race _currentRace;

        [XmlElement]
        public Race CurrentRace
        {
            get
            {
                return _currentRace;
            }
            set
            {
                _currentRace = value;
                NotifyPropertyChanged();
            }
        }
    }
}
