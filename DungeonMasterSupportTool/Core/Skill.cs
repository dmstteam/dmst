﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Core
{
    [Serializable]
    public class Skill : NameDescription
    {
        [XmlIgnore]
        private int _bonus;

        [XmlElement]
        public int Bonus
        {
            get
            {
                return _bonus;
            }
            set
            {
                _bonus = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private AttributeEnum _attribute;

        [XmlElement]
        public AttributeEnum Attribute
        {
            get
            {
                return _attribute;
            }
            set
            {
                _attribute = value;
                NotifyPropertyChanged();
            }
        }
    }
}
