﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DMSTCore.Characters;

namespace DMSTCore.Core
{
    [Serializable]
    public class CombatantGroup : NameDescription
    {
        public void AddMember(Combatant member)
        {
            foreach (string key in MemberKeys)
            {
                if (member.Key == key)
                {
                    return;
                }
            }

            MemberKeys.Add(member.Key);
            NotifyPropertyChanged("MemberKeys");
            NotifyPropertyChanged("Members");
            NotifyPropertyChanged("NumberOfMembers");
        }

        [XmlIgnore]
        ObservableCollection<string> _memberKeys = new ObservableCollection<string>();

        [XmlElement]
        public ObservableCollection<string> MemberKeys
        {
            get
            {
                return _memberKeys;
            }
            set
            {
                _memberKeys = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Members");
                NotifyPropertyChanged("NumberOfMembers");
            }
        }

        [XmlIgnore]
        public virtual ObservableCollection<Combatant> Members
        {
            get
            {
                ObservableCollection<Combatant> members = new ObservableCollection<Combatant>();

                if (ApplicationData.CurrentCampaign == null) { return members; }

                for (int i = MemberKeys.Count - 1; i >= 0; i--)
                {
                    string key = MemberKeys[i];
                    Character character = ApplicationData.CurrentCampaign.GetNPC(key);

                    if (character != null)
                    {
                        members.Add(character);
                    }
                    else
                    {
                        MemberKeys.RemoveAt(i);
                    }
                }

                return members;
            }
        }


        [XmlIgnore]
        public virtual int NumberOfMembers
        {
            get
            {
                return Members.Count;
            }
        }
    }
}
