﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore
{
    [Serializable]
    public class NameDescription : PropertyChangedNotifier
    {
        public NameDescription()
        {
            //_iconPath = @"..\Images\ItemIcon.png";
        }

        public override string ToString()
        {
            return Name;
        }

        [XmlIgnore]
        private string _name;

        [XmlElement]
        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private string _key;

        [XmlElement]
        public string Key
        {
            get
            {
                if (_key == null)
                {
                    _key = Guid.NewGuid().ToString();
                }

                return _key;
            }
            set
            {
                _key = value;
            }
        }

        [XmlIgnore]
        private string _description;

        [XmlElement]
        public virtual string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                NotifyPropertyChanged();
            }
        }


        [XmlIgnore]
        private string _iconPath;

        [XmlElement]
        public virtual string IconPath
        {
            get
            {
                return _iconPath;
            }
            set
            {
                _iconPath = value;
                NotifyPropertyChanged();
            }
        }
    }
}
