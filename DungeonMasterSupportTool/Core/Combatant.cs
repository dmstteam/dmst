﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Actions;
using DMSTCore.Core;
using DMSTCore.Characters;
using DMSTCore.Encounters;
using DMSTCore.Items;
using DMSTCore.Monsters;
using DMSTCore.Spells;

namespace DMSTCore
{
    [XmlInclude(typeof(Character))]
    [XmlInclude(typeof(Monster))]
    [Serializable]
    public abstract class Combatant : NameDescription
    {
        public Combatant()
        {
            IconPath = @"../Images/CharacterIcon.png";
            BaseStats = new Attributes();
        }

        protected void Stats_AttributeChanged(object sender, EventArgs e)
        {
            UpdateAllCalculatedFields();
        }

        protected void UpdateAllCalculatedFields()
        {
            NotifyPropertyChanged("Stats");
            NotifyPropertyChanged("SaveStr");
            NotifyPropertyChanged("SaveDex");
            NotifyPropertyChanged("SaveCon");
            NotifyPropertyChanged("SaveInt");
            NotifyPropertyChanged("SaveWis");
            NotifyPropertyChanged("SaveCha");
            NotifyPropertyChanged("HP");
            NotifyPropertyChanged("MaxHP");
            NotifyPropertyChanged("HitPointDisplay");
            NotifyPropertyChanged("AC");
            NotifyPropertyChanged("Perception");
        }

        [XmlIgnore]
        private int _age;

        [XmlElement]
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private string _playerName;

        [XmlElement]
        public string PlayerName
        {
            get
            {
                return _playerName;
            }
            set
            {
                _playerName = value;
                NotifyPropertyChanged();
            }
        }


        public virtual void SetStats(List<int> values)
        {
            BaseStats.Str = values[0];
            BaseStats.Dex = values[1];
            BaseStats.Con = values[2];
            BaseStats.Int = values[3];
            BaseStats.Wis = values[4];
            BaseStats.Cha = values[5];
        }

        [XmlIgnore]
        public bool IsCharacter
        {
            get
            {
                return (this is Character);
            }
        }

        [XmlIgnore]
        public bool IsMonster
        {
            get
            {
                return (this is Monster);
            }
        }

        [XmlIgnore]
        private Gender _gender;

        [XmlElement]
        public Gender Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        protected int _level;

        [XmlElement]
        public virtual int Level
        {
            get
            {
                return _level;
            }
            set
            {
                _level = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public string _religion = "None";

        [XmlElement]
        public string Religion
        {
            get
            {
                return _religion;
            }
            set
            {
                _religion = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<Sense> _senses = new ObservableCollection<Sense>();

        [XmlElement]
        public ObservableCollection<Sense> Senses
        {
            get
            {
                return _senses;
            }
            set
            {
                _senses = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<NameDescription> _languages = new ObservableCollection<NameDescription>();

        [XmlElement]
        public virtual ObservableCollection<NameDescription> Languages
        {
            get
            {
                return _languages;
            }
            set
            {
                _languages = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<Skill> _skills = new ObservableCollection<Skill>();

        [XmlElement]
        public ObservableCollection<Skill> Skills
        {
            get 
            {
                return _skills;
            }
            set
            {
                _skills = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public string SkillsDisplay
        {
            get
            {
                string display = String.Empty;

                for (int i = 0; i < Skills.Count; i++)
                {
                    display += String.Format("{0} +{1}",Skills[i].Name, Skills[i].Bonus);

                    if (i != Skills.Count - 1)
                    {
                        display += ",  ";
                    }
                }

                return display;
            }
        }

        [XmlIgnore]
        public string SensesDisplay
        {
            get
            {
                string display = String.Empty;

                for (int i = 0; i < Senses.Count; i++)
                {
                    display += String.Format("{0}", Senses[i].Name);

                    if (i != Senses.Count - 1)
                    {
                        display += ",  ";
                    }
                }

                return display;
            }
        }

        [XmlIgnore]
        public string LanguagesDisplay
        {
            get
            {
                return CommaDisplay(Languages);
            }
        }

        [XmlIgnore]
        public string DamageResistancesDisplay
        {
            get
            {
                return CommaDisplay(DamageResistances);
            }
        }

        [XmlIgnore]
        public string DamageImmunitiesDisplay
        {
            get
            {
                return CommaDisplay(DamageImmunities);
            }
        }

        [XmlIgnore]
        public string ConditionImmunitiesDisplay
        {
            get
            {
                return CommaDisplay(ConditionImmunities);
            }
        }

        private string CommaDisplay(ObservableCollection<NameDescription> list)
        {
            string display = String.Empty;

            for (int i = 0; i < list.Count; i++)
            {
                display += String.Format("{0}", list[i].Name);

                if (i != list.Count - 1)
                {
                    display += ",  ";
                }
            }
            return display;
        }

        [XmlIgnore]
        private Inventory _inventory;

        [XmlElement]
        public Inventory Inventory
        {
            get
            {
                if (_inventory == null)
                {
                    _inventory = new Inventory();
                }

                return _inventory;
            }
            set
            {
                _inventory = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        protected int _maxHP;

        [XmlElement]
        public virtual int MaxHP
        {
            get { return _maxHP; }
            set
            {
                _maxHP = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("HP");
                NotifyPropertyChanged("HitPointDisplay");
            }
        }

        public float HPPercent
        {
            get
            {
                if (MaxHP == 0)
                {
                    return 0;
                }

                return (float)HP / (float)MaxHP;
            }
        }

        [XmlElement]
        protected int _HP;

        public int HP
        {
            get
            {
                if (_HP > MaxHP)
                {
                    _HP = MaxHP;
                }

                return _HP;
            }
            set
            {
                _HP = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public string HitPointDisplay
        {
            get
            {
                return String.Format("{0}/{1}", HP, MaxHP);
            }
        }

        [XmlIgnore]
        private Alignment _alignment;

        [XmlElement]
        public Alignment Alignment
        {
            get
            {
                return _alignment;
            }
            set
            {
                _alignment = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _currentInitialiveRoll;

        [XmlIgnore]
        public int CurrentInitiativeRoll
        {
            get
            {
                return _currentInitialiveRoll;
            }
            set
            {
                _currentInitialiveRoll = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private SpellBook _spellBook = new SpellBook();

        [XmlElement]
        public SpellBook SpellBook
        {
            get
            {
                return _spellBook;
            }
            set
            {
                _spellBook = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _baseInitiativeModifier;

        [XmlElement]
        public int BaseInitiativeModifier
        {
            get
            {
                return _baseInitiativeModifier;
            }
            set
            {
                _baseInitiativeModifier = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public virtual int InitiativeModifier
        {
            get
            {
                return BaseInitiativeModifier + Stats.DexModifier;
            }
        }

        [XmlIgnore]
        private ObservableCollection<Proficiency> _baseProficiencies = new ObservableCollection<Proficiency>();

        [XmlElement]
        public ObservableCollection<Proficiency> BaseProficiencies
        {
            get
            {
                return _baseProficiencies;
            }
            set
            {
                _baseProficiencies = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Proficiencies");
            }
        }

        [XmlIgnore]
        public virtual ObservableCollection<Proficiency> Proficiencies
        {
            get
            {
                return _baseProficiencies;
            }
        }

        [XmlIgnore]
        public int SaveStr
        {
            get
            {
                return GetSave(AttributeEnum.Str);
            }
        }

        [XmlIgnore]
        private string _notes;

        [XmlElement]
        public string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public int SaveDex
        {
            get
            {
                return GetSave(AttributeEnum.Dex);
            }
        }

        [XmlIgnore]
        public int SaveCon
        {
            get
            {
                return GetSave(AttributeEnum.Con);
            }
        }

        [XmlIgnore]
        public int SaveInt
        {
            get
            {
                return GetSave(AttributeEnum.Int);
            }
        }

        [XmlIgnore]
        public int SaveWis
        {
            get
            {
                return GetSave(AttributeEnum.Wis);
            }
        }

        [XmlIgnore]
        public int SaveCha
        {
            get
            {
                return GetSave(AttributeEnum.Cha);
            }
        }

        public virtual int GetSave(AttributeEnum attribute)
        {
            int save = Stats.GetModifier(attribute);

            return save;
        }

        public virtual AOrD GetSaveAOrD(AttributeEnum attribute)
        {
            return AOrD.Normal;
        }

        public virtual void SetStats(Attributes stats)
        {
            Stats.SetAll(stats);
        }

        public virtual int GetStat(AttributeEnum stat)
        {
            return Stats.GetAttribute(stat);
        }

        public virtual int GetModifier(AttributeEnum stat)
        {
            return Stats.GetModifier(stat);
        }

        [XmlIgnore]
        private Attributes _baseStats;

        [XmlElement]
        public Attributes BaseStats 
        {
            get
            {
                if (!_statEventsSet)
                {
                    _baseStats.PropertyChanged += Stats_PropertyChanged;
                    _statEventsSet = true;
                }

                return _baseStats;
            }
            set
            {
                _baseStats = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Stats");
            } 
        }

        [XmlIgnore]
        private bool _statEventsSet = false;

        [XmlIgnore]
        public virtual Attributes Stats
        {
            get
            {
                return BaseStats;
            }
        }

        void Stats_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateAllCalculatedFields();
        }

        [XmlIgnore]
        private FloatDistance _groundSpeed = new FloatDistance();

        [XmlElement]
        public virtual FloatDistance GroundSpeed
        {
            get
            {
                return _groundSpeed;
            }
            set
            {
                _groundSpeed = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private FloatDistance _flySpeed = new FloatDistance();

        [XmlElement]
        public FloatDistance FlySpeed
        {
            get
            {
                return _flySpeed;
            }
            set
            {
                _flySpeed = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private FloatDistance _burrowSpeed = new FloatDistance();

        [XmlElement]
        public FloatDistance BurrowSpeed
        {
            get
            {
                return _burrowSpeed;
            }
            set
            {
                _burrowSpeed = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private FloatDistance _climbSpeed = new FloatDistance();

        [XmlElement]
        public FloatDistance ClimbSpeed
        {
            get
            {
                return _climbSpeed;
            }
            set
            {
                _climbSpeed = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private FloatDistance _swimSpeed = new FloatDistance();

        [XmlElement]
        public FloatDistance SwimSpeed
        {
            get
            {
                return _swimSpeed;
            }
            set
            {
                _swimSpeed = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<NameDescription> _damageImmunities = new ObservableCollection<NameDescription>();

        [XmlElement]
        public ObservableCollection<NameDescription> DamageImmunities
        {
            get
            {
                return _damageImmunities;
            }
            set
            {
                _damageImmunities = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<NameDescription> _conditionImmunities = new ObservableCollection<NameDescription>();

        [XmlElement]
        public ObservableCollection<NameDescription> ConditionImmunities
        {
            get
            {
                return _conditionImmunities;
            }
            set
            {
                _conditionImmunities = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<PassiveAbility> _passiveAbilities = new ObservableCollection<PassiveAbility>();

        [XmlElement]
        public ObservableCollection<PassiveAbility> PassiveAbilities
        {
            get
            {
                return _passiveAbilities;
            }
            set
            {
                _passiveAbilities = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private List<Condition> _conditions = new List<Condition>();

        [XmlElement]
        public List<Condition> Conditions
        {
            get
            {
                return _conditions;
            }
            set
            {
                _conditions = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _experiencePoints;

        [XmlElement]
        public int ExperiencePoints
        {
            get
            {
                if (_experiencePoints < 0) { _experiencePoints = 0; }

                return _experiencePoints;
            }
            set
            {
                _experiencePoints = value;
                NotifyPropertyChanged();
            }
        }

        public virtual void Damage(int damage)
        {
            _HP -= damage;
            NotifyPropertyChanged("HP");
            NotifyPropertyChanged("HitPointDisplay");
        }

        public virtual void Heal(int heal)
        {
            _HP += heal;

            if (_HP > MaxHP)
            {
                _HP = MaxHP;
            }

            NotifyPropertyChanged("HP");
            NotifyPropertyChanged("HitPointDisplay");
        }

        public virtual List<AttackParameters> GetAttackRolls()
        {
            return null;
        }

        public virtual List<Combatant> Targets { get; set; }

        public virtual bool RollSavingThrow(AOrD aOrD, AttributeEnum attribute, int DC, out int roll)
        {
            roll = Dice.RollD20(aOrD);
            roll += GetSave(attribute);

            if (roll >= DC)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [XmlElement]
        public virtual int NumberOfAttacksPerAttackAction
        {
            get
            {
                return 1;
            }
        }

        [XmlIgnore]
        public virtual ObservableCollection<MyAction> Actions
        {
            get
            {
                ObservableCollection<MyAction> actions = new ObservableCollection<MyAction>();

                // Add Dash Action
                string name = "Dash";
                string description = "Double your movement speed by spending an action.";
                DashAction dashAction = new DashAction(name, description);
                actions.Add(dashAction);

                // Add Disengage Action
                name = "Disengage";
                description = "Moving does not produce an attack of opportunity.";
                DisengageAction disengageAction = new DisengageAction(name, description);
                actions.Add(disengageAction);

                // Add Dodge Action
                name = "Dodge";
                description = "Gain advantage on Dex checks and attacks against you have disadvantage.";
                DodgeAction dodgeAction = new DodgeAction(name, description);
                actions.Add(dodgeAction);

                return actions;
            }
        }

        [XmlIgnore]
        private int _baseAc = 10;

        [XmlElement]
        public int BaseAC
        {
            get
            {
                return _baseAc;
            }
            set
            {
                _baseAc = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public virtual int AC
        {
            get
            {
                return BaseAC;
            }
        }

        [XmlIgnore]
        private int _miscACModifier;

        [XmlElement]
        public virtual int MiscACModifier
        {
            get
            {
                return _miscACModifier;
            }
            set
            {
                _miscACModifier = value;
                NotifyPropertyChanged();
            }
        }

        public virtual AOrD AOrDOnStrSave
        {
            get
            {
                return AOrD.Normal;
            }
        }

        public virtual AOrD AOrDOnDexSave
        {
            get
            {
                return AOrD.Normal;
            }
        }

        public virtual AOrD AOrDOnConSave
        {
            get
            {
                return AOrD.Normal;
            }
        }

        public virtual AOrD AOrDOnIntSave
        {
            get
            {
                return AOrD.Normal;
            }
        }

        public virtual AOrD AOrDOnWisSave
        {
            get
            {
                return AOrD.Normal;
            }
        }

        public virtual AOrD AOrDOnChaSave
        {
            get
            {
                return AOrD.Normal;
            }
        }


        public virtual AOrD HasAdvantageOnAttack
        {
            get
            {
                return AOrD.Normal;
            }
        }

        public virtual AOrD OpponentHasAdvantageOnAttackAgainstYou
        {
            get
            {
                return AOrD.Normal;
            }
        }

        [XmlIgnore]
        private ObservableCollection<NameDescription> _damageResistances = new ObservableCollection<NameDescription>();

        [XmlElement]
        public virtual ObservableCollection<NameDescription> DamageResistances
        {
            get
            {
                return _damageResistances;
            }
            set
            {
                _damageResistances = value;
                NotifyPropertyChanged();
            }
        }

        public virtual AOrD HasAdvantageOnInitiativeRoll
        {
            get
            {
                return AOrD.Normal;
            }
        }

        public virtual int ProficiencyBonus
        {
            get
            {
                return 0;
            }
        }

        [XmlIgnore]
        private string _backstory;

        [XmlElement]
        public string Backstory
        {
            get
            {
                return _backstory;
            }
            set
            {
                _backstory = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private string _futurePlans;

        [XmlElement]
        public string FuturePlans
        {
            get
            {
                return _futurePlans;
            }
            set
            {
                _futurePlans = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public int HitDiceMax
        {
            get
            {
                return Level;
            }
        }

        [XmlIgnore]
        private int _hitDice;

        [XmlElement]
        public int HitDice
        {
            get
            {
                if (_hitDice > HitDiceMax) { _hitDice = HitDiceMax; }
                if (_hitDice < 0) { _hitDice = 0; }

                return _hitDice;
            }
            set
            {
                _hitDice = value;
                NotifyPropertyChanged();
            }
        }

        public virtual void SpendHitDice(int dice)
        {
  
        }

        [XmlIgnore]
        private int _inspiration;

        [XmlElement]
        public int Inspiration
        {
            get
            {
                if (_inspiration < 0) { _inspiration = 0; }

                return _inspiration;
            }
            set
            {
                _inspiration = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public virtual int Perception
        {
            get
            {
                return Stats.WisModifier;
            }
        }

        public virtual int CriticalHitDamageRolls(Weapon weapon)
        {
            return 2;
        }

        public virtual bool CanProvokeAttackOfOpportunity
        {
            get
            {
                return true;
            }
        }
    }
}
