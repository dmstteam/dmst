﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using DMSTCore.Actions;

namespace DMSTCore
{
    public class CommunicationManagerCore
    {
        public delegate bool YesNoEventHandler(string text, string caption);
        public event YesNoEventHandler YesNoRequestHandler;
        public bool YesNoRequest(string text, string caption)
        {
            if (YesNoRequestHandler != null)
            {
                return YesNoRequestHandler(text, caption);
            }

            return false;
        }

        public delegate void RollEventHandler(Roll roll);
        public event RollEventHandler RollRequestHandler;
        public void RollRequest(Roll roll)
        {
            if (RollRequestHandler != null)
            {
                RollRequestHandler(roll);
            }
        }

        public delegate void AttackRollEventHandler(AttackParameters attackParameters);
        public event AttackRollEventHandler AttackRollRequestHandler;
        public void AttackRollRequest(AttackParameters attackParameters)
        {
            if (AttackRollRequestHandler != null)
            {
                AttackRollRequestHandler(attackParameters);
            }
        }

        public delegate void RequestTargetEventHandler(Object sender, RequestTargetEventArgs e);
        public event RequestTargetEventHandler RequestTargetHandler;
        public void TargetRequest(Object sender, RequestTargetEventArgs e)
        {
            if (RequestTargetHandler != null)
            {
                RequestTargetHandler(sender, e);
            }
        }
    }

    public class RequestTargetEventArgs : PropertyChangedNotifier
    {
        private ObservableCollection<Combatant> _possibleTargets = new ObservableCollection<Combatant>();

        public ObservableCollection<Combatant> PossibleTargets
        {
            get
            {
                return _possibleTargets;
            }
            set
            {
                _possibleTargets = value;
                NotifyPropertyChanged();
            }
        }

        private int _numberOfTargetsToSelect;

        public int NumberOfTargetsToSelect
        {
            get
            {
                return _numberOfTargetsToSelect;
            }
            set
            {
                _numberOfTargetsToSelect = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<Combatant> _selectedTargets = new ObservableCollection<Combatant>();

        public ObservableCollection<Combatant> SelectedTargets
        {
            get
            {
                return _selectedTargets;
            }
            set
            {
                _selectedTargets = value;
                NotifyPropertyChanged();
            }
        }
    }
}
