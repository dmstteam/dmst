﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore;

namespace DMSTCore.Core
{
    [Serializable]
    public class FloatDistance : PropertyChangedNotifier
    {
        public FloatDistance()
        {
            Value = 0;
            Unit = DistanceUnit.Feet;
        }

        public FloatDistance(float value, DistanceUnit unit)
        {
            Value = value;
            Unit = unit;
        }

        public override string ToString()
        {
            if (Unit == DistanceUnit.Feet)
            {
                return String.Format("{0} ft", Value);
            }
            else
            {
                return String.Format("{0} {1}", Value, Unit);
            }
        }

        [XmlIgnore]
        private float _value;

        [XmlElement]
        public float Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private DistanceUnit _unit;

        [XmlElement]
        public DistanceUnit Unit
        {
            get
            {
                return _unit;
            }
            set
            {
                _unit = value;
                NotifyPropertyChanged();
            }
        }
    }
}
