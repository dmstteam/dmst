﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace DMSTCore
{
    public enum Gender { Male, Female, Other}
    public enum AOrD { Normal, Advantage, Disadvantage }
    public enum ArmorType { None, Light, Medium, Heavy}
    //public enum Kit { Barbarian, Bard, Cleric, Druid, Fighter, Monk, Paladin, Ranger, Rogue, Sorceror, Warlock, Wizard}
    public enum Alignment { LawfulGood, LawfulNeutral, LawfulEvil, NeutralGood, TrueNeutral, NeutralEvil, ChaoticGood, ChaoticNeutral, ChaoticEvil}
    public enum WeaponType { SimpleMeleeWeapon, SimpleRangedWeapon, MartialMeleeWeapon, MartialRangedWeapon, Exotic, Shield }
    public enum DamageType { Bludgeoning, Piercing, Slashing, Acid, Fire}
    public enum WeaponProperty { Light, Finesse, Thrown, TwoHanded, Versatile, Reach, Heavy}
    public enum UIPage { Campaign, Quest, Encounter, EncounterEditor}
    public enum RemoteButton { Attack, Dodge, Sprint, Parry, AddEnemy, CastSpell}
    public enum Condition { Blinded, Charmed, Deafened, Exhausted, Frightened, Incapacitated, Unconscious}
    public enum MonsterType { Aberration, Beast, Celestial, Construct, Dragon, Elemental, Fey, Fiend, Giant, Humanoid, Monstrosity, Ooze, Plant, Undead}
    public enum SizeCategory { Tiny, Small, medium, Large, Huge, Gargantuan}
    public enum TimeUnit { Instantaneous, Reaction, Action, Round, Turn, ShortRest, LongRest, Second, Minute, Hour, Day, Year, Permanent }
    public enum BattleStatus { Normal, Dodging, Parrying, Concentrating}
    public enum MagicSchool { Abjuration, Conjuration, Divination, Enchantment, Evocation, Illusion, Necromancy, Transmutation}
    public enum DistanceUnit { Self, Touch, Feet, Mile, Yard, Special, Sight, Unlimited }
    public enum Shape { Line, Cone, Cube, Sphere, Cylinder }
    public enum SubKit { None, PathOfTheBerserker, PathOfTheTotemWarrior}
    public enum AttributeEnum { Str, Dex, Con, Int, Wis, Cha}
    public enum EffectType { AdditionalAC, AttackBonus, BonusDamage, MoveSpeed, AdvantageOnSave, AdvantageOnAttack, DamageResistance, ImproveAbilityScore, AdditionalAttack, InitiativeRoll, SetHP}
    public enum EffectRequirementType { ArmorRequirement, ConditionRequirement, ActiveAction, SaveRequirement, HPRequirement, WeaponRequirement }
    public enum ActionType { Regular, AttackAction}
    public enum MapObjectType { Alchemist, Castle, City, Church, Dungeon, Library, Tavern, Theater }
    public enum ItemType { Item, Weapon, Armor, Shield, Head, Neck, Hands, Wrist, Feet, Finger, Back, Waist, Legs}

    public static class EnumHelper
    {
        public static string ToString<T>(T type)
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            return r.Replace(type.ToString(), " ");
        }

        public static List<String> EnumToListofStrings<T>()
        {
            var types = Enum.GetValues(typeof(T));
            List<String> values = new List<string>();
            foreach (T type in types)
            {
                values.Add(ToString<T>(type));
            }

            return values;
        }

        /// <summary>
        /// Returns 1 if advantage, 0 if normal, -1 if disadvantage
        /// </summary>
        /// <param name="aord"></param>
        /// <returns></returns>
        public static int IsAdvantageDisadvantageOrNormal(AOrD aord)
        {
            if (aord == AOrD.Advantage)
            {
                return 1;
            }
            else if (aord == AOrD.Disadvantage)
            {
                return -1;
            }

            return 0;
        }

        /// <summary>
        /// return advantage is balance greater than 0, disadvatange if balance less than 0 and normal if equal.
        /// </summary>
        /// <param name="kit"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static AOrD GetAdvantageDisadvantageNormalFromBalance(int balance)
        {
            if (balance > 0)
            {
                return AOrD.Advantage;
            }
            else if (balance < 0)
            {
                return AOrD.Disadvantage;
            }
            else
            {
                return AOrD.Normal;
            }
        }
    }
}
