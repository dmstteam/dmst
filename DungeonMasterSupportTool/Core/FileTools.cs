﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace LIBS.Toolbox
{
    public static class FileTools
    {
        public static string ReadFromFile(string filePath)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                return reader.ReadToEnd();
            }
        }

        public static TObject ReadFromFile<TObject>(string filePath)
        {
            try
            {
                string data = ReadFromFile(filePath);
                return SerializationTools.ReadFromString<TObject>(data);
            }
            catch
            {
                return default(TObject);
            }
        }

        public static void WriteToFile(string filePath, string data, bool overwriteReadOnly)
        {
            using (StreamWriter writer = new StreamWriter(filePath, false, Encoding.UTF8))
            {
                writer.Write(data);
            }
        }

        public static void WriteToBinaryFile<TObject>(TObject obj, string filePath) where TObject : class
        {
            using (FileStream stream = File.Create(filePath))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Close();
            }
        }

        public static TObject ReadFromBinaryFile<TObject>(string filePath) where TObject : class
        {
            using (FileStream inStr = new FileStream(filePath, FileMode.Open))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                TObject obj = formatter.Deserialize(inStr) as TObject;
                inStr.Close();
                return obj;
            }
        }

        public static void WriteToFile<TObject>(TObject obj, string filePath) where TObject : class
        {
            string contents = SerializationTools.WriteToString<TObject>(obj);
            WriteToFile(filePath, contents, true);
        }

        public static void CreateDirectoryIfNotPresent(string directory)
        {
            try
            {
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
            catch (Exception)
            {
            }
        }

        public static void DeleteDirectory(string directory)
        {
            string[] files = Directory.GetFiles(directory);
            string[] dirs = Directory.GetDirectories(directory);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(directory, false);
        }
    }
}
