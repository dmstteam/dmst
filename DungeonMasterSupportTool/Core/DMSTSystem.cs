﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DMSTCore.Campaigns;
using LIBS.Toolbox;


namespace DMSTCore
{
    public class DMSTSystem : PropertyChangedNotifier
    {
        public EventHandler SaveCampaignRequest;

        private string _currentPage;

        public string CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                _currentPage = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsNotCampaignPage
        {
            get
            {
                return !String.Equals(CurrentPage, "Campaign");
            }
        }

        public bool IsSaving;

        public void SaveCampaign()
        {
            if (ApplicationData.CurrentCampaign == null) { return; }

            SaveCampaignRequest?.Invoke(new object(), new EventArgs());

            IsSaving = true;

            string path = Path.Combine(CampaignSaveLocation, ApplicationData.CurrentCampaign.Filename);
            FileTools.WriteToFile<Campaign>(ApplicationData.CurrentCampaign, path);

            IsSaving = false;
        }

        public List<Campaign> AllSavedCampaigns
        {
            get
            {
                List<Campaign> campaigns = new List<Campaign>();
                string[] files = Directory.GetFiles(CampaignSaveLocation);
                foreach (string file in files)
                {
                    try
                    {
                        Campaign campaign = FileTools.ReadFromFile<Campaign>(file);
                        if (campaign != null)
                        {
                            campaigns.Add(campaign);
                        }
                    }
                    catch
                    {

                    }
                }

                return campaigns;
            }
        }

        private string CampaignSaveLocation
        {
            get
            {
                string campaignDirectory = Path.Combine(Environment.CurrentDirectory, "Campaigns");

                if (!Directory.Exists(campaignDirectory))
                {
                    Directory.CreateDirectory(campaignDirectory);
                }

                return campaignDirectory;
            }
        }

        public string MapStrokeFileSaveLocation
        {
            get
            {
                string mapDirectory = Path.Combine(Environment.CurrentDirectory, "Maps");

                if (!Directory.Exists(mapDirectory))
                {
                    Directory.CreateDirectory(mapDirectory);
                }

                return mapDirectory;
            }
        }

        public void AutoLoadCampaign()
        {
            LoadCampaign("Autosave.xml");
        }

        public void LoadCampaign(string filename)
        {
            string campaignDirectory = Path.Combine(Environment.CurrentDirectory, "Campaigns");
            campaignDirectory = Path.Combine(campaignDirectory, filename);
            Campaign campaign = FileTools.ReadFromFile<Campaign>(campaignDirectory);

            if (campaign != null)
            {
                ApplicationData.CurrentCampaign = campaign;
            }
        }
    }
}
