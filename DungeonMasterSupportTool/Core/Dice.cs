﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSTCore
{
    public static class Dice
    {
        private static Random rand = new Random();

        public static string FormatDiceString(int timesToRoll, int die, int additional)
        {
            if (additional == 0)
            {
                return String.Format("{0}d{1}", timesToRoll, die);
            }
            else if (additional > 0)
            {
                return String.Format("{0}d{1} + {2}", timesToRoll, die, additional);
            }
            else
            {
                return String.Format("{0}d{1} - {2}", timesToRoll, die, -additional);
            }
        }

        public static int Roll(int die, int dice, int totalModifier)
        {
            int roll = 0;
            for (int i = 0; i < dice; i++)
            {
                roll += Roll(die);
            }

            roll += totalModifier;

            return roll;
        }

        public static int RollD100()
        {
            return rand.Next(100) + 1;
        }

        public static int RollD20()
        {
            return rand.Next(20) + 1;
        }

        public static int RollD20(AOrD aord)
        {
            int roll = RollD20();
            int roll2 = RollD20();

            if (aord == AOrD.Advantage)
            {
                if (roll2 > roll) { roll = roll2; }
            }
            else if (aord == AOrD.Disadvantage)
            {
                if (roll2 < roll) { roll = roll2; }
            }

            return roll;
        }

        public static int RollD12()
        {
            return rand.Next(12) + 1;
        }

        public static int RollD10()
        {
            return rand.Next(10) + 1;
        }

        public static int RollD8()
        {
            return rand.Next(8) + 1;
        }

        public static int RollD6()
        {
            return rand.Next(6) + 1;
        }

        public static int RollD4()
        {
            return rand.Next(4) + 1;
        }

        /// <summary>
        /// Returns roll of specified dice.  Lowest possible value is 1.
        /// </summary>
        /// <param name="dice"></param>
        /// <returns></returns>
        public static int Roll(int dice)
        {
            return rand.Next(dice) + 1;
        }

        public static List<int> Roll3Stats()
        {
            List<int> values = new List<int>();

            for (int i = 0; i < 6; i++)
            {
                values.Add(RollD6() + RollD6() + RollD6());
            }

            return values;
        }

        public static List<int> Roll4Drop1Stats()
        {
            List<int> values = new List<int>();

            for (int i = 0; i < 6; i++)
            {
                List<int> rolls = new List<int>();
                for (int j = 0; j < 4; j++)
                {
                    rolls.Add(RollD6());
                }

                values.Add(rolls.Sum() - rolls.Min());
            }

            return values;
        }
    }
}
