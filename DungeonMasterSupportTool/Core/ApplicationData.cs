﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using LIBS.Toolbox;

using DMSTCore.Effects;
using DMSTCore.Campaigns;
using DMSTCore.Characters;
using DMSTCore.Encounters;
using DMSTCore.Kits;
using DMSTCore.Items;
using DMSTCore.Monsters;
using DMSTCore.Spells;

namespace DMSTCore
{
    public static class ApplicationData
    {
        private static DMSTSystem _system;

        public static DMSTSystem System
        {
            get
            {
                if (_system == null)
                {
                    _system = new DMSTSystem();
                }

                return _system;
            }
        }

        private static CommunicationManagerCore _communication;

        public static CommunicationManagerCore Communication
        {
            get
            {
                if (_communication == null)
                {
                    _communication = new CommunicationManagerCore();
                }

                return _communication;
            }
        }

        public static Campaign CurrentCampaign { get; set; }

        public static Combatant CurrentCombatant;

        public static Quest CurrentQuest
        {
            get
            {
                if (CurrentCampaign != null)
                {
                    return CurrentCampaign.CurrentQuest;
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (CurrentCampaign != null)
                {
                    CurrentCampaign.CurrentQuest = value;
                }
            }
        }

        public static Encounter CurrentEncounter
        {
            get
            {
                if (CurrentQuest != null)
                {
                    return CurrentQuest.CurrentEncounter;
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (CurrentQuest != null)
                {
                    CurrentQuest.CurrentEncounter = value;
                }
            }
        }

        public static Round CurrentRound
        {
            get
            {
                if (CurrentEncounter != null)
                {
                    if (CurrentEncounter.CurrentRound == null)
                    {
                        CurrentEncounter.CurrentRound = new Round();
                    }

                    return CurrentEncounter.CurrentRound;
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (CurrentEncounter != null)
                {
                    CurrentEncounter.CurrentRound = value;
                }
            }
        }

        private static string _databasePath = "DataBase.xml";

        public static Database Database { get; set; }

        public static void SaveDatabase()
        {
            string path = Path.Combine(Environment.CurrentDirectory, _databasePath);
            FileTools.WriteToFile<Database>(Database, path);
        }

        public static void LoadDatabase()
        {
            string path = Path.Combine(Environment.CurrentDirectory, _databasePath);

            bool exists = File.Exists(path);

            Database = FileTools.ReadFromFile<Database>(path);
        }

        public static KitTemplate GetKitTemplateFromKey(string templateKey)
        {
            foreach (KitTemplate template in Database.KitTemplates)
            {
                if (template.Key == templateKey)
                {
                    return template;
                }
            }

            return null;
        }

        public static KitTemplate GetKitTemplateFromName(string templateName)
        {
            foreach (KitTemplate template in Database.KitTemplates)
            {
                if (template.Name == templateName)
                {
                    return template;
                }
            }

            return null;
        }

        public static Race GetRaceFromName(string raceName)
        {
            foreach (Race race in Database.Races)
            {
                if (race.Name == raceName)
                {
                    return race;
                }
            }

            return null;
        }

        public static Race GetRaceFromKey(string raceKey)
        {
            foreach (Race race in Database.Races)
            {
                if (race.Key == raceKey)
                {
                    return race;
                }
            }

            return null;
        }

        public static ObservableCollection<KitTemplate> GetKitTemplates()
        {
            return Database.KitTemplates;
        }

        public static void AddToDatabase(Item item)
        {
            if (Database == null)
            {
                Database = new Database();
            }

            if (Database.Items == null)
            {
                Database.Items = new ObservableCollection<Item>();
            }

            Database.Items.Add(item);

            SaveDatabase();
            LoadDatabase();
        }

        public static void ClearSpellsFromDatabase()
        {
            if (Database == null)
            {
                Database = new Database();
            }

            Database.SpellBook.Spells.Clear();

            SaveDatabase();
            LoadDatabase();
        }

        public static void AddToDatabase(Spell spell)
        {
            Database.SpellBook.AddSpell(spell);

            SaveDatabase();
            LoadDatabase();
        }

        public static Item GetItem(string name)
        {
            foreach (Item item in Database.Items)
            {
                if (item.Name.Equals(name))
                {
                    return item;
                }
            }

            return null;
        }

        public static Armor GetRandomArmor(List<ArmorType> armorTypes)
        {
            var armors = GetAllArmors(armorTypes);
            int index = Dice.Roll(armors.Count)-1;

            if (armors.Count == 0)
            {
                return null;
            }
            
            return armors[index];
        }

        public static Armor GetRandomArmor()
        {
            var armors = GetAllArmors();
            int index = Dice.Roll(armors.Count)-1;
            return armors[index];
        }

        public static Weapon GetRandomWeapon(List<WeaponProficiency> weaponProficiencies)
        {
            var weapons = GetAllWeapons(weaponProficiencies);
            int index = Dice.Roll(weapons.Count)-1;

            if (weapons.Count == 0)
            {
                return null;
            }

            return weapons[index];
        }

        public static Weapon GetRandomWeapon()
        {
            var weapons = GetAllWeapons();
            int index = Dice.Roll(weapons.Count)-1;
            return weapons[index];
        }

        public static List<Armor> GetAllArmors(List<ArmorType> armorTypes)
        {
            List<Armor> armors = new List<Armor>();
            foreach (Item item in Database.Items)
            {
                if (item is Armor)
                {
                    bool addToList = false;
                    foreach (ArmorType type in armorTypes)
                    {
                        if (type == (item as Armor).Type)
                        {
                            addToList = true;
                            break;
                        }
                    }

                    if (addToList)
                    {
                        armors.Add(item as Armor);
                    }
                }
            }

            return armors;
        }

        public static List<Armor> GetAllArmors()
        {
            if (Database == null) { return null; }

            List<Armor> armors = new List<Armor>();
            foreach (Item item in Database.Items)
            {
                if (item is Armor)
                {
                    armors.Add(item as Armor);
                }
            }

            return armors;
        }

        public static List<Weapon> GetAllWeapons()
        {
            if (Database == null) { return null; }

            List<Weapon> weapons = new List<Weapon>();
            foreach (Item item in Database.Items)
            {
                if (item is Weapon)
                {
                    weapons.Add(item as Weapon);
                }
            }

            return weapons;
        }

        public static List<Weapon> GetAllWeapons(List<WeaponProficiency> weaponProficiencies)
        {
            List<Weapon> weapons = new List<Weapon>();
            foreach (Item item in Database.Items)
            {
                if (item is Weapon)
                {
                    foreach (WeaponProficiency p in weaponProficiencies)
                    {
                        if (p.Weapon == null)
                        {
                            if (p.Type == (item as Weapon).WeapType)
                            {
                                weapons.Add(item as Weapon);
                            }
                        }
                        else
                        {
                            if (p.Weapon.Name.Equals(item.Name))
                            {
                                weapons.Add(item as Weapon);
                            }
                        }
                    }
                }
            }

            return weapons;
        }

        public static SpellBook GetSpellBook()
        {
            return Database.SpellBook;
        }

        public static void EditItemInDatabase(Armor armor)
        {
            var armors = GetAllArmors();

            foreach (Armor x in armors)
            {
                if (armor.Name.Equals(x.Name))
                {
                    x.BaseAC = armor.BaseAC;
                    x.Cost = armor.Cost;
                    x.Description = armor.Description;
                    x.Name = armor.Name;
                    x.OnStealth = armor.OnStealth;
                    x.RequiredStrength = armor.RequiredStrength;
                    x.Type = armor.Type;
                    x.Weight = armor.Weight;

                    break;
                }
            }

            SaveDatabase();
            LoadDatabase();
        }

        public static void EditItemInDatabase(Weapon weapon)
        {
            var weapons = GetAllWeapons();

            foreach (Weapon x in weapons)
            {
                if (weapon.Name.Equals(x.Name))
                {
                    x.AddedDamage = weapon.AddedDamage;
                    x.Cost = weapon.Cost;
                    x.DamageDie = weapon.DamageDie;
                    x.Description = weapon.Description;
                    x.Name = weapon.Name;

                    x.Properties = new List<WeaponProperty>();
                    foreach (WeaponProperty prop in weapon.Properties)
                    {
                        x.Properties.Add(prop);
                    }

                    x.TimesToRoll = weapon.TimesToRoll;
                    x.WeapType = weapon.WeapType;
                    x.DamageType = weapon.DamageType;
                    x.Weight = weapon.Weight;

                    break;
                }
            }

            SaveDatabase();
            LoadDatabase();
        }

        public static void EditSpellInDatabase(Spell spell)
        {
            var spells = Database.SpellBook.Spells;

            foreach (Spell x in spells)
            {
                if (spell.Name.Equals(x.Name))
                {
                    x.CastingTime = spell.CastingTime;
                    x.CastingTimeUnits = spell.CastingTimeUnits;
                    x.Description = spell.Description;
                    x.KitsThatHaveAccess = spell.KitsThatHaveAccess;
                    x.Name = spell.Name;
                    x.SpellLevel = spell.SpellLevel;

                    break;
                }
            }

            SaveDatabase();
            LoadDatabase();
        }

        public static ObservableCollection<Item> GetAllItems()
        {
            if (Database == null) { return null; }

            return Database.Items;
        }

        public static void RemoveFromDatabase(Item item)
        {
            for (int i = 0; i < Database.Items.Count; i++)
            {
                if (item.Name == Database.Items[i].Name)
                {
                    Database.Items.RemoveAt(i);
                    break;
                }
            }

            SaveDatabase();
            LoadDatabase();
        }

        public static void RemoveFromDatabase(Spell spell)
        {
            List<Spell> spells = Database.SpellBook.Spells;
            for (int i = 0; i < spells.Count; i++)
            {
                if (spell.Name == spells[i].Name)
                {
                    spells.RemoveAt(i);
                    break;
                }
            }

            SaveDatabase();
            LoadDatabase();
        }

        public static void AddToDatabase(Monster monster)
        {
            if (Database == null)
            {
                Database = new Database();
            }

            if (Database.Items == null)
            {
                Database.Items = new ObservableCollection<Item>();
            }

            Database.Monsters.Add(monster);

            SaveDatabase();
            LoadDatabase();
        }

        public static void AddToDatabase(KitFeature feature)
        {
            if (Database == null)
            {
                Database = new Database();
            }

            if (Database.KitFeatures == null)
            {
                Database.KitFeatures = new ObservableCollection<KitFeature>();
            }

            Database.KitFeatures.Add(feature);

            SaveDatabase();
            LoadDatabase();
        }
    }
}
