﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore
{
    [Serializable]
    public class Icon : NameDescription
    {
        public Icon() { }

        public Icon(string iconPath)
        {
            IconPath = iconPath;
        }
    }
}
