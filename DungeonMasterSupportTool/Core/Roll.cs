﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace DMSTCore
{
    public class Roll : PropertyChangedNotifier
    {
        private int _die;

        public int Die
        {
            get
            {
                return _die;
            }
            set
            {
                _die = value;
                NotifyPropertyChanged();
            }
        }

        private int _dice;

        public int Dice
        {
            get
            {
                return _dice;
            }
            set
            {
                _dice = value;
                NotifyPropertyChanged();
            }
        }

        private int _modifier;

        public int Modifier
        {
            get
            {
                return _modifier;
            }
            set
            {
                _modifier = value;
                NotifyPropertyChanged();
            }
        }

        private ObservableCollection<int> _results = new ObservableCollection<int>();

        public ObservableCollection<int> Results
        {
            get
            {
                return _results;
            }
            set
            {
                _results = value;
                NotifyPropertyChanged();
            }
        }
    }
}
