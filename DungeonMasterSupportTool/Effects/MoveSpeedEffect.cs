﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Characters;
using DMSTCore.Items;

namespace DMSTCore.Effects
{
    [Serializable]
    public class MoveSpeedEffect : CombatantEffect
    {
        public MoveSpeedEffect()
        {
            Name = "Move Speed";
            _groundSpeedMultiplier = 1;
        }

        [XmlIgnore]
        private int _groundSpeed;

        [XmlElement]
        public int GroundSpeed
        {
            get
            {
                return _groundSpeed;
            }
            set
            {
                _groundSpeed = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _groundSpeedMultiplier;

        [XmlElement]
        public int GroundSpeedMultiplier
        {
            get
            {
                return _groundSpeedMultiplier;
            }
            set 
            {
                _groundSpeedMultiplier = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private bool _ignoreAOO;

        [XmlElement]
        public bool IgnoreAOO
        {
            get
            {
                return _ignoreAOO;
            }
            set
            {
                _ignoreAOO = value;
                NotifyPropertyChanged();
            }
        }

        public override int AdditionalGroundSpeed(Character character)
        {
            return GroundSpeed;
        }

        public override int MultiplyGroundSpeed(Character character)
        {
            return GroundSpeedMultiplier;
        }

        public override bool IgnoreAttackOfOpportunity(Character character)
        {
            return IgnoreAOO;
        }
    }
}
