﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Characters;

namespace DMSTCore.Effects
{
    public class DamageResistanceEffect : CombatantEffect
    {
        public override string Name
        {
            get
            {
                return String.Format("Resistance: {0}", EnumHelper.ToString<DamageType>(DamageType));
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private DamageType _damageType;

        [XmlElement]
        public DamageType DamageType
        {
            get
            {
                return _damageType;
            }
            set
            {
                _damageType = value;
                NotifyPropertyChanged();
            }
        }

        public override List<DamageType> AdditionalDamageResistance(Character character)
        {
            List<DamageType> list = new List<DamageType>();

            list.Add(DamageType);

            return list;
        }
    }
}
