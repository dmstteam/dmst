﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Effects
{
    [Serializable]
    public class InitiativeRollEffect : CombatantEffect
    {
        public override string Name
        {
            get
            {
                return String.Format("{0} On Initiative Roll",AdvantageOrDisadvantage);
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private AOrD _advantageOrDisadvantage;

        [XmlElement]
        public AOrD AdvantageOrDisadvantage
        {
            get
            {
                return _advantageOrDisadvantage;
            }
            set
            {
                _advantageOrDisadvantage = value;
                NotifyPropertyChanged();
            }
        }

        public override AOrD HasAdvantageOnInitiativeRoll(Characters.Character character)
        {
            return AdvantageOrDisadvantage;
        }
    }
}
