﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Effects
{
    [Serializable]
    public class SetHPEffect : CombatantEffect
    {
        public override string Name
        {
            get
            {
                return String.Format("Set HP {0}", HP);
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private int _hP;

        [XmlElement]
        public int HP
        {
            get
            {
                return _hP;
            }
            set
            {
                _hP = value;
                NotifyPropertyChanged();
            }
        }

        public override void Damage(Characters.Character character)
        {
            base.Damage(character);

            character.HP = HP;
        }
    }
}
