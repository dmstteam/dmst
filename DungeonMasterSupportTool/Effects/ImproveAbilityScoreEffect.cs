﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Characters;

namespace DMSTCore.Effects
{
    [Serializable]
    public class ImproveAbilityScoreEffect : CombatantEffect
    {
        public override string Name
        {
            get
            {
                return "Improve Ability Score";
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlElement]
        public int UserSelected { get; set; }

        [XmlIgnore]
        int _pointsToGain { get; set; }

        [XmlElement]
        public int PointsToGain
        {
            get
            {
                return _pointsToGain;
            }
            set
            {
                _pointsToGain = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        AttributeEnum _stat;

        [XmlElement]
        public AttributeEnum AttributeGained
        {
            get
            {
                return _stat;
            }
            set
            {
                _stat = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        private List<AttributeEnum> gainedAttributes = new List<AttributeEnum>();

        public override int AdditionalAttributes(Combatant combatant, AttributeEnum stat)
        {
            int add = base.AdditionalAttributes(combatant, stat);

            foreach (AttributeEnum att in gainedAttributes)
            {
                if (stat == att)
                {
                    add++;
                }
            }

            return add;
        }
    }
}
