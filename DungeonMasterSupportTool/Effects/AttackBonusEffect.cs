﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Characters;
using DMSTCore.Items;

namespace DMSTCore.Effects
{
    [Serializable]
    public class AttackBonusEffect : CombatantEffect
    {
        public AttackBonusEffect()
        {
            Name = "Attack Bonus";
        }

        [XmlIgnore]
        private int _attackBonusMainHand;

        [XmlElement]
        public int AttackBonusMainHandAdditional
        {
            get
            {
                return _attackBonusMainHand;
            }
            set
            {
                _attackBonusMainHand = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _attackBonusOffHand;

        [XmlElement]
        public int AttackBonusOffHandAdditional
        {
            get
            {
                return _attackBonusOffHand;
            }
            set
            {
                _attackBonusOffHand = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public List<WeaponType> AllowedWeaponTypes;

        public override int AdditionalMainHandAttackBonus(Character character)
        {
            foreach (WeaponType type in AllowedWeaponTypes)
            {
                if (type == character.WeaponMainHand.WeapType)
                {
                    return AttackBonusMainHandAdditional;
                }
            }

            return 0;
        }

        public override int AdditionalOffHandAttackBonus(Character character)
        {
            foreach (WeaponType type in AllowedWeaponTypes)
            {
                if (type == character.WeaponOffHand.WeapType)
                {
                    return AttackBonusOffHandAdditional;
                }
            }

            return 0;
        }
    }
}
