﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSTCore.Effects
{
    public class AdditionalAttackEffect : CombatantEffect
    {
        public override string Name
        {
            get
            {
                return "Additional Attack";
            }
            set
            {
                base.Name = value;
            }
        }

        public override int AdditionalAttacks(Combatant combatant)
        {
            return 1;
        }
    }
}
