﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Characters;

namespace DMSTCore.Effects
{
    [Serializable]
    public class AdvantageOnSaveEffect : CombatantEffect
    {
        public override string Name
        {
            get
            {
                return String.Format("Advantage: {0}", Stat);
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private AttributeEnum _stat;

        [XmlElement]
        public AttributeEnum Stat
        {
            get
            {
                return _stat;
            }
            set
            {
                _stat = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private AOrD _aOrD;

        [XmlElement]
        public AOrD AOrD
        {
            get
            {
                return _aOrD;
            }
            set
            {
                _aOrD = value;
                NotifyPropertyChanged();
            }
        }

        private AOrD AOrDOnSave(Character character)
        {
            return AOrD;
        }

        public override AOrD AOrDOnStrSave(Character character)
        {
            if (Stat == AttributeEnum.Str)
            {
                return AOrDOnSave(character);
            }
            else
            {
                return DMSTCore.AOrD.Normal;
            }
        }

        public override AOrD AOrDOnDexSave(Character character)
        {
            if (Stat == AttributeEnum.Dex)
            {
                return AOrDOnSave(character);
            }
            else
            {
                return DMSTCore.AOrD.Normal;
            }
        }

        public override AOrD AOrDOnConSave(Character character)
        {
            if (Stat == AttributeEnum.Con)
            {
                return AOrDOnSave(character);
            }
            else
            {
                return DMSTCore.AOrD.Normal;
            }
        }

        public override AOrD AOrDOnIntSave(Character character)
        {
            if (Stat == AttributeEnum.Int)
            {
                return AOrDOnSave(character);
            }
            else
            {
                return DMSTCore.AOrD.Normal;
            }
        }

        public override AOrD AOrDOnWisSave(Character character)
        {
            if (Stat == AttributeEnum.Wis)
            {
                return AOrDOnSave(character);
            }
            else
            {
                return DMSTCore.AOrD.Normal;
            }
        }

        public override AOrD AOrDOnChaSave(Character character)
        {
            if (Stat == AttributeEnum.Cha)
            {
                return AOrDOnSave(character);
            }
            else
            {
                return DMSTCore.AOrD.Normal;
            }
        }
    }
}
