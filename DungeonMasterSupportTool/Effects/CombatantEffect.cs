﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Xml.Serialization;
using DMSTCore.Items;
using DMSTCore.Characters;
using DMSTCore.GameEngine;

namespace DMSTCore.Effects
{
    [Serializable]
    public abstract class CombatantEffect : NameDescription
    {
        [XmlIgnore]
        private Engine _combatantEffectEngine = new Engine();
        
        [XmlElement]
        public Engine CombatantEffectEngine
        {
            get
            {
                return _combatantEffectEngine;
            }
            set
            {
                _combatantEffectEngine = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public virtual int ActiveAtLevel { get; set; }

        public virtual int AdditionalCriticalHitDamageRolls(Character character) { return 0; }

        public virtual int AdditionalCriticalHitDamageRollsOffHand(Character character) { return 0; }

        public virtual int AdditionalGroundSpeed(Character character) { return CombatantEffectEngine.GetCombatantAugmentation(character, Augmentation.AddSubtract, Metric.GroundSpeed); }

        public virtual int MultiplyGroundSpeed(Character character) { return 1; }

        public virtual int AdditionalAbilityCheckModifier(Character character, Proficiency proficiency) { return 0; }

        /// <summary>
        /// Damage the character who owns this feature
        /// </summary>
        /// <param name="damage"></param>
        public virtual void Damage(Character character) { }

        public virtual int AdditionalMainHandAttackBonus(Character character) { return 0; }

        public virtual int AdditionalOffHandAttackBonus(Character character) { return 0; }

        public virtual int AdditionalMainHandDamage(Character character) { return 0; }

        public virtual int AdditionalOffHandDamage(Character character) { return 0; }

        public virtual int AdditionalAC(Character character) { return 0; }

        public virtual int AdditionalAttributes(Combatant combatant, AttributeEnum stat) { return 0; }

        public virtual int AdditionalAttacks(Combatant combatant) { return 0; }

        public virtual AOrD AOrDOnStrSave(Character character) { return AOrD.Normal; }

        public virtual AOrD AOrDOnDexSave(Character character) { return AOrD.Normal; }

        public virtual AOrD AOrDOnConSave(Character character) { return AOrD.Normal; }

        public virtual AOrD AOrDOnIntSave(Character character) { return AOrD.Normal; }

        public virtual AOrD AOrDOnWisSave(Character character) { return AOrD.Normal; }

        public virtual AOrD AOrDOnChaSave(Character character) { return AOrD.Normal; }

        public virtual AOrD HasAdvantageOnMainHandAttack(Character character) { return AOrD.Normal; }

        public virtual AOrD HasAdvantageOnOffHandAttack(Character character) { return AOrD.Normal; }

        public virtual AOrD HasAdvantageAgainstCondition(Character character, Condition condition) { return AOrD.Normal; }

        public virtual AOrD OpponentHasAdvantageOnAttackAgainstYou(Character character) { return AOrD.Normal; }

        public virtual AOrD HasAdvantageOnInitiativeRoll(Character character) { return AOrD.Normal; }

        public virtual List<DamageType> AdditionalDamageResistance(Character character) { return new List<DamageType>(); }

        public virtual bool IgnoreAttackOfOpportunity(Character character) { return false; }
    }
}
