﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Characters;

namespace DMSTCore.Effects
{
    [Serializable]
    public class AdditionalACEffect : CombatantEffect
    {
        public AdditionalACEffect()
        {
            Name = "Additional AC";
        }

        [XmlIgnore]
        private int _ACBonus;

        [XmlElement]
        public int ACBonus 
        {
            get 
            {
                return _ACBonus;
            }
            set 
            {
                _ACBonus = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private bool _useStatBonus;

        [XmlElement]
        public bool UseStatBonus
        {
            get
            {
                return _useStatBonus;
            }
            set
            {
                _useStatBonus = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private AttributeEnum _statBonus;

        [XmlElement]
        public AttributeEnum StatBonus 
        {
            get
            {
                return _statBonus;
            }
            set
            {
                _statBonus = value;
                NotifyPropertyChanged();
            }
        }

        public override int AdditionalAC(Character character)
        {
            int ac = ACBonus;

            if (UseStatBonus)
            {
                ac += character.GetModifier(StatBonus);
            }

            return ac;
        }
    }
}
