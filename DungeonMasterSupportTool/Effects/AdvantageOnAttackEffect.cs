﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Characters;

namespace DMSTCore.Effects
{
    [Serializable]
    public class AdvantageOnAttackEffect : CombatantEffect
    {
        public override string Name
        {
            get
            {
                return "Advantage On Attack";
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private AOrD _mainHandAttack;

        [XmlElement]
        public AOrD MainHandAttack
        {
            get
            {
                return _mainHandAttack;
            }
            set
            {
                _mainHandAttack = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private AOrD _offHandAttack;

        [XmlElement]
        public AOrD OffHandAttack
        {
            get
            {
                return _offHandAttack;
            }
            set
            {
                _offHandAttack = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private AOrD _againstYou;

        [XmlElement]
        public AOrD AgainstYou
        {
            get
            {
                return _againstYou;
            }
            set
            {
                _againstYou = value;
                NotifyPropertyChanged();
            }
        }

        public override AOrD HasAdvantageOnMainHandAttack(Character character)
        {
            return MainHandAttack;
        }

        public override AOrD HasAdvantageOnOffHandAttack(Character character)
        {
            return OffHandAttack;
        }

        public override AOrD OpponentHasAdvantageOnAttackAgainstYou(Character character)
        {
            return AgainstYou;
        }
    }
}
