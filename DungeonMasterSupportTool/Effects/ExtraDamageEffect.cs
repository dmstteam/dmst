﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Characters;

namespace DMSTCore.Effects
{
    [Serializable]
    public class ExtraDamageEffect : CombatantEffect
    {
        public ExtraDamageEffect()
        {
            Name = "Extra Damage";
        }

        [XmlIgnore]
        private AttributeEnum _stat;

        [XmlElement]
        public AttributeEnum Stat
        {
            get
            {
                return _stat;
            }
            set
            {
                _stat = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private bool _extraDiceOnCritical;

        [XmlElement]
        public bool ExtraDiceOnCritical
        {
            get
            {
                return _extraDiceOnCritical;
            }
            set
            {
                _extraDiceOnCritical = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _mainHandExtraDamage;

        [XmlElement]
        public int MainHandExtraDamage
        {
            get
            {
                return _mainHandExtraDamage;
            }
            set
            {
                _mainHandExtraDamage = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _offHandExtraDamage;

        [XmlElement]
        public int OffHandExtraDamage
        {
            get
            {
                return _offHandExtraDamage;
            }
            set
            {
                _offHandExtraDamage = value;
                NotifyPropertyChanged();
            }
        }

        public override int AdditionalCriticalHitDamageRolls(Character character)
        {
            if (ExtraDiceOnCritical)
            {
                return 1;
            }

            return 0;
        }

        public override int AdditionalMainHandDamage(Character character)
        {
            return MainHandExtraDamage;
        }

        public override int AdditionalOffHandDamage(Character character)
        {
            return OffHandExtraDamage;
        }
    }
}
