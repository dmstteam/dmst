﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace DMSTCore.Maps
{
    [Serializable]
    public class Map : NameDescription
    {
        public Map() { }

        public Map(string name, string description, string parentMapKey)
        {
            Width = MaxWidth;
            Height = MaxHeight;
            Name = name;
            Description = description;
            ParentMapKey = parentMapKey;
        }

        [XmlIgnore]
        private int _width;

        [XmlElement]
        public int Width
        {
            get
            {
                if (_width > MaxWidth)
                {
                    _width = MaxWidth;
                }

                return _width;
            }
            set
            {
                _width = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _height;

        [XmlElement]
        public int Height
        {
            get
            {
                if (_height > MaxHeight)
                {
                    _height = MaxHeight;
                }

                return _height;
            }
            set
            {
                _height = value;
                NotifyPropertyChanged();
            }
        }

        public static int MaxWidth { get { return 660; } }

        public static int MaxHeight { get { return 500; } }

        [XmlIgnore]
        private ObservableCollection<Map> _subMaps = null;

        [XmlElement]
        public ObservableCollection<Map> SubMaps
        {
            get { return _subMaps; }
            set
            {
                _subMaps = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public List<MapObject> MapObjects = new List<MapObject>();

        [XmlElement]
        public string ParentMapKey;

        [XmlIgnore]
        public string StrokeFileName
        {
            get
            {
                return String.Format("{0}.ink", Key);
            }
        }

        public bool DeleteSubMap(string key)
        {
            if (SubMaps == null)
            {
                return false;
            }

            Map mapToDelete = null;

            foreach (Map map in SubMaps)
            {
                if (map.Key == key)
                {
                    mapToDelete = map;
                    break;
                }
                else
                {
                    if (map.DeleteSubMap(key))
                    {
                        return true;
                    }
                }
            }

            if (mapToDelete != null)
            {
                SubMaps.Remove(mapToDelete);
                return true;
            }
            else
            {
                return false;
            }
        }

        public void AddSubMap(string mapName, string description)
        {
            if (SubMaps == null)
            {
                SubMaps = new ObservableCollection<Map>();
            }

            SubMaps.Add(new Map(mapName, description, this.Key));
        }

        /// <summary>
        /// Recursively looks for a map within the complete submap data structure.
        /// </summary>
        /// <param name="mapName"></param>
        /// <returns></returns>
        public Map FindMapInSubMaps(string mapKey)
        {
            if (Key == mapKey)
            {
                return this;
            }

            if (SubMaps == null)
            {
                return null;
            }

            foreach (Map map in SubMaps)
            {
                if (map.Key == mapKey)
                {
                    return map;
                }
                else
                {
                    Map subMap = map.FindMapInSubMaps(mapKey);
                    if (subMap != null)
                    {
                        return subMap;
                    }
                }
            }

            return null;
        }

        public MapObject FindMapObject(string objectKey)
        {
            foreach (MapObject mapObject in MapObjects)
            {
                if (mapObject.Key == objectKey)
                {
                    return mapObject;
                }
            }

            return null;
        }
    }
}
