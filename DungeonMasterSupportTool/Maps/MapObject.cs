﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSTCore.Maps
{
    [Serializable]
    public class MapObject : NameDescription
    {
        [XmlElement]
        public MapObjectType Type { get; set; }

        [XmlElement]
        public override string IconPath
        {
            get
            {
                switch (Type)
                {
                    case MapObjectType.City:
                        return "/Dungeon Master Support Tool WPF;component/Images/cityIcon.png";
                    case MapObjectType.Dungeon:
                        return "/Dungeon Master Support Tool WPF;component/Images/dungeonDoorIcon.png";
                    case MapObjectType.Church:
                        return "/Dungeon Master Support Tool WPF;component/Images/churchIcon.png";
                    case MapObjectType.Alchemist:
                        return "/Dungeon Master Support Tool WPF;component/Images/potionIcon.png";
                    case MapObjectType.Castle:
                        return "/Dungeon Master Support Tool WPF;component/Images/castleIcon.png";
                    case MapObjectType.Library:
                        return "/Dungeon Master Support Tool WPF;component/Images/bookIcon.png";
                    case MapObjectType.Tavern:
                        return "/Dungeon Master Support Tool WPF;component/Images/beerStein.png";
                    case MapObjectType.Theater:
                        return "/Dungeon Master Support Tool WPF;component/Images/theaterIcon.png";
                    default:
                        return "/Dungeon Master Support Tool WPF;component/Images/castleIcon.png";
                }
            }
        }

        [XmlIgnore]
        public override string Name
        {
            get
            {
                if (MapKey != null)
                {
                    return ApplicationData.CurrentCampaign.WorldMap.FindMapInSubMaps(MapKey).Name;
                }
                else
                {
                    return base.Name;
                }
            }
            set
            {
                if (MapKey != null)
                {
                    ApplicationData.CurrentCampaign.WorldMap.FindMapInSubMaps(MapKey).Name = value;
                }
                else
                {
                    base.Name = value;
                }
                
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public string MapKey;

        [XmlIgnore]
        private double _x;

        [XmlElement]
        public double X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private double _y;

        [XmlElement]
        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
                NotifyPropertyChanged();
            }
        }
    }
}
