﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Effects;

namespace DMSTCore.Kits
{
    [Serializable]
    public class KeyLevelPair : PropertyChangedNotifier
    {
        [XmlIgnore]
        private int _gainedAtlevel;

        [XmlElement]
        public int GainedAtLevel
        {
            get
            {
                return _gainedAtlevel;
            }
            set
            {
                _gainedAtlevel = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public string Key;

        [XmlIgnore]
        private KitFeature _kitFeature;

        [XmlIgnore]
        public KitFeature KitFeature
        {
            get
            {
                if (_kitFeature == null)
                {
                    _kitFeature = ApplicationData.Database.GetKitFeature(Key);
                }

                return _kitFeature;
            }
        }
    }
}
