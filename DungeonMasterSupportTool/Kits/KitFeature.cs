﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Actions;
using DMSTCore.Characters;
using DMSTCore.Effects;
using DMSTCore.GameEngine;
using DMSTCore.Items;

namespace DMSTCore.Kits
{
    [Serializable]
    public class KitFeature : CombatantEffect
    {
        public KitFeature()
        {
            Name = "None";
            IconPath = @"..\Images\KitFeatureIcon.png";
        }

        [XmlIgnore]
        public bool HasAction
        {
            get
            {
                return Action != null;
            }
        }

        [XmlIgnore]
        private MyAction _action;

        [XmlElement]
        public MyAction Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _gainedAtLevel;

        [XmlElement]
        public int GainedAtLevel
        {
            get
            {
                return _gainedAtLevel;
            }
            set
            {
                _gainedAtLevel = value;
                NotifyPropertyChanged();
            }
        }
    }
}
