﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Effects;
using DMSTCore.Characters;

namespace DMSTCore.Kits
{
    [Serializable]
    public class KitTemplate : NameDescription
    {
        public KitTemplate()
        {
            Name = "New Kit Template";
        }

        [XmlIgnore]
        private int _hPDie;

        [XmlElement]
        public int HPDie
        {
            get
            {
                return _hPDie;
            }
            set
            {
                _hPDie = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private List<int> _chargesPerLevel;

        [XmlElement]
        public List<int> ChargesPerLevel
        {
            get
            {

                if (_chargesPerLevel == null)
                {
                    _chargesPerLevel = new List<int>();
                    for (int i = 0; i < 20; i++)
                    {
                        _chargesPerLevel.Add(0);
                    }
                }
                else if (_chargesPerLevel.Count == 40)
                {
                    List<int> newList = new List<int>();
                    for (int i = 0; i < 20; i++)
                    {
                        newList.Add(_chargesPerLevel[i + 20]);
                    }
                    _chargesPerLevel = newList;
                }

                return _chargesPerLevel;
            }
            set
            {
                _chargesPerLevel = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public AttributeEnum MajorStat1
        {
            get
            {
                return (AttributeEnum)MajorStat1Index;
            }
        }

        [XmlIgnore]
        private int _majorStat1Index;

        [XmlElement]
        public int MajorStat1Index
        {
            get
            {
                return _majorStat1Index;
            }
            set
            {
                _majorStat1Index = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public AttributeEnum MajorStat2
        {
            get
            {
                return (AttributeEnum)MajorStat2Index;
            }
        }

        [XmlIgnore]
        private int _majorStat2Index;

        [XmlElement]
        public int MajorStat2Index
        {
            get
            {
                return _majorStat2Index;
            }
            set
            {
                _majorStat2Index = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<KeyLevelPair> _keyLevelPairs = new ObservableCollection<KeyLevelPair>();

        [XmlElement]
        public ObservableCollection<KeyLevelPair> KeyLevelPairs
        {
            get
            {
                return _keyLevelPairs;
            }
            set
            {
                _keyLevelPairs = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<Proficiency> _proficiencies = new ObservableCollection<Proficiency>();

        [XmlElement]
        public ObservableCollection<Proficiency> Proficiencies
        {
            get
            {
                return _proficiencies;
            }
            set
            {
                _proficiencies = value;
                NotifyPropertyChanged();
            }
        }

        public void AddKeyValuePair(KeyLevelPair pair)
        {
            KeyLevelPairs.Add(pair);
            KeyLevelPairs = new ObservableCollection<KeyLevelPair>(KeyLevelPairs.OrderBy(x => x.GainedAtLevel));
        }

        public void RemoveKeyValuePair(string key)
        {
            for(int i=KeyLevelPairs.Count-1;i>=0;i--)
            {
                if (KeyLevelPairs[i].Key == key)
                {
                    KeyLevelPairs.RemoveAt(i);
                }
            }
        }

        public List<KitFeature> GetFeaturesAtLevel(int level)
        {
            List<KitFeature> kitFeatures = new List<KitFeature>();
            foreach (KeyLevelPair keyLevelPair in KeyLevelPairs)
            {
                if (keyLevelPair.GainedAtLevel == level)
                {
                    kitFeatures.Add(keyLevelPair.KitFeature);
                }
            }

            return kitFeatures;
        }
    }
}
