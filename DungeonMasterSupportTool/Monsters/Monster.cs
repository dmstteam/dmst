﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using DMSTCore.Actions;
using DMSTCore.Encounters;
using DMSTCore.Characters;
using System.Xml.Serialization;

namespace DMSTCore.Monsters
{
    [Serializable]
    public class Monster : Combatant, ICloneable
    {
        public Monster()
        {
            PlayerName = "DM";
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string IconPath
        {
            get
            {
                return @"../Images/MonsterIcon.png";
            }
        }

        [XmlIgnore]
        private int _hpDie;

        [XmlElement]
        public int HpDie
        {
            get
            {
                return _hpDie;
            }
            set
            {
                _hpDie = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("HPRollDisplay");
            }
        }

        [XmlIgnore]
        private int _hpDice;

        [XmlElement]
        public int HpDice
        {
            get
            {
                return _hpDice;
            }
            set
            {
                _hpDice = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("HPRollDisplay");
            }
        }

        [XmlIgnore]
        private int _hpModifier;

        [XmlElement]
        public int HpModifier
        {
            get
            {
                return _hpModifier;
            }
            set
            {
                _hpModifier = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("HPRollDisplay");
            }
        }

        [XmlIgnore]
        public bool IsLegendaryMonster
        {
            get
            {
                return LegendaryActions.Count > 0;
            }
        }

        [XmlIgnore]
        private MyAction _currentAction;

        [XmlIgnore]
        public MyAction CurrentAction
        {
            get
            {
                return _currentAction;
            }
            set
            {
                _currentAction = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private MyAction _currentLegendaryAction;

        [XmlIgnore]
        public MyAction CurrentLegendaryAction
        {
            get
            {
                return _currentLegendaryAction;
            }
            set
            {
                _currentLegendaryAction = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private NameDescription _currentPassiveAbility;

        [XmlIgnore]
        public NameDescription CurrentPassiveAbility
        {
            get
            {
                return _currentPassiveAbility;
            }
            set
            {
                _currentPassiveAbility = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public List<AttributeEnum> SavingThrowsThatReceiveBonus = new List<AttributeEnum>();

        [XmlElement]
        public int SavingThrowBonus;

        public override int GetSave(AttributeEnum attribute)
        {
            int save = base.GetSave(attribute);

            foreach (AttributeEnum stat in SavingThrowsThatReceiveBonus)
            {
                if (stat == attribute)
                {
                    save += SavingThrowBonus;
                    break;
                }
            }

            return save;
        }

        [XmlIgnore]
        public string HPRollDisplay
        {
            get
            {
                return Dice.FormatDiceString(HpDice, HpDie, HpModifier);
            }
        }

        [XmlElement]
        public MonsterType Type;

        [XmlIgnore]
        private float _challengeRating;

        [XmlElement]
        public float ChallengeRating
        {
            get
            {
                return _challengeRating;
            }
            set
            {
                _challengeRating = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<MyAction> _additionalActions = new ObservableCollection<MyAction>();

        [XmlElement]
        public ObservableCollection<MyAction> AdditionalActions
        {
            get
            {
                return _additionalActions;
            }
            set
            {
                _additionalActions = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<MyAction> _legendaryActions = new ObservableCollection<MyAction>();

        [XmlElement]
        public ObservableCollection<MyAction> LegendaryActions
        {
            get
            {
                return _legendaryActions;
            }
            set
            {
                _legendaryActions = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public override ObservableCollection<MyAction> Actions
        {
            get
            {
                ObservableCollection<MyAction> actions = base.Actions;

                foreach (MyAction action in AdditionalActions)
                {
                    actions.Add(action);
                }

                return actions;
            }
        }
    }
}
