﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Core;
using DMSTCore.Characters;

namespace DMSTCore.Campaigns
{
    [Serializable]
    public class Place : CombatantGroup
    {
        public Place()
        {
            Name = "New Place";
        }

        public override string IconPath
        {
            get
            {
                return @"../Images/PlaceIcon.png";
            }
            set
            {
                base.IconPath = value;
            }
        }
    }
}
