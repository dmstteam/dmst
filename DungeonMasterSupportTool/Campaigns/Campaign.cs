﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Characters;
using DMSTCore.Maps;

namespace DMSTCore.Campaigns
{
    [Serializable]
    public class Campaign : NameDescription
    {
        public Campaign()
        {
            Name = "New Campaign";
            Description = "This is the story so far";
            CurrentQuest = null;
        }

        [XmlElement]
        public string Filename
        {
            get
            {
                return String.Format("{0}.xml", Key);
            }
        }

        [XmlIgnore]
        private ObservableCollection<Character> _PCs = new ObservableCollection<Character>();

        [XmlElement]
        public ObservableCollection<Character> PCs
        {
            get
            {
                return _PCs;
            }
            set
            {
                _PCs = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<Character> _NPCs = new ObservableCollection<Character>();

        [XmlElement]
        public ObservableCollection<Character> NPCs
        {
            get
            {
                return _NPCs;
            }
            set
            {
                _NPCs = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Organizations");
            }
        }

        public Character GetNPC(string key)
        {
            foreach (Character character in NPCs)
            {
                if (character.Key == key)
                {
                    return character;
                }
            }

            return null;
        }

        [XmlIgnore]
        private ClockCalender _currentTime = new ClockCalender();

        [XmlElement]
        public ClockCalender CurrentTime
        {
            get
            {
                return _currentTime;
            }
            set
            {
                _currentTime = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<Quest> _quests = new ObservableCollection<Quest>();

        [XmlIgnore]
        public int NumberOfQuests
        {
            get
            {
                return _quests.Count;
            }
        }

        [XmlElement]
        public ObservableCollection<Quest> Quests
        {
            get
            {
                return _quests;
            }
            set
            {
                _quests = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("NumberOfQuests");
                NotifyPropertyChanged("NumberOfEncounters");
            }
        }

        [XmlIgnore]
        public int NumberOfEncounters
        {
            get
            {
                int encounters = 0;
                foreach (Quest quest in _quests)
                {
                    encounters += quest.NumberOfEncounters;
                }
                return encounters;
            }
        }

        [XmlIgnore]
        private Map _worldMap;

        [XmlElement]
        public Map WorldMap
        {
            get
            {
                if (_worldMap == null)
                {
                    _worldMap = new Map("World Map", "The Highest Level Map of the World", String.Empty);
                }
                return _worldMap;
            }
            set
            {
                _worldMap = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Map _currentMap;

        [XmlIgnore]
        public Map CurrentMap 
        {
            get
            {
                if (_currentMap == null)
                {
                    _currentMap = WorldMap;
                }

                return _currentMap;
            }
            set
            {
                _currentMap = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Place _currentPlace;

        [XmlIgnore]
        public Place CurrentPlace
        {
            get
            {
                return _currentPlace;
            }
            set
            {
                _currentPlace = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Quest _currentQuest;

        [XmlIgnore]
        public Quest CurrentQuest
        {
            get
            {
                return _currentQuest;
            }
            set
            {
                _currentQuest = value;
                NotifyPropertyChanged();
            }
        }

        private Organization _currentOrganization;

        [XmlIgnore]
        public Organization CurrentOrganization
        {
            get
            {
                return _currentOrganization;
            }
            set
            {
                _currentOrganization = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<Organization> _organizations = new ObservableCollection<Organization>();

        [XmlElement]
        public ObservableCollection<Organization> Organizations
        {
            get
            {
                return _organizations;
            }
            set
            {
                _organizations = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ObservableCollection<Place> _places = new ObservableCollection<Place>();

        [XmlElement]
        public ObservableCollection<Place> Places
        {
            get
            {
                return _places;
            }
            set
            {
                _places = value;
                NotifyPropertyChanged();
            }
        }

        public Place GetPlace(string key)
        {
            foreach (Place place in Places)
            {
                if (place.Key == key)
                {
                    return place;
                }
            }

            return null;
        }
    }
}
