﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Maps;
using DMSTCore.Characters;
using DMSTCore.Monsters;
using DMSTCore.Campaigns;

namespace DMSTCore.Campaigns
{
    public class Quest : NameDescription
    {
        public Quest()
        {
            Name = "New Quest";
            IconPath = @"..\Images\QuestIcon.png";
            Dungeons = new List<Dungeon>();
            Encounters = new ObservableCollection<Encounter>();
            CurrentEncounter = null;

            Scenes = new ObservableCollection<Scene>();
        }

        [XmlIgnore]
        public string ShortDescription
        {
            get
            {
                return "A short description";
            }
        }

        [XmlIgnore]
        private ObservableCollection<Scene> _scenes;

        [XmlElement]
        public ObservableCollection<Scene> Scenes
        {
            get
            {
                return _scenes;
            }
            set
            {
                _scenes = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Scene _currentScene;

        [XmlIgnore]
        public Scene CurrentScene
        {
            get
            {
                return _currentScene;
            }
            set
            {
                _currentScene = value;
                NotifyPropertyChanged();
            }
        }

        public Encounter GetEncounter(string key)
        {
            foreach (Encounter encounter in Encounters)
            {
                if (encounter.Key == key)
                {
                    return encounter;
                }
            }

            return null;
        }

        [XmlIgnore]
        public List<Character> ActivePCs
        {
            get
            {
                List<Character> activePCs = new List<Character>();
                foreach (Character pc in ApplicationData.CurrentCampaign.PCs)
                {
                    if (pc.Active)
                    {
                        activePCs.Add(pc);
                    }
                }

                return activePCs;
            }
        }

        [XmlElement]
        public List<Dungeon> Dungeons;

        [XmlIgnore]
        public int NumberOfEncounters
        {
            get
            {
                return _encounters.Count;
            }
        }

        [XmlIgnore]
        private ObservableCollection<Encounter> _encounters = new ObservableCollection<Encounter>();

        [XmlElement]
        public ObservableCollection<Encounter> Encounters
        {
            get
            {
                return _encounters;
            }
            set
            {
                _encounters = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("NumberOfEncounters");
            }
        }

        [XmlIgnore]
        public Encounter CurrentEncounter;
    }
}
