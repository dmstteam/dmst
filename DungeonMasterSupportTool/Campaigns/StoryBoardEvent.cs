﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace DMSTCore.Campaigns
{
    [Serializable]
    public class StoryBoardEvent : NameDescription
    {
        public StoryBoardEvent(string previousStoryBoardEventkKey)
        {
            Name = "Event";
            PreviousStoryBoardEventKey = previousStoryBoardEventkKey;
        }

        public StoryBoardEvent()
        {
            Name = "Event";
        }

        public void AddStoryBoardEvent()
        {
            StoryBoardEvent story = new StoryBoardEvent();
            story.PreviousStoryBoardEventKey = Key;
            NextStoryBoardEvents.Add(story);
        }

        [XmlIgnore]
        private ObservableCollection<StoryBoardEvent> _nextStoryBoardEvents = new ObservableCollection<StoryBoardEvent>();

        [XmlElement]
        public ObservableCollection<StoryBoardEvent> NextStoryBoardEvents
        {
            get
            {
                return _nextStoryBoardEvents;
            }
            set
            {
                _nextStoryBoardEvents = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public string PreviousStoryBoardEventKey;

        public StoryBoardEvent GetStoryBoardEvent(string key)
        {
            if (Key == key)
            {
                return this;
            }

            foreach (StoryBoardEvent storyBoardEvent in NextStoryBoardEvents)
            {
                if (storyBoardEvent.Key == key)
                {
                    return storyBoardEvent;
                }

                StoryBoardEvent subStory = storyBoardEvent.GetStoryBoardEvent(key);
                if (subStory != null)
                {
                    return subStory;
                }
            }

            return null;
        }

        public float RadialArc { get; set; }

        [XmlIgnore]
        private string _encounterKey;

        [XmlElement]
        public string EncounterKey
        {
            get
            {
                return _encounterKey;
            }
            set
            {
                _encounterKey = value;
            }
        }

        [XmlIgnore]
        public Encounter Encounter
        {
            get
            {
                return ApplicationData.CurrentQuest.GetEncounter(EncounterKey);
            }
            set
            {
                EncounterKey = value.Key;
                NotifyPropertyChanged();
                NotifyPropertyChanged("EncounterButtonText");
            }
        }

        public string EncounterButtonText
        {
            get
            {
                if (Encounter != null)
                {
                    return String.Format("Run '{0}'", Encounter.Name);
                }
                else
                {
                    return "Add Encounter";
                }
            }
        }
    }
}
