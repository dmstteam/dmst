﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Campaigns
{
    [Serializable]
    public class ClockCalender : PropertyChangedNotifier
    {
        Timer _timeTick;

        public ClockCalender()
        {
            _timeTick = new Timer(1000);
            _timeTick.Enabled = false;
            _timeTick.Elapsed += _timeTick_Elapsed;
            DateTime = new DateTime(1, 1, 1, 12, 0, 0);
        }

        void _timeTick_Elapsed(object sender, ElapsedEventArgs e)
        {
            DateTime = DateTime.AddSeconds(1.0);
            NotifyPropertyChanged("Year");
            NotifyPropertyChanged("Month");
            NotifyPropertyChanged("Day");
            NotifyPropertyChanged("Hour");
            NotifyPropertyChanged("Minute");
            NotifyPropertyChanged("Second");
        }

        public void Pause()
        {
            _timeTick.Enabled = false;
        }

        public void Start()
        {
            _timeTick.Enabled = true;
        }

        [XmlIgnore]
        public DateTime _dateTime;

        [XmlElement]
        public DateTime DateTime
        {
            get
            {
                return _dateTime;
            }
            set
            {
                _dateTime = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public int Hour
        {
            get
            {
                return DateTime.Hour;
            }
        }

        [XmlIgnore]
        public int Minute
        {
            get
            {
                return DateTime.Minute;
            }
        }

        [XmlIgnore]
        public int Second
        {
            get
            {
                return DateTime.Second;
            }
        }

        [XmlIgnore]
        public int Day
        {
            get
            {
                return DateTime.Day;
            }
        }

        [XmlIgnore]
        public string Month
        {
            get
            {
                return DateTime.ToString("MMM");
            }
        }

        [XmlIgnore]
        public int Year
        {
            get
            {
                return DateTime.Year;
            }
        }
    }
}
