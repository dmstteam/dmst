﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using System.Xml.Serialization;
using DMSTCore.Characters;
using DMSTCore.Encounters;
using DMSTCore.Core; 

namespace DMSTCore.Campaigns
{
    [Serializable]
    public class Encounter : NameDescription
    {
        public Encounter()
        {
            Name = "New Encounter";
            Description = "This is a cool encounter.";
            IconPath = @"../Images/EncounterIcon.jpg";
        }

        [XmlIgnore]
        public List<Character> ActivePCs 
        {
            get 
            {
                return ApplicationData.CurrentQuest.ActivePCs;
            }
        }

        [XmlIgnore]
        private ObservableCollection<Team> _teams = new ObservableCollection<Team>();

        [XmlElement]
        public ObservableCollection<Team> Teams
        {
            get
            {
                return _teams;
            }
            set
            {
                _teams = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Team _currentTeam;

        [XmlIgnore]
        public Team CurrentTeam
        {
            get
            {
                return _currentTeam;
            }
            set
            {
                _currentTeam = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Round _currentRound;

        [XmlIgnore]
        public Round CurrentRound
        {
            get
            {
                return _currentRound;
            }
            set
            {
                _currentRound = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Turn _currentTurn;

        [XmlIgnore]
        public Turn CurrentTurn 
        {
            get
            {
                return _currentTurn;
            }
            set
            {
                _currentTurn = value;
                NotifyPropertyChanged();
            }
        }

        public ObservableCollection<Combatant> TargetsInRange(Combatant referenceCombantant, FloatDistance distance)
        {
            ObservableCollection<Combatant> targetsInRange = new ObservableCollection<Combatant>();

            foreach (Combatant pc in ActivePCs)
            {
                targetsInRange.Add(pc);
            }

            foreach (Team team in Teams)
            {
                foreach (Combatant combatant in team.Members)
                {
                    targetsInRange.Add(combatant);
                }
            }

            targetsInRange.Remove(referenceCombantant);

            return targetsInRange;
        }
    }
}
