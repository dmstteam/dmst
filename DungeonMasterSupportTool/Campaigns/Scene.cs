﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Campaigns
{
    [Serializable]
    public class Scene : NameDescription
    {
        public Scene()
        {
            Name = "New Scene";
            StoryBoard = new StoryBoardEvent();
        }

        [XmlIgnore]
        private StoryBoardEvent _storyBoard;

        [XmlElement]
        public StoryBoardEvent StoryBoard
        {
            get
            {
                return _storyBoard;
            }
            set
            {
                _storyBoard = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StoryBoardEvent _currentStoryBoardEvent;

        [XmlIgnore]
        public StoryBoardEvent CurrentStoryBoardEvent
        {
            get
            {
                return _currentStoryBoardEvent;
            }
            set
            {
                _currentStoryBoardEvent = value;
                NotifyPropertyChanged();
            }
        }
    }
}
