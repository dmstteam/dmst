﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Characters;
using DMSTCore.Core;

namespace DMSTCore.Campaigns
{
    [Serializable]
    public class Organization : CombatantGroup
    {
        public Organization()
        {
            Name = "New Organization";
            IconPath = @"..\Images\OrganizationIcon.png";
        }

        [XmlElement]
        public string HeadQuartersKey;

        [XmlIgnore]
        public Place HeadQuarters
        {
            get
            {
                return ApplicationData.CurrentCampaign.GetPlace(HeadQuartersKey);
            }
            set
            {
                HeadQuartersKey = value.Key;
                NotifyPropertyChanged();
            }
        }
    }
}
