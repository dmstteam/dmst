﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DMSTCore.Characters;

namespace DMSTCore.Encounters
{
    public class Turn : PropertyChangedNotifier
    {
        public Turn(Combatant combatant)
        {
            Combatant = combatant;
            NumberOfActions = 1;
            NumberOfMovements = (int)combatant.GroundSpeed.Value;
        }

        private int _numberOfActions;

        public int NumberOfActions
        {
            get
            {
                return _numberOfActions;
            }
            set
            {
                _numberOfActions = value;
                NotifyPropertyChanged();
            }
        }

        private int _numberOfMovements;

        public int NumberOfMovements
        {
            get
            {
                return _numberOfMovements;
            }
            set
            {
                _numberOfMovements = value;
                NotifyPropertyChanged();
            }
        }

        private Combatant _combatant;

        public Combatant Combatant
        {
            get
            {
                return _combatant;
            }
            set
            {
                _combatant = value;
                NotifyPropertyChanged();
            }
        }

        public bool AreActionsRemaining
        {
            get
            {
                return NumberOfActions != 0;
            }
        }
    }
}
