﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using DMSTCore.Campaigns;
using DMSTCore.Characters;

namespace DMSTCore.Encounters
{
    public class Round : PropertyChangedNotifier
    {
        private ObservableCollection<Turn> _turns = new ObservableCollection<Turn>();

        public ObservableCollection<Turn> Turns
        {
            get
            {
                return _turns;
            }
            set
            {
                _turns = value;
                NotifyPropertyChanged();
            }
        }

        public List<Turn> HeldTurns = new List<Turn>();

        public void StartNewRound()
        {
            Turns.Clear();   
            RollForInitiative();
            NextTurn();
        }

        private void RollForInitiative()
        {
            Encounter currentEncounter = ApplicationData.CurrentEncounter;
            List<Combatant> combatants = new List<Combatant>();

            combatants.AddRange(currentEncounter.ActivePCs);
            foreach (Team team in currentEncounter.Teams)
            {
                combatants.AddRange(team.Members);
            }

            foreach (Combatant combatant in combatants)
            {
                combatant.CurrentInitiativeRoll = Dice.RollD20() + combatant.InitiativeModifier;
            }

            BuildTurnQueue(combatants);
        }

        public bool NextTurn()
        {
            if (ApplicationData.CurrentEncounter.CurrentTurn != null && ApplicationData.CurrentEncounter.CurrentTurn.AreActionsRemaining)
            {
                ApplicationData.Communication.YesNoRequest(
                    String.Format("{0} Actions remaining.  Do you want to hold initiative?", ApplicationData.CurrentEncounter.CurrentTurn.NumberOfActions), "Hold Initiative");
            }

            if (Turns.Count > 0)
            {
                ApplicationData.CurrentEncounter.CurrentTurn = Turns[0];
                Turns.RemoveAt(0);
            }
            else
            {
                ApplicationData.CurrentEncounter.CurrentTurn = null;
            }

            return true;
        }

        private void BuildTurnQueue(List<Combatant> combatants) 
        {
            Turns = new ObservableCollection<Turn>();
            List<bool> queued = new List<bool>();

            for (int i = 0; i < combatants.Count;i++ )
            {
                queued.Add(false);
            }

            int queueindex = 0;
            while (queueindex != -1)
            {
                queueindex = -1;
                int max = -100;

                for (int i = 0; i < combatants.Count; i++)
                {
                    if (queued[i]) { continue; }

                    if (combatants[i].CurrentInitiativeRoll > max)
                    {
                        max = combatants[i].CurrentInitiativeRoll;
                        queueindex = i;
                    }
                }

                if (queueindex > -1)
                {
                    Turn turn = new Turn(combatants[queueindex]);
                    Turns.Add(turn);
                    queued[queueindex] = true;
                }
            }
        }
    }
}
