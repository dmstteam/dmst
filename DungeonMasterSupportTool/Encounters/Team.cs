﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Core;
using DMSTCore.Monsters;

namespace DMSTCore.Encounters
{
    [Serializable]
    public class Team : CombatantGroup
    {
        public Team()
        {
            Name = "New Team";
            IconPath = @"../Images/TeamIcon.png";
        }

        public override ObservableCollection<Combatant> Members
        {
            get
            {
                ObservableCollection<Combatant> members = base.Members;

                foreach (Monster monster in Monsters)
                {
                    members.Add(monster);
                }

                return members;
            }
        }

        [XmlIgnore]
        private ObservableCollection<Monster> _monsters = new ObservableCollection<Monster>();

        [XmlElement]
        public ObservableCollection<Monster> Monsters
        {
            get
            {
                return _monsters;
            }
            set
            {
                _monsters = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("NumberOfMembers");
            }
        }

        public void AddMonster(Monster monster)
        {
            Monsters.Add(monster);

            NotifyPropertyChanged("Members");
            NotifyPropertyChanged("NumberOfMembers");
        }
    }
}
