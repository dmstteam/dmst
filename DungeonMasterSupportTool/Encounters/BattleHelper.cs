﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DMSTCore.Actions;
using DMSTCore.Spells;

namespace DMSTCore.Encounters
{
    public static class BattleHelper
    {
        # region Public - Other Functions

        public static void DamageCombatant(Combatant combatant, int damage)
        {
            if (damage <= 0) { return; }

            combatant.Damage(damage);
        }

        public static void HealCombatant(Combatant combatant, int heal)
        {
            if (heal <= 0) { return; }

            combatant.Heal(heal);
        }

        # endregion

        # region Private Functions

        private static bool IsInRange()
        {
            return true;
        }

        private static bool IsDefenderDead()
        {
            return false;
        }

        # endregion
    }
}
