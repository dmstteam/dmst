﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ComponentModel;

namespace DMSTCore.Characters
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Attributes : PropertyChangedNotifier
    {
        public Attributes() 
        {
            SetAll(10);
        }

        public Attributes(int str, int dex, int con, int inte, int wis, int cha)
        {
            Str = str;
            Dex = dex;
            Con = con;
            Int = inte;
            Wis = wis;
            Cha = cha;
        }

        public Attributes(int startValue)
        {
            SetAll(startValue);
        }

        public void SetAll(int startValue) 
        {
            Str = startValue;
            Dex = startValue;
            Con = startValue;
            Int = startValue;
            Wis = startValue;
            Cha = startValue;
        }

        public void SetAll(Attributes stats)
        {
            Str = stats.Str;
            Dex = stats.Dex;
            Con = stats.Con;
            Int = stats.Int;
            Wis = stats.Wis;
            Cha = stats.Cha;
        }

        public void AddPoints(Attributes a2)
        {
            Str = Str + a2.Str;
            Dex = Dex + a2.Dex;
            Con = Con + a2.Con;
            Int = Int + a2.Int;
            Wis = Wis + a2.Wis;
            Cha = Cha + a2.Cha;
        }

        public void SubtractPoints(Attributes a2)
        {
            Str = Str - a2.Str;
            Dex = Dex - a2.Dex;
            Con = Con - a2.Con;
            Int = Int - a2.Int;
            Wis = Wis - a2.Wis;
            Cha = Cha - a2.Cha;
        }

        public int GetModifier(AttributeEnum attribute)
        {
            switch (attribute)
            {
                case AttributeEnum.Str :
                    return StrModifier;
                case AttributeEnum.Dex :
                    return DexModifier;
                case AttributeEnum.Con :
                    return ConModifier;
                case AttributeEnum.Int :
                    return IntModifier;
                case AttributeEnum.Wis :
                    return WisModifier;
                case AttributeEnum.Cha :
                    return ChaModifier;
            }

            return 0;
        }

        public int GetAttribute(AttributeEnum attribute)
        {
            switch (attribute)
            {
                case AttributeEnum.Str:
                    return Str;
                case AttributeEnum.Dex:
                    return Dex;
                case AttributeEnum.Con:
                    return Con;
                case AttributeEnum.Int:
                    return Int;
                case AttributeEnum.Wis:
                    return Wis;
                case AttributeEnum.Cha:
                    return Cha;
            }

            return 0;
        }

        public void OrderByPriority(List<AttributeEnum> Priorities)
        {
            List<AttributeEnum> largest = new List<AttributeEnum>();

            List<bool> skip = new List<bool>();
            for (int i = 0; i < 6; i++)
            {
                skip.Add(false);
            }

            for (int j = 0; j < Priorities.Count; j++)
            {
                AttributeEnum stat = AttributeEnum.Str;
                int largestValue = 0;
                for (int i = 0; i < 6; i++)
                {
                    if (skip[i]) { continue; }

                    int value = GetAttribute((AttributeEnum)i);
                    if (value > largestValue)
                    {
                        stat = (AttributeEnum)i;
                        largestValue = value;
                    }
                }

                largest.Add(stat);
                skip[(int)stat] = true;
            }

            for (int i = 0; i < Priorities.Count; i++)
            {
                int low = GetAttribute(Priorities[i]);
                int high = GetAttribute(largest[i]);

                SetAttribute(largest[i], low);
                SetAttribute(Priorities[i], high);
            }
        }

        public void SetAttribute(AttributeEnum attribute, int value)
        {
            switch (attribute)
            {
                case AttributeEnum.Str:
                    Str = value;
                    break;
                case AttributeEnum.Dex:
                    Dex = value;
                    break;
                case AttributeEnum.Con:
                    Con = value;
                    break;
                case AttributeEnum.Int:
                    Int = value;
                    break;
                case AttributeEnum.Wis:
                    Wis = value;
                    break;
                case AttributeEnum.Cha:
                    Cha = value;
                    break;
            }
        }

        [XmlIgnore]
        private int _str;

        [XmlElement]
        public int Str 
        {
            get { return _str; }
            set 
            {
                _str = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _dex;

        [XmlElement]
        public int Dex
        {
            get { return _dex; }
            set
            {
                _dex = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _con;

        [XmlElement]
        public int Con
        {
            get { return _con; }
            set
            {
                _con = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _int;

        [XmlElement]
        public int Int
        {
            get { return _int; }
            set
            {
                _int = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _wis;

        [XmlElement]
        public int Wis
        {
            get { return _wis; }
            set
            {
                _wis = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _cha;

        [XmlElement]
        public int Cha
        {
            get { return _cha; }
            set
            {
                _cha = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public int StrModifier
        {
            get
            {
                return GetModifier(Str);
            }
        }

        [XmlIgnore]
        public int DexModifier
        {
            get
            {
                return GetModifier(Dex);
            }
        }

        [XmlIgnore]
        public int ConModifier
        {
            get
            {
                return GetModifier(Con);
            }
        }

        [XmlIgnore]
        public int IntModifier
        {
            get
            {
                return GetModifier(Int);
            }
        }

        [XmlIgnore]
        public int WisModifier
        {
            get
            {
                return GetModifier(Wis);
            }
        }

        [XmlIgnore]
        public int ChaModifier
        {
            get
            {
                return GetModifier(Cha);
            }
        }

        private int GetModifier(int stat)
        {
            int mod = (int)Math.Floor((double)(stat - 10) / 2.0);
            return mod;
        }
    }
}
