﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Characters
{
    [Serializable]
    public class HpRoll : PropertyChangedNotifier
    {
        public HpRoll() { }

        public HpRoll(int roll, int level)
        {
            Value = roll;
            GainedAtlevel = level;
        }

        [XmlIgnore]
        public bool PropertyChangedSet = false;

        [XmlIgnore]
        public bool IsActive;

        [XmlIgnore]
        private int _value;

        [XmlElement]
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _gainedAtLevel;

        [XmlElement]
        public int GainedAtlevel
        {
            get
            {
                return _gainedAtLevel;
            }
            set
            {
                _gainedAtLevel = value;
                NotifyPropertyChanged();
            }
        }
    }
}
