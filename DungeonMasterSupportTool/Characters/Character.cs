﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore;
using DMSTCore.Actions;
using DMSTCore.Core;
using DMSTCore.Encounters;
using DMSTCore.Kits;
using DMSTCore.Items;
using DMSTCore.Spells;
using DMSTCore.Effects;

namespace DMSTCore.Characters
{
    [Serializable]
    public class Character : Combatant
    {
        public Character()
            : base()
        {
            Name = "Character Name";
            PlayerName = "Player Name";
            Religion = "Religion";
            Active = true;
            Race = ApplicationData.GetRaceFromName("Human");
        }

        public Character(string name)
        {
            Name = name;
            PlayerName = "Player Name";
            Active = true;
            Race = ApplicationData.GetRaceFromName("Human");
        }

        [XmlElement]
        public string RaceKey;

        [XmlIgnore]
        public Race Race
        {
            get
            {
                return ApplicationData.GetRaceFromKey(RaceKey);
            }
            set
            {
                RaceKey = value.Key;
                NotifyPropertyChanged();
                UpdateAllCalculatedFields();
            }
        }

        [XmlElement]
        public string KitTemplateKey;

        [XmlIgnore]
        public KitTemplate KitTemplate
        {
            get
            {
                KitTemplate template = ApplicationData.GetKitTemplateFromKey(KitTemplateKey);
                return template;
            }
            set
            {
                KitTemplateKey = value.Key;

                int level = Level;
                ChangeLevel(0);
                ChangeLevel(level);

                NotifyPropertyChanged();
                NotifyPropertyChanged("KitFeatures");
                NotifyPropertyChanged("Proficiencies");
                UpdateAllCalculatedFields();
            }
        }

        [XmlIgnore]
        public override ObservableCollection<Proficiency> Proficiencies
        {
            get
            {
                ObservableCollection<Proficiency> profs = new ObservableCollection<Proficiency>();

                var basep = base.Proficiencies;
                foreach (Proficiency p in basep)
                {
                    profs.Add(p);
                }

                if (KitTemplate == null)
                {
                    return profs;
                }

                foreach (Proficiency prof in KitTemplate.Proficiencies)
                {
                    profs.Add(prof);
                }

                return profs;
            }
        }

        [XmlIgnore]
        private ObservableCollection<Feat> _feats = new ObservableCollection<Feat>();

        [XmlElement]
        public ObservableCollection<Feat> Feats
        {
            get
            {
                return _feats;
            }
            set
            {
                _feats = value;
                NotifyPropertyChanged();
            }
        }

        public override void SetStats(List<int> values)
        {
            base.SetStats(values);

            List<AttributeEnum> priorities = new List<AttributeEnum>();
            priorities.Add(KitTemplate.MajorStat1);
            priorities.Add(KitTemplate.MajorStat2);
            BaseStats.OrderByPriority(priorities);
        }

        [XmlIgnore]
        public override Attributes Stats
        {
            get
            {
                Attributes stats = new Attributes();
                stats.SetAll(base.BaseStats);
                stats.AddPoints(Race.AdditionalStats);
                return stats;
            }
        }

        [XmlIgnore]
        private ObservableCollection<KeyLevelPair> _kitFeatureKeyLevelPairs = new ObservableCollection<KeyLevelPair>();

        [XmlElement]
        public ObservableCollection<KeyLevelPair> KitFeatureKeyLevelPairs
        {
            get
            {
                return _kitFeatureKeyLevelPairs;
            }
            set
            {
                _kitFeatureKeyLevelPairs = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public ObservableCollection<KitFeature> KitFeatures
        {
            get
            {
                ObservableCollection<KitFeature> kitFeatures = new ObservableCollection<KitFeature>();
                foreach (KeyLevelPair pair in KitFeatureKeyLevelPairs)
                {
                    kitFeatures.Add(pair.KitFeature);
                }

                return kitFeatures;
            }
        }

        public void UnEquipItem(EquippableItem item)
        {
            try
            {
                switch (item.EquipLocation)
                {
                    case ItemType.Armor:
                        if (Armor.Key == item.Key) Armor = null;
                        break;
                    case ItemType.Back:
                        if (Back.Key == item.Key) Back = null;
                        break;
                    case ItemType.Feet:
                        if (Feet.Key == item.Key) Feet = null;
                        break;
                    case ItemType.Finger:
                        if (LeftFinger.Key == item.Key) { LeftFinger = null; }
                        if (RightFinger.Key == item.Key) { RightFinger = null; }
                        break;
                    case ItemType.Hands:
                        if (Hands.Key == item.Key) Hands = null;
                        break;
                    case ItemType.Head:
                        if (Head.Key == item.Key) Head = null;
                        break;
                    case ItemType.Legs:
                        if (Legs.Key == item.Key) Legs = null;
                        break;
                    case ItemType.Neck:
                        if (Neck.Key == item.Key) Neck = null;
                        break;
                    case ItemType.Shield:
                        if (WeaponOffHand.Key == item.Key) WeaponOffHand = null;
                        break;
                    case ItemType.Waist:
                        if (Waist.Key == item.Key) Waist = null;
                        break;
                    case ItemType.Wrist:
                        if (Wrists.Key == item.Key) Wrists = null;
                        break;
                    case ItemType.Weapon:
                        if (WeaponOffHand != null && WeaponOffHand.Key == item.Key) { WeaponOffHand = null; }
                        if (WeaponMainHand != null && WeaponMainHand.Key == item.Key) { WeaponMainHand = null; }
                        break;
                    default:
                        break;
                }

                NotifyPropertyChanged("Actions");
            }
            catch
            {
                return;
            }
        }

        public void EquipItem(EquippableItem item)
        {
            try
            {
                if (item is Armor)
                {
                    if (Stats.Str < (item as Armor).RequiredStrength) { return; }

                    Armor = item as Armor;
                }
                else if (item is Weapon)
                {
                    if (item is Shield)
                    {
                        if (WeaponMainHand != null && WeaponMainHand.IsTwoHanded())
                        {
                            WeaponMainHand = null;
                        }
                        WeaponOffHand = item as Shield;
                    }
                    else if ((item as Weapon).IsTwoHanded())
                    {
                        WeaponMainHand = item as Weapon;
                        WeaponOffHand = null;
                    }
                    else
                    {
                        WeaponOffHand = WeaponMainHand;
                        WeaponMainHand = item as Weapon;
                    }
                }
                else if (item.EquipLocation == ItemType.Head)
                {
                    Head = item;
                }
                else if (item.EquipLocation == ItemType.Back)
                {
                    Back = item;
                }
                else if (item.EquipLocation == ItemType.Feet)
                {
                    Feet = item;
                }
                else if (item.EquipLocation == ItemType.Hands)
                {
                    Hands = item;
                }
                else if (item.EquipLocation == ItemType.Legs)
                {
                    Legs = item;
                }
                else if (item.EquipLocation == ItemType.Neck)
                {
                    Neck = item;
                }
                else if (item.EquipLocation == ItemType.Waist)
                {
                    Waist = item;
                }
                else if (item.EquipLocation == ItemType.Wrist)
                {
                    Wrists = item;
                }
                else if (item.EquipLocation == ItemType.Finger)
                {
                    if ( (LeftFinger != null && LeftFinger.Key == item.Key)
                        || (RightFinger != null && RightFinger.Key == item.Key) ) { return; }

                    RightFinger = LeftFinger;
                    LeftFinger = item;
                }

                NotifyPropertyChanged("Actions");
            }
            catch
            {
                return;
            }
        }

        [XmlIgnore]
        private ObservableCollection<HpRoll> _hPRolls = new ObservableCollection<HpRoll>();

        [XmlElement]
        public ObservableCollection<HpRoll> HPRolls
        {
            get
            {
                foreach (HpRoll roll in _hPRolls)
                {
                    if (!roll.PropertyChangedSet)
                    {
                        roll.PropertyChanged += roll_PropertyChanged;
                        roll.PropertyChangedSet = true;
                        roll.IsActive = roll.GainedAtlevel <= Level;
                    }
                }

                return _hPRolls;
            }
            set
            {
                _hPRolls = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("HP");
                NotifyPropertyChanged("MaxHP");
            }
        }

        void roll_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateAllCalculatedFields();
        }

        public void ChangeLevel(int newLevel)
        {
            if (newLevel > Level)
            {
                while (Level < newLevel)
                {
                    LevelUp();
                }
            }
            else
            {
                while (Level > newLevel)
                {
                    LevelDown();
                }
            }
        }

        [XmlIgnore]
        public override int MaxHP
        {
            get
            {
                int maxhp = 0;

                if (HPRolls == null || HPRolls.Count == 0)
                {
                    maxhp = 1;
                }
                else
                {
                    maxhp = 0;
                    foreach (HpRoll roll in HPRolls)
                    {
                        maxhp += roll.Value;
                    }

                    maxhp += Stats.ConModifier * Level;
                }

                if (maxhp < 1)
                {
                    maxhp = 1;
                }

                return maxhp;
            }
            set
            {
                base.MaxHP = value;
            }
        }



        public void LevelUp()
        {
            if (Level == 0)
            {
                HPRolls = new ObservableCollection<HpRoll>();
            }

            Level++;
            HitDice++;

            int roll = Dice.Roll(KitTemplate.HPDie);
            HPRolls.Add(new HpRoll(roll, Level));
            Heal(roll);

            AddKitFeatures();
            UpdateAllCalculatedFields();
            NotifyPropertyChanged("KitFeatures");
        }

        public void LevelDown()
        {
            if (Level == 0) { return; }

            RemoveKitFeatures();

            Level--;
            HitDice--;

            HPRolls.RemoveAt(HPRolls.Count - 1);
            UpdateAllCalculatedFields();
            NotifyPropertyChanged("KitFeatures");
        }

        private void AddKitFeatures()
        {
            if (KitTemplate == null)
            {
                return;
            }

            List<KitFeature> features = KitTemplate.GetFeaturesAtLevel(Level);

            foreach (KitFeature feature in features)
            {
                KeyLevelPair pair = new KeyLevelPair();
                pair.GainedAtLevel = Level;
                pair.Key = feature.Key;
                KitFeatureKeyLevelPairs.Add(pair);
            }
        }

        private void RemoveKitFeatures()
        {
            if (KitFeatureKeyLevelPairs == null)
            {
                return;
            }

            int start = KitFeatureKeyLevelPairs.Count - 1;
            for (int i = start; i >= 0; i--)
            {
                if (KitFeatureKeyLevelPairs[i].GainedAtLevel == Level)
                {
                    KitFeatureKeyLevelPairs.RemoveAt(i);
                }
            }
        }

        public override void SpendHitDice(int dice)
        {
            for (int i = 0; i < dice; i++)
            {
                if (HitDice == 0) { return; }

                HitDice--;

                int roll = Dice.Roll(KitTemplate.HPDie);
                Heal(roll);
            }
        }

        [XmlIgnore]
        private Weapon _mainHand;

        [XmlElement("WeaponMainHand")]
        public Weapon WeaponMainHand
        {
            get
            {
                return _mainHand;
            }
            set
            {
                _mainHand = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _leftFinger;

        [XmlElement]
        public EquippableItem LeftFinger
        {
            get
            {
                return _leftFinger;
            }
            set
            {
                _leftFinger = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _rightFinger;

        [XmlElement]
        public EquippableItem RightFinger
        {
            get
            {
                return _rightFinger;
            }
            set
            {
                _rightFinger = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _back;

        [XmlElement]
        public EquippableItem Back
        {
            get
            {
                return _back;
            }
            set
            {
                _back = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _hands;

        [XmlElement]
        public EquippableItem Hands
        {
            get
            {
                return _hands;
            }
            set
            {
                _hands = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _wrists;

        [XmlElement]
        public EquippableItem Wrists
        {
            get
            {
                return _wrists;
            }
            set
            {
                _wrists = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _waist;

        [XmlElement]
        public EquippableItem Waist
        {
            get
            {
                return _waist;
            }
            set
            {
                _waist = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _shoulders;

        [XmlElement]
        public EquippableItem Shoulders
        {
            get
            {
                return _shoulders;
            }
            set
            {
                _shoulders = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _neck;

        [XmlElement]
        public EquippableItem Neck
        {
            get
            {
                return _neck;
            }
            set
            {
                _neck = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _legs;

        [XmlElement]
        public EquippableItem Legs
        {
            get
            {
                return _legs;
            }
            set
            {
                _legs = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _feet;

        [XmlElement]
        public EquippableItem Feet
        {
            get
            {
                return _feet;
            }
            set
            {
                _feet = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private EquippableItem _head;

        [XmlElement]
        public EquippableItem Head
        {
            get
            {
                return _head;
            }
            set
            {
                _head = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Weapon _offHand;

        [XmlElement("WeaponOffHand")]
        public Weapon WeaponOffHand
        {
            get
            {
                return _offHand;
            }
            set
            {
                _offHand = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("ShieldBonus");
                NotifyPropertyChanged("AC");
            }
        }

        [XmlIgnore]
        public int ShieldBonus
        {
            get
            {
                if (WeaponOffHand is Shield)
                {
                    return 2;
                }

                return 0;
            }
        }

        [XmlElement]
        public bool Active;

        [XmlIgnore]
        private Armor _armor;

        [XmlElement("EquippedArmor")]
        public Armor Armor
        {
            get
            {
                return _armor;
            }
            set
            {
                _armor = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("AC");
            }
        }

        [XmlIgnore]
        private Armor _shield;

        [XmlElement]
        public Armor Shield
        {
            get
            {
                return _shield;
            }
            set
            {
                _shield = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public override int AC 
        {
            get
            {
                int ac = base.AC;

                if (Armor == null)
                {
                    ac += Stats.DexModifier;
                }
                else
                {
                    ac = Armor.BaseAC;

                    if (Armor.Type == ArmorType.Light)
                    {
                        ac += Stats.DexModifier;
                    }
                    else if (Armor.Type == ArmorType.Medium)
                    {
                        ac += (Stats.DexModifier < 2) ? Stats.DexModifier : 2;
                    }
                }

                ac += ShieldBonus;

                foreach (KitFeature feature in KitFeatures)
                {
                    ac += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.AC);
                }

                return ac;
            }
        }

        public override AOrD HasAdvantageOnAttack
        {
            get 
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.HasAdvantageOnMainHandAttack(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        public override AOrD OpponentHasAdvantageOnAttackAgainstYou
        {
            get
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.OpponentHasAdvantageOnAttackAgainstYou(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        [XmlIgnore]
        public override int ProficiencyBonus
        {
            get
            {
                return 2 + (Level - 1) / 4;
            }
        }

        [XmlElement]
        public override int MiscACModifier
        {
            get
            {
                int ac = base.MiscACModifier;

                foreach (KitFeature feature in KitFeatures)
                {
                    ac += feature.AdditionalAC(this);
                }

                return ac;
            }
            set
            {
                base.MiscACModifier = value;
            }
        }

        [XmlIgnore]
        public virtual int MainHandAttackBonus
        {
            get
            {
                int value = ProficiencyBonus;

                foreach (KitFeature feature in KitFeatures)
                {
                    value += feature.AdditionalMainHandAttackBonus(this);
                }

                return value;
            }
        }

        [XmlIgnore]
        public string MainHandDamageDisplay
        {
            get
            {
                return WeaponDamageDisplay(WeaponMainHand, MainHandDamageModifier);

            }
        }

        private string WeaponDamageDisplay(Weapon weapon, int damageModifier)
        {
            string display;

            int damageDie = 2;
            int timesToRoll = 1;
            if (weapon != null)
            {
                damageDie = weapon.DamageDie;
                timesToRoll = weapon.TimesToRoll;
            }

            if (damageModifier > 0)
            {
                display = String.Format("{0}d{1} + {2}", timesToRoll, damageDie, damageModifier);
            }
            else if (damageModifier < 0)
            {
                display = String.Format("{0}d{1} - {2}", timesToRoll, damageDie, -damageModifier);
            }
            else
            {
                display = String.Format("{0}d{1}", timesToRoll, damageDie);
            }

            return display;
        }

        [XmlIgnore]
        public virtual int OffHandAttackBonus
        {
            get
            {
                int value = ProficiencyBonus;

                foreach (KitFeature feature in KitFeatures)
                {
                    value += feature.AdditionalOffHandAttackBonus(this);
                }

                return value;
            }
        }

        [XmlIgnore]
        public string OffHandDamageDisplay
        {
            get
            {
                return WeaponDamageDisplay(WeaponOffHand, OffHandDamageModifier);
            }
        }

        [XmlIgnore]
        public int MainHandDamageModifier
        {
            get
            {
                int dam = 0;

                if (WeaponMainHand != null)
                {
                    dam = WeaponMainHand.AddedDamage;

                    if (WeaponMainHand.WeapType == WeaponType.MartialMeleeWeapon || WeaponMainHand.WeapType == WeaponType.SimpleMeleeWeapon)
                    {
                        dam += Stats.StrModifier;
                    }
                    else
                    {
                        dam += Stats.DexModifier;
                    }
                }
                else
                {
                    dam += Stats.StrModifier;
                }

                return dam;
            }
        }

        [XmlIgnore]
        public int OffHandDamageModifier
        {
            get
            {
                int dam = 0;

                if (WeaponOffHand != null)
                {
                    dam = WeaponOffHand.AddedDamage;

                    if (WeaponOffHand.WeapType == WeaponType.MartialMeleeWeapon || WeaponOffHand.WeapType == WeaponType.SimpleMeleeWeapon)
                    {
                        dam += Stats.StrModifier;
                    }
                    else
                    {
                        dam += Stats.DexModifier;
                    }
                }

                return dam;
            }
        }

        public override int CriticalHitDamageRolls(Weapon weapon)
        {
            int num = base.CriticalHitDamageRolls(weapon);

            foreach (KitFeature feature in KitFeatures)
            {
                num += feature.AdditionalCriticalHitDamageRolls(this);
            }

            return num;
        }

        public override int GetSave(AttributeEnum attribute)
        {
            int save = base.GetSave(attribute);

            if (KitTemplate.MajorStat1 == attribute ||
                KitTemplate.MajorStat2 == attribute)
            {
                save += ProficiencyBonus;
            }

            foreach (KitFeature feature in KitFeatures)
            {
                switch (attribute) {
                    case AttributeEnum.Str:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.StrSave);
                        break;
                    case AttributeEnum.Dex:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.DexSave);
                        break;
                    case AttributeEnum.Con:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.ConSave);
                        break;
                    case AttributeEnum.Int:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.IntSave);
                        break;
                    case AttributeEnum.Wis:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.WisSave);
                        break;
                    case AttributeEnum.Cha:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.ChaSave);
                        break;
                }
            }

            return save;
        }

        public override AOrD GetSaveAOrD(AttributeEnum attribute)
        {
            int save = 0;

            foreach (KitFeature feature in KitFeatures)
            {
                switch (attribute)
                {
                    case AttributeEnum.Str:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.StrSave);
                        break;
                    case AttributeEnum.Dex:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.DexSave);
                        break;
                    case AttributeEnum.Con:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.ConSave);
                        break;
                    case AttributeEnum.Int:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.IntSave);
                        break;
                    case AttributeEnum.Wis:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.WisSave);
                        break;
                    case AttributeEnum.Cha:
                        save += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.ChaSave);
                        break;
                }
            }

            return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(save);
        }

        [XmlIgnore]
        public List<ArmorType> ArmorProficiencies
        {
            get
            {
                List<ArmorType> armorTypes = new List<ArmorType>();
                foreach (Proficiency p in Proficiencies)
                {
                    if (p is ArmorProficiency)
                    {
                        armorTypes.Add((p as ArmorProficiency).Type);
                    }
                }

                return armorTypes;
            }
        }

        public override void Damage(int damage)
        {
            base.Damage(damage);

            foreach (KitFeature feature in KitFeatures)
            {
                feature.Damage(this);
            }
        }

        [XmlIgnore]
        public List<WeaponProficiency> WeaponProficiencies
        {
            get
            {
                List<WeaponProficiency> weaponProficiencies = new List<WeaponProficiency>();
                foreach (Proficiency p in Proficiencies)
                {
                    if (p is WeaponProficiency)
                    {
                        weaponProficiencies.Add((p as WeaponProficiency));
                    }
                }

                return weaponProficiencies;
            }
        }

        public override int GetStat(AttributeEnum stat)
        {
            int val = base.GetModifier(stat);

            foreach (KitFeature feature in KitFeatures)
            {
                val += feature.AdditionalAttributes(this, stat);
            }

            return val;
        }

        public override int NumberOfAttacksPerAttackAction
        {
            get
            {
                int val = base.NumberOfAttacksPerAttackAction;

                foreach (KitFeature feature in KitFeatures)
                {
                    val += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.NumberOfAttacks);
                }

                return val;
            }
        }
        
        [XmlIgnore]
        public override ObservableCollection<MyAction> Actions
        {
            get
            {
                var actions = base.Actions;

                List<AttackAction> attackActions = CalculateAttackActions();

                foreach (AttackAction action in attackActions)
                {
                    actions.Add(action);
                }

                foreach (KitFeature feature in KitFeatures)
                {
                    if (feature.HasAction)
                    {
                        if (String.IsNullOrEmpty(feature.Action.Name))
                        {
                            feature.Action.Name = feature.Name;
                        }

                        actions.Add(feature.Action);
                    }
                }

                return actions;
            }
        }

        private List<AttackAction> CalculateAttackActions()
        {
            List<AttackAction> attackActions = new List<AttackAction>();

            for (int i = 0; i < NumberOfAttacksPerAttackAction; i++)
            {
                AttackAction attackAction = new AttackAction();
                attackActions.Add(attackAction);

                attackAction.ChargesRequired = 0;
                attackAction.Duration = 0;
                attackAction.DurationTimeUnit = TimeUnit.Second;

                if (i == 0)
                {
                    attackAction.IsBonusAction = false;
                }
                else
                {
                    attackAction.IsBonusAction = true;
                }

                if (WeaponMainHand == null && WeaponOffHand == null)
                {
                    attackAction.Name = "Fist";
                    attackAction.AttackParameters.AttackBonus = MainHandAttackBonus;
                    attackAction.AttackParameters.AttackRollRequired = true;
                    attackAction.AttackParameters.DamageDice = 1;
                    attackAction.AttackParameters.DamageDie = 2;
                    attackAction.AttackParameters.DamageModifier = MainHandDamageModifier;
                    attackAction.AttackParameters.DamageType = DamageType.Bludgeoning;
                    attackAction.AttackParameters.InflictedConditions = new List<Condition>();
                    attackAction.AttackParameters.SaveAvailable = false;
                }

                if (WeaponMainHand != null)
                {
                    attackAction.Name = WeaponMainHand.Name;
                    attackAction.AttackParameters.AttackBonus = MainHandAttackBonus;
                    attackAction.AttackParameters.AttackRollRequired = true;
                    attackAction.AttackParameters.DamageDice = WeaponMainHand.TimesToRoll;
                    attackAction.AttackParameters.DamageDie = WeaponMainHand.DamageDie;
                    attackAction.AttackParameters.DamageModifier = MainHandDamageModifier;
                    attackAction.AttackParameters.DamageType = WeaponMainHand.DamageType;
                    attackAction.AttackParameters.InflictedConditions = new List<Condition>();
                    attackAction.AttackParameters.SaveAvailable = false;
                }
            }

            if (WeaponOffHand != null)
            {
                for (int i = 0; i < NumberOfAttacksPerAttackAction; i++)
                {
                    AttackAction attackAction = new AttackAction();
                    attackActions.Add(attackAction);

                    attackAction.ChargesRequired = 0;
                    attackAction.Duration = 0;
                    attackAction.DurationTimeUnit = TimeUnit.Second;
                    attackAction.IsBonusAction = true;

                    attackAction.Name = WeaponOffHand.Name;
                    attackAction.AttackParameters.AttackBonus = OffHandAttackBonus;
                    attackAction.AttackParameters.AttackRollRequired = true;
                    attackAction.AttackParameters.DamageDice = WeaponOffHand.TimesToRoll;
                    attackAction.AttackParameters.DamageDie = WeaponOffHand.DamageDie;
                    attackAction.AttackParameters.DamageModifier = OffHandDamageModifier;
                    attackAction.AttackParameters.DamageType = WeaponOffHand.DamageType;
                    attackAction.AttackParameters.InflictedConditions = new List<Condition>();
                    attackAction.AttackParameters.SaveAvailable = false;
                }
            }

            return attackActions;
        }

        [XmlIgnore]
        public override AOrD AOrDOnStrSave
        {
            get
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.AOrDOnStrSave(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        [XmlIgnore]
        public override AOrD AOrDOnDexSave
        {
            get
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.AOrDOnDexSave(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        [XmlIgnore]
        public override AOrD AOrDOnConSave
        {
            get
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.AOrDOnConSave(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        [XmlIgnore]
        public override AOrD AOrDOnIntSave
        {
            get
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.AOrDOnIntSave(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        [XmlIgnore]
        public override AOrD AOrDOnWisSave
        {
            get
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.AOrDOnWisSave(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        [XmlIgnore]
        public override AOrD AOrDOnChaSave
        {
            get
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.AOrDOnChaSave(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        [XmlIgnore]
        public override AOrD HasAdvantageOnInitiativeRoll
        {
            get
            {
                int balance = 0;
                foreach (KitFeature feature in KitFeatures)
                {
                    balance += EnumHelper.IsAdvantageDisadvantageOrNormal(feature.HasAdvantageOnInitiativeRoll(this));
                }

                return EnumHelper.GetAdvantageDisadvantageNormalFromBalance(balance);
            }
        }

        public override FloatDistance GroundSpeed
        {
            get
            {
                FloatDistance speed = new FloatDistance();
                speed.Value = Race.BaseGroundSpeed.Value;

                foreach (KitFeature feature in KitFeatures)
                {
                    speed.Value += feature.CombatantEffectEngine.GetCombatantAugmentation(this, GameEngine.Augmentation.AddSubtract, GameEngine.Metric.GroundSpeed);
                }

                foreach (KitFeature feature in KitFeatures)
                {
                    speed.Value = speed.Value * feature.MultiplyGroundSpeed(this);
                }

                return speed;
            }
        }

        public override bool CanProvokeAttackOfOpportunity
        {
            get
            {
                foreach (KitFeature feature in KitFeatures) 
                {
                    if (feature.IgnoreAttackOfOpportunity(this))
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}
