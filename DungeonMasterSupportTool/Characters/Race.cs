﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore;
using DMSTCore.Core;

namespace DMSTCore.Characters
{
    [Serializable]
    public class Race : NameDescription
    {
        public Race()
        {
            Name = "Human";
            AdditionalStats = new Attributes();
            BaseGroundSpeed = new FloatDistance();
        }

        [XmlIgnore]
        private Attributes _additionalStats;

        [XmlElement]
        public Attributes AdditionalStats
        {
            get
            {
                return _additionalStats;
            }
            set
            {
                _additionalStats = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private SizeCategory _size;

        [XmlElement]
        public SizeCategory Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private FloatDistance _baseGroundSpeed;

        [XmlElement]
        public FloatDistance BaseGroundSpeed
        {
            get
            {
                return _baseGroundSpeed;
            }
            set
            {
                _baseGroundSpeed = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public List<RacialTrait> Traits = new List<RacialTrait>();

        [XmlElement]
        public List<NameDescription> Languages = new List<NameDescription>();
    }
}
