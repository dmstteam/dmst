﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSTCore.Characters
{
    public class ArmorProficiency : Proficiency
    {
        public ArmorProficiency() { }

        public ArmorProficiency(ArmorType type) : base(type)
        {
            Type = type;
        }

        public ArmorType Type;
    }
}
