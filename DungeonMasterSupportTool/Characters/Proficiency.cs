﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Items;
using DMSTCore;

namespace DMSTCore.Characters
{
    [XmlInclude(typeof(ArmorProficiency))]
    [XmlInclude(typeof(WeaponProficiency))]
    [Serializable]
    public class Proficiency : NameDescription
    {
        public Proficiency() { }

        public Proficiency(WeaponType type)
        {
            Name = type.ToString();
            Description = String.Format("Can add proficiency bonus when using this weapon.");
        }

        public Proficiency(ArmorType type)
        {

            Name = type.ToString();
            Name += " Armor";

            Description = String.Format("Using an armor of this type does not give disadvantage.");
        }

        public Proficiency(Item item)
        {
            if (item == null)
            {
                Name = "New Item Proficiency";
                Description = "Item Proficiency Description";

                return;
            }

            Name = item.Name;

            if (item is Weapon)
            {
                Description = String.Format("Can add proficiency bonus when using this weapon.");
            }
            else if (item is Armor)
            {
                Description = String.Format("Using an armor of this type does not give disadvantage.");
            }
            else
            {
                Description = String.Format("You are proficient with this item");
            }
        }
    }
}
