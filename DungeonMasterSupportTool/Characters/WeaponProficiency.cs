﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMSTCore.Items;
using System.Xml.Serialization;

namespace DMSTCore.Characters
{
    [Serializable]
    public class WeaponProficiency : Proficiency
    {
        public WeaponProficiency() { }

        public WeaponProficiency(WeaponType type)
            : base(type)
        {
            Type = type;
            Weapon = null;
        }

        public WeaponProficiency(Item item)
            : base(item)
        {
            Weapon = item as Weapon;
            Type = Weapon.WeapType;
        }

        [XmlElement]
        public WeaponType Type;

        [XmlElement]
        public Weapon Weapon;
    }
}
