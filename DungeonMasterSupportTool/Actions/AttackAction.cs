﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Characters;
using DMSTCore.Encounters;

namespace DMSTCore.Actions
{
    [Serializable]
    public class AttackAction : MyAction
    {
        public AttackAction()
        {
            IconPath = @"..\Images\SwordItemIcon.png";
        }

        [XmlIgnore]
        private AttackParameters _attackParameters = new AttackParameters();

        [XmlElement]
        public AttackParameters AttackParameters
        {
            get
            {
                return _attackParameters;
            }
            set
            {
                _attackParameters = value;
                NotifyPropertyChanged();
            }
        }

        public override void Execute(Combatant actionTaker)
        {
            base.Execute(actionTaker);

            RequestTargetEventArgs e = new RequestTargetEventArgs();

            e.PossibleTargets = ApplicationData.CurrentEncounter.TargetsInRange(actionTaker, null);
            e.NumberOfTargetsToSelect = 1;

            ApplicationData.Communication.TargetRequest(this, e);

            foreach (Combatant target in e.SelectedTargets)
            {
                if (AttackRoll(AttackParameters, target))
                {
                    DamageRoll(AttackParameters, target);
                }
            }
        }

        protected void DamageRoll(AttackParameters attackRoll, Combatant target)
        {
            attackRoll.DamageRoll = 0;
            for (int i = 0; i < attackRoll.DamageDice; i++)
            {
                attackRoll.DamageRoll += Dice.Roll(attackRoll.DamageDie);
            }

            attackRoll.DamageRoll += attackRoll.DamageModifier;

            target.Damage(attackRoll.DamageRoll);
        }

        protected bool AttackRoll(AttackParameters attackRoll, Combatant defender)
        {
            attackRoll.AttackRoll = Dice.RollD20();
            attackRoll.AttackRoll += attackRoll.AttackBonus;

            if (attackRoll.AttackRoll >= defender.AC)
            {
                return true;
            }

            return false;
        }
    }
}
