﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSTCore.Actions
{
    public class DashAction : MyAction
    {
        public DashAction() { }

        public DashAction(string name, string description) : base(name, description) { }
    }
}
