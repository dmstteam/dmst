﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

using DMSTCore.Characters;
using DMSTCore.Effects;

namespace DMSTCore.Actions
{
    [Serializable]
    [XmlInclude(typeof(AttackAction))]
    [XmlInclude(typeof(DashAction))]
    [XmlInclude(typeof(DisengageAction))]
    [XmlInclude(typeof(DodgeAction))]
    public class MyAction : CombatantEffect
    {
        public MyAction() { }

        public MyAction(string name, string description)
        {
            Name = name;
            Description = description;
        }

        [XmlElement]
        public int ChargesRequired { get; set; }

        [XmlIgnore]
        protected int RemainingCharges;

        [XmlElement]
        public bool IsBonusAction { get; set; }

        [XmlIgnore]
        public bool IsActive;

        [XmlElement]
        public int Duration { get; set; }

        [XmlElement]
        public TimeUnit DurationTimeUnit { get; set; }

        [XmlElement]
        public int RemainingTime;

        [XmlElement]
        public TimeUnit RemainingTimeTimeUnit;

        public virtual void Execute(Combatant actionTaker) 
        {

        }
    }
}
