﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Actions
{
    public enum DamageDistributionOption {RerollForEachTarget, DistributeDamageOfSingleRoll, SingleRollForAllTargets }

    public class MultipleTargetAttackAction : AttackAction
    {
        [XmlIgnore]
        public List<Combatant> Targets;

        [XmlElement]
        public DamageDistributionOption DamageDistribution;
    }
}
