﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSTCore.Actions
{
    public class DisengageAction : MyAction
    {
        public DisengageAction() { }

        public DisengageAction(string name, string description)
            : base(name, description)
        {

        }
    }
}
