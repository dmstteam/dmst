﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Characters;

namespace DMSTCore.Actions
{
    public class SingleTargetAttackAction : AttackAction
    {
        //public SingleTargetAttackAction() { }

        //public SingleTargetAttackAction(
        //    string name,
        //    string description,
        //    int numberOfAttack, 
        //    int damageDie, 
        //    int damageDice,
        //    int damageModifier,
        //    int attackBonus, 
        //    bool attackRollRequired, 
        //    List<Condition> conditionsInflicted, 
        //    bool saveAvailable, 
        //    AttributeEnum saveType) 
        //    : base(name, description, damageDie,damageDice, damageModifier, attackBonus, attackRollRequired, conditionsInflicted, saveAvailable, saveType)
        //{
        //    NumberOfAttacks = numberOfAttack;
        //}

        //[XmlElement]
        //public int NumberOfAttacks;

        //public override string Execute(Combatant actionTaker)
        //{
        //    if (targets == null || targets.Count != 1)
        //    {
        //        return "No attacks made, please pass in 1 target.";
        //    }

        //    Combatant target = targets[0];

        //    string summary = base.Execute(actionTaker, targets);

        //    int attackRoll;
        //    int damageRoll;
        //    bool hit = true;

        //    for (int i = 0; i < NumberOfAttacks; i++)
        //    {
        //        summary += String.Format("{0}: ", i);

        //        if (AttackRollRequired)
        //        {
        //            hit = AttackRoll(target, out attackRoll);

        //            if (hit)
        //            {
        //                DamageRoll(target, out damageRoll);
        //                summary += String.Format("Attacked {0}, hit with roll of {1} vs. AC {2} for {3} damage.", target.Name, attackRoll, target.AC, damageRoll);
        //            }
        //            else
        //            {
        //                summary += String.Format("Attacked {0}, missed with roll of {1} vs. AC {2}.", target.Name, attackRoll, target.AC);
        //            }
        //        }
        //        else
        //        {
        //            DamageRoll(target, out damageRoll);
        //            summary += String.Format("Attacked {0} for {1} damage.", target.Name, damageRoll);
        //        }

        //        summary += "\r\n";
        //    }

        //    return summary;
        //}
    }
}
