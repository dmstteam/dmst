﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Actions
{
    [Serializable]
    public class AttackParameters : PropertyChangedNotifier
    {
        [XmlIgnore]
        private int _damageDie;

        [XmlElement]
        public int DamageDie
        {
            get
            {
                return _damageDie;
            }
            set
            {
                _damageDie = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _damageDice;

        [XmlElement]
        public int DamageDice
        {
            get
            {
                return _damageDice;
            }
            set
            {
                _damageDice = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _damageModifier;

        [XmlElement]
        public int DamageModifier
        {
            get
            {
                return _damageModifier;
            }
            set
            {
                _damageModifier = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private DamageType _damageType;

        [XmlElement]
        public DamageType DamageType
        {
            get
            {
                return _damageType;
            }
            set
            {
                _damageType = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        public int DamageRoll;

        [XmlIgnore]
        public int AttackRoll;

        [XmlIgnore]
        private int _attackBonus;

        [XmlIgnore]
        public int AttackBonus
        {
            get
            {
                return _attackBonus;
            }
            set
            {
                _attackBonus = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private bool _attackRollRequired;

        [XmlElement]
        public bool AttackRollRequired
        {
            get
            {
                return _attackRollRequired;
            }
            set
            {
                _attackRollRequired = true;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public List<Condition> InflictedConditions;

        [XmlIgnore]
        private bool _saveAvailable;

        [XmlElement]
        public bool SaveAvailable
        {
            get
            {
                return _saveAvailable;
            }
            set
            {
                _saveAvailable = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private AttributeEnum _saveType;

        [XmlElement]
        public AttributeEnum SaveType
        {
            get
            {
                return _saveType;
            }
            set
            {
                _saveType = value;
                NotifyPropertyChanged();
            }
        }
    }
}
