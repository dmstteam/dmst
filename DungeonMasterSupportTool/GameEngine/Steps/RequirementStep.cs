﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using DMSTCore.Characters;
using DMSTCore.GameEngine;

namespace DMSTCore.GameEngine
{
    [Serializable]
    public class RequirementStep : Step
    {
        public virtual int CheckRequirement(Character character)
        {
            bool success = true;

            if ((Requirement)RequirementType.SelectedIndex == Requirement.Armor)
            {
                int val = character.Armor == null ? 0 : (int)character.Armor.Type;
                success = CheckRequrement(val);
            }

            return success ? IfMetStepIndex.Value : IfNotMetStepIndex.Value;
        }

        public override string ToString()
        {
            string metText = IfMetStepIndex.Value == -1 ? "End" : IfMetStepIndex.Value.ToString();
            string notMetText = IfNotMetStepIndex.Value == -1 ? "End" : IfNotMetStepIndex.Value.ToString();

            return String.Format("{0} {1} {2} Met: {3}, Not Met: {4}", RequirementType.SelectedIndex, Logic.SelectedIndex, AmountRequired.Value, metText, notMetText);
        }

        private bool CheckRequrement(int n) 
        {
            Logic logic = (Logic)this.Logic.SelectedIndex;

            if (logic == GameEngine.Logic.LessThan)
            {
                return n < AmountRequired.Value;
            }
            else if (logic == GameEngine.Logic.Equals)
            {
                return n == AmountRequired.Value;
            }
            else if (logic == GameEngine.Logic.GreaterThan)
            {
                return n > AmountRequired.Value;
            }
            else if (logic == GameEngine.Logic.GreaterThanOrEqual)
            {
                return n >= AmountRequired.Value;
            }
            else if (logic == GameEngine.Logic.LessThanOrEqual)
            {
                return n <= AmountRequired.Value;
            }

            return false;
        }

        [XmlIgnore]
        public override ObservableCollection<StepProperty> Properties
        {
            get
            {
                ObservableCollection<StepProperty> properties = new ObservableCollection<StepProperty>();
                properties.Add(RequirementType);
                properties.Add(Logic);
                properties.Add(AmountRequired);
                properties.Add(IfMetStepIndex);
                properties.Add(IfNotMetStepIndex);

                return properties;
            }
        }

        [XmlIgnore]
        private StepProperty _logic = new StepProperty("Logic", typeof(Logic));

        [XmlElement]
        public StepProperty Logic
        {
            get
            {
                return _logic;
            }
            set
            {
                _logic = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StepProperty _amountRequired = new StepProperty("Amount Req.");

        [XmlElement]
        public StepProperty AmountRequired
        {
            get
            {
                return _amountRequired;
            }
            set
            {
                _amountRequired = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StepProperty _requirementType = new StepProperty("Req. Type", typeof(Requirement));

        [XmlElement]
        public StepProperty RequirementType
        {
            get
            {
                return _requirementType;
            }
            set
            {
                _requirementType = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StepProperty _ifMetStepIndex = new StepProperty("Met Index");

        [XmlElement]
        public StepProperty IfMetStepIndex
        {
            get
            {
                return _ifMetStepIndex;
            }
            set
            {
                _ifMetStepIndex = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StepProperty _ifNotMetStepIndex = new StepProperty("Not Met Index");

        [XmlElement]
        public StepProperty IfNotMetStepIndex
        {
            get
            {
                return _ifNotMetStepIndex;
            }
            set
            {
                _ifNotMetStepIndex = value;
                NotifyPropertyChanged();
            }
        }
    }
}
