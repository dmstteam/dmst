﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace DMSTCore.GameEngine
{
    [Serializable]
    [XmlInclude(typeof(AugmentationStep))]
    [XmlInclude(typeof(RequirementStep))]
    public abstract class Step : PropertyChangedNotifier
    {
        [XmlIgnore]
        public abstract ObservableCollection<StepProperty> Properties { get; }
    }
}
