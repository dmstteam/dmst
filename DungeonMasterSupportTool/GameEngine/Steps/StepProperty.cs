﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.GameEngine
{
    [Serializable]
    public class StepProperty : PropertyChangedNotifier
    {
        public StepProperty() { }

        public StepProperty(string name, Type enumType)
        {
            Name = name;
            IsEnum = true;
            IsText = false;
            EnumType = enumType.ToString();
        }

        public StepProperty(string name)
        {
            Name = name;
            IsEnum = false;
            IsText = true;
        }

        [XmlElement]
        public bool IsEnum { get; set; }

        [XmlElement]
        public bool IsText { get; set; }

        [XmlIgnore]
        private string _name;

        [XmlElement]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public string EnumType;

        [XmlIgnore]
        public Array Items
        {
            get
            {
                try
                {
                    var array = Type.GetType(EnumType).GetEnumValues();
                    return array;
                }
                catch
                {
                    return null;
                }
            }
        }

        [XmlIgnore]
        private int _value;

        [XmlElement]
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _selectedIndex;

        [XmlElement]
        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                _selectedIndex = value;
                NotifyPropertyChanged();
            }
        }
    }
}
