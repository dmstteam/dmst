﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace DMSTCore.GameEngine
{
    [Serializable]
    public class AugmentationStep : Step
    {
        public override string ToString()
        {
            string text = String.Format("{0}: {1} {2}", Metric.SelectedIndex, AugmentationType.SelectedIndex, Amount.Value);
            if (NextStepIndex.Value != -1)
            {
                text += String.Format(" Next: {3}", Metric.SelectedIndex, AugmentationType.SelectedIndex, Amount.Value, NextStepIndex.Value);
            }
            else
            {
                text += String.Format(" Next: End");
            }

            return text;
        }

        public int GetAmount(Characters.Character character)
        {
            if ((ReturnType)ReturnType.SelectedIndex == GameEngine.ReturnType.Number)
            {
                return Amount.Value;
            }
            else if ((ReturnType)ReturnType.SelectedIndex == GameEngine.ReturnType.StrMod)
            {
                return character.GetModifier(AttributeEnum.Str);
            }
            else if ((ReturnType)ReturnType.SelectedIndex == GameEngine.ReturnType.DexMod)
            {
                return character.GetModifier(AttributeEnum.Dex);
            }
            else if ((ReturnType)ReturnType.SelectedIndex == GameEngine.ReturnType.ConMod)
            {
                return character.GetModifier(AttributeEnum.Con);
            }
            else if ((ReturnType)ReturnType.SelectedIndex == GameEngine.ReturnType.IntMod)
            {
                return character.GetModifier(AttributeEnum.Int);
            }
            else if ((ReturnType)ReturnType.SelectedIndex == GameEngine.ReturnType.WisMod)
            {
                return character.GetModifier(AttributeEnum.Wis);
            }
            else if ((ReturnType)ReturnType.SelectedIndex == GameEngine.ReturnType.ChaMod)
            {
                return character.GetModifier(AttributeEnum.Cha);
            }
            else if ((ReturnType)ReturnType.SelectedIndex == GameEngine.ReturnType.ProficiencyBonus)
            {
                return character.ProficiencyBonus;
            }
            

            return 0;
        }

        [XmlIgnore]
        public override ObservableCollection<StepProperty> Properties
        {
            get
            {
                ObservableCollection<StepProperty> properties = new ObservableCollection<StepProperty>();
                properties.Add(Metric);
                properties.Add(AugmentationType);
                properties.Add(ReturnType);
                properties.Add(Amount);
                properties.Add(NextStepIndex);

                return properties;
            }
        }

        [XmlIgnore]
        private StepProperty _augmentationType = new StepProperty("Augmentation", typeof(Augmentation));

        [XmlElement]
        public StepProperty AugmentationType
        {
            get
            {
                return _augmentationType;
            }
            set
            {
                _augmentationType = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StepProperty _metric = new StepProperty("Metric", typeof(Metric));

        [XmlElement]
        public StepProperty Metric
        {
            get
            {
                return _metric;
            }
            set
            {
                _metric = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StepProperty _returnType = new StepProperty("Return Type", typeof(ReturnType));

        [XmlElement]
        public StepProperty ReturnType
        {
            get
            {
                return _returnType;
            }
            set
            {
                _returnType = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StepProperty _amount = new StepProperty("Amount");

        [XmlElement]
        public StepProperty Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private StepProperty _nextStepIndex = new StepProperty("Next index");

        [XmlElement]
        public StepProperty NextStepIndex
        {
            get
            {
                return _nextStepIndex;
            }
            set
            {
                _nextStepIndex = value;
                NotifyPropertyChanged();
            }
        }
    }
}
