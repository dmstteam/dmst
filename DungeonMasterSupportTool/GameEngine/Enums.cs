﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMSTCore.GameEngine
{
    public enum Metric
    {
        Str,
        Dex,
        Con,
        Int,
        Wis,
        Cha,
        StrSave,
        DexSave,
        ConSave,
        IntSave,
        WisSave,
        ChaSave,
        StrMod,
        DexMod,
        ConMod,
        IntMod,
        WisMod,
        ChaMod,
        HPMax,
        AC,
        BA,
        Damage,
        GroundSpeed,
        FlySpeed,
        SwimSpeed,
        BurrowSpeed,
        NumberOfAttacks
    }

    public enum Requirement
    {
        Armor
    }

    public enum Augmentation
    {
        AddSubtract,
        MultiplyDivide,
        AOrD
    }

    public enum StepType
    {
        Augmentation,
        Requirement
    }

    public enum Logic
    {
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        Equals,
    }

    public enum ReturnType
    {
        Number,
        StrMod,
        ConMod,
        DexMod,
        IntMod,
        WisMod,
        ChaMod,
        ProficiencyBonus,
    }
}
