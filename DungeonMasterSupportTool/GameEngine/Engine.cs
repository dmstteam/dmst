﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using DMSTCore.Characters;

namespace DMSTCore.GameEngine
{
    [Serializable]
    public class Engine : PropertyChangedNotifier
    {
        public int GetCombatantAugmentation(Character character, Augmentation bonusType, Metric metric)
        {
            if (Steps.Count == 0) { return 0; }
            if (!ContainsCorrectAugmentationStep(bonusType, metric)) { return 0; }

            int finalValue = 0;

            int nextIndex = 0;
            Step currentStep = Steps[nextIndex];
            while (nextIndex != -1)
            {
                if (currentStep is RequirementStep)
                {
                    nextIndex = (currentStep as RequirementStep).CheckRequirement(character);
                } 
                else if (currentStep is AugmentationStep)
                {
                    finalValue += (currentStep as AugmentationStep).GetAmount(character);
                    nextIndex = (currentStep as AugmentationStep).NextStepIndex.Value;
                }

                if (nextIndex == -1 || nextIndex >= Steps.Count)
                {
                    break;
                }

                currentStep = Steps[nextIndex];
            }

            return finalValue;
        }

        private bool ContainsCorrectAugmentationStep(Augmentation bonusType, Metric metric)
        {
            foreach (Step step in Steps)
            {
                if (step is AugmentationStep)
                {
                    if ((Metric)(step as AugmentationStep).Metric.SelectedIndex == metric && (Augmentation)(step as AugmentationStep).AugmentationType.SelectedIndex == bonusType)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        
        [XmlIgnore]
        private ObservableCollection<Step> _steps = new ObservableCollection<Step>();

        [XmlElement]
        public ObservableCollection<Step> Steps
        {
            get
            {
                return _steps;
            }
            set
            {
                _steps = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Step _currentStep;

        [XmlIgnore]
        public Step CurrentStep
        {
            get
            {
                return _currentStep;
            }
            set
            {
                _currentStep = value;
                NotifyPropertyChanged();
            }
        }
    }
}
