﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Characters;

namespace DMSTCore.EffectRequirements
{
    [Serializable]
    public class ArmorRequirement : EffectRequirement
    {
        public ArmorRequirement()
        {
            BestArmorAllowed = ArmorType.Heavy;
            WorstArmorAllowed = ArmorType.None;
        }

        public override string Name
        {
            get
            {
                return String.Format("Armor Requirement: {0}", BestArmorAllowed);
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private ArmorType _bestArmorAllowed = new ArmorType();

        [XmlElement]
        public ArmorType BestArmorAllowed
        {
            get
            {
                return _bestArmorAllowed;
            }
            set
            {
                _bestArmorAllowed = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private ArmorType _worstArmorAllowed = new ArmorType();

        [XmlElement]
        public ArmorType WorstArmorAllowed
        {
            get
            {
                return _worstArmorAllowed;
            }
            set
            {
                _worstArmorAllowed = value;
                NotifyPropertyChanged();
            }
        }

        public override bool MetRequirement(Character character)
        {
            if (character.Armor == null)
            {
                if (_bestArmorAllowed == ArmorType.None)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (character.Armor.Type <= _bestArmorAllowed)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
