﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.EffectRequirements
{
    [Serializable]
    public class HPRequirement : EffectRequirement
    {
        public override string Name
        {
            get
            {
                if (GreaterThan)
                {
                    return String.Format("HP > {0}", HP);
                } 
                else
                {
                    return String.Format("HP < {0}", HP);
                }
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private int _hP;

        [XmlElement]
        public int HP
        {
            get
            {
                return _hP;
            }
            set
            {
                _hP = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private bool _greaterThan;

        [XmlElement]
        public bool GreaterThan
        {
            get
            {
                return _greaterThan;
            }
            set
            {
                _greaterThan = value;
                NotifyPropertyChanged();
            }
        }

        public override bool MetRequirement(Characters.Character character)
        {
            if (GreaterThan) 
            {
                return character.HP > HP;
            }
            else 
            {
                return character.HP < HP;
            }
        }
    }
}
