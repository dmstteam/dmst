﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Characters;

namespace DMSTCore.EffectRequirements
{
    [Serializable]
    [XmlInclude(typeof(ArmorRequirement))]
    [XmlInclude(typeof(ConditionRequirement))]
    [XmlInclude(typeof(ActiveActionRequirement))]
    [XmlInclude(typeof(SaveRequirement))]
    [XmlInclude(typeof(HPRequirement))]
    [XmlInclude(typeof(WeaponRequirement))]
    public class EffectRequirement : NameDescription
    {
        public virtual bool MetRequirement(Character character)
        {
            return true;
        }
    }
}
