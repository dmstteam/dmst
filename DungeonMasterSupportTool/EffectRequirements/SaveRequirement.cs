﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.EffectRequirements
{
    [Serializable]
    public class SaveRequirement : EffectRequirement
    {
        public SaveRequirement()
        {
            _currentDifficultyCheck = BaseDifficultyCheck;
        }

        public override string Name
        {
            get
            {
                return String.Format("{0} Save: {1}", Stat, BaseDifficultyCheck);
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private AOrD _aOrD;

        [XmlElement]
        public AOrD AOrD
        {
            get
            {
                return _aOrD;
            }
            set
            {
                _aOrD = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private AttributeEnum _stat;

        [XmlElement]
        public AttributeEnum Stat
        {
            get
            {
                return _stat;
            }
            set
            {
                _stat = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _difficultyCheckIncrease;

        [XmlElement]
        public int DifficultyCheckIncrease
        {
            get
            {
                return _difficultyCheckIncrease;
            }
            set
            {
                _difficultyCheckIncrease = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _baseDifficultyCheck;

        [XmlElement]
        public int BaseDifficultyCheck
        {
            get
            {
                return _baseDifficultyCheck;
            }
            set
            {
                _baseDifficultyCheck = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _currentDifficultyCheck;

        public override bool MetRequirement(Characters.Character character)
        {
            int roll;
            bool success = character.RollSavingThrow(AOrD, Stat, _currentDifficultyCheck, out roll);

            _currentDifficultyCheck += DifficultyCheckIncrease;

            return success;
        }
    }
}
