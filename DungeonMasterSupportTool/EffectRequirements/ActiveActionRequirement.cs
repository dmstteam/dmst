﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Effects;
using DMSTCore.Kits;

namespace DMSTCore.EffectRequirements
{
    [Serializable]
    public class ActiveActionRequirement : EffectRequirement
    {
        public override string Name
        {
            get
            {
                return String.Format("Active: {0}", ActiveAction);
            }
            set
            {
                base.Name = value;
            }
        }

        public override bool MetRequirement(Characters.Character character)
        {
            foreach (KitFeature feature in character.KitFeatures)
            {
                if (feature.Action.Name == ActiveAction && feature.Action.IsActive)
                {
                    return true;
                }
            }

            return false;
        }

        [XmlIgnore]
        private string _activeAction;

        [XmlElement]
        public string ActiveAction 
        {
            get 
            {
                return _activeAction;
            }  
            set 
            {
                _activeAction = value;
                NotifyPropertyChanged();
            }
        }
        
    }
}
