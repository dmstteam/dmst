﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Characters;

namespace DMSTCore.EffectRequirements
{
    [Serializable]
    public class ConditionRequirement : EffectRequirement
    {
        public override string Name
        {
            get
            {
                if (ShouldHave)
                {
                    return String.Format("Must Be {0}", EnumHelper.ToString<Condition>(Condition));
                }
                else
                {
                    return String.Format("Not {0}", EnumHelper.ToString<Condition>(Condition));
                }
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlIgnore]
        private Condition _condition;

        [XmlElement]
        public Condition Condition
        {
            get
            {
                return _condition;
            }
            set
            {
                _condition = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private bool _shouldHave;

        [XmlElement]
        public bool ShouldHave
        {
            get
            {
                return _shouldHave;
            }
            set
            {
                _shouldHave = value;
                NotifyPropertyChanged();
            }
        }

        public override bool MetRequirement(Character character)
        {
            foreach (Condition condition in character.Conditions)
            {
                if (condition == Condition)
                {
                    if (ShouldHave)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            if (ShouldHave)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
