﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using DMSTCore.Items;

namespace DMSTCore.EffectRequirements
{
    [Serializable]
    public class WeaponRequirement : EffectRequirement
    {
        public override string Name
        {
            get
            {
                if (NoWeapon)
                {
                    return "No Weapon";
                }
                else
                {
                    return "Weapon Requirement";
                }
            }
            set
            {
                base.Name = value;
            }
        }

        [XmlElement]
        public bool NoWeapon { get; set; }

        [XmlElement]
        public List<WeaponType> Types { get; set; }

        [XmlElement]
        public List<string> SpecificWeapons { get; set; }

        public override bool MetRequirement(Characters.Character character)
        {
            bool mainHand = checkWeapon(character.WeaponMainHand);
            bool offHand = checkWeapon(character.WeaponOffHand);

            return mainHand;
        }

        private bool checkWeapon(Weapon weapon)
        {
            if (weapon == null)
            {
                if (NoWeapon)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            foreach (WeaponType type in Types)
            {
                if (weapon.WeapType == type)
                {
                    return true;
                }
            }

            foreach (String s in SpecificWeapons)
            {
                if (weapon.Name == s)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
