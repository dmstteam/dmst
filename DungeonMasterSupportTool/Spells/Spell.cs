﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Kits;

namespace DMSTCore.Spells
{
    [Serializable]
    public class Spell : NameDescription
    {
        public Spell()
        {
            Name = "New Spell";

            Verbal = false;
            Somatic = false;
            Material = false;
        }

        [XmlIgnore]
        private string _atHigherLevelDescription;

        [XmlElement]
        public string AtHigherLevelDescription
        {
            get
            {
                return _atHigherLevelDescription;
            }
            set
            {
                _atHigherLevelDescription = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public int SpellLevel;

        [XmlElement]
        public MagicSchool MagicSchool;

        [XmlIgnore]
        private int _range;

        [XmlElement]
        public int Range
        {
            get
            {
                return _range;
            }
            set
            {
                _range = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private DistanceUnit _rangeUnit;

        [XmlElement]
        public DistanceUnit RangeUnit
        {
            get
            {
                return _rangeUnit;
            }
            set
            {
                _rangeUnit = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public Shape Shape;

        [XmlElement]
        public int ShapeSize;

        [XmlElement]
        public DistanceUnit ShapeSizeUnit;

        [XmlElement]
        public bool Verbal;

        [XmlElement]
        public bool Somatic;

        [XmlElement]
        public bool Material;

        [XmlIgnore]
        private int _castingTime;

        [XmlElement]
        public int CastingTime
        {
            get
            {
                return _castingTime;
            }
            set
            {
                _castingTime = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private TimeUnit _castingTimeUnits;

        [XmlElement]
        public TimeUnit CastingTimeUnits
        {
            get
            {
                return _castingTimeUnits;
            }
            set
            {
                _castingTimeUnits = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _duration;

        [XmlElement]
        public int Duration
        {
            get
            {
                return _duration;
            }
            set
            {
                _duration = value;
                NotifyPropertyChanged();
            }
        }

        private TimeUnit _durationUnits;

        [XmlElement]
        public TimeUnit DurationUnits
        {
            get
            {
                return _durationUnits;
            }
            set
            {
                _durationUnits = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement]
        public bool ConcentrationRequired;

        [XmlElement]
        public List<KitTemplate> KitsThatHaveAccess;

        public override string ToString()
        {
            return Name;
        }
    }
}
