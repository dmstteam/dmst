﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using System.Xml.Serialization;
using DMSTCore.Characters;
using DMSTCore.Kits;

namespace DMSTCore.Spells
{
    [Serializable]
    public class SpellBook : PropertyChangedNotifier
    {
        public SpellBook()
        {
            Spells = new List<Spell>();
        }

        [XmlElement]
        public List<Spell> Spells;

        [XmlIgnore]
        private ObservableCollection<Spell> _sortedSpellList = new ObservableCollection<Spell>();

        [XmlIgnore]
        public ObservableCollection<Spell> SortedSpellList
        {
            get
            {
                return _sortedSpellList;
            }
            set
            {
                _sortedSpellList = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private Spell _currentSpell;

        [XmlIgnore]
        public Spell CurrentSpell
        {
            get
            {
                if (_currentSpell == null && SortedSpellList != null && SortedSpellList.Count > 0)
                {
                    _currentSpell = SortedSpellList[0];
                }

                return _currentSpell;
            }
            set
            {
                _currentSpell = value;
                NotifyPropertyChanged();
            }
        }

        public void AddSpell(Spell spell)
        {
            bool newSpell = true;
            for (int i = 0; i < Spells.Count; i++)
            {
                if (Spells[i].Name == spell.Name)
                {
                    newSpell = false;
                    Spells[i] = spell;
                    break;
                }
            }

            if (newSpell)
            {
                Spells.Add(spell);
            }
        }

        public Spell GetSpell(string name)
        {
            foreach (Spell s in Spells)
            {
                if (s.Name == name)
                {
                    return s;
                }
            }

            return null;
        }

        public List<Spell> GetSpellsOfSpecificLevel(int spellLevel)
        {
            List<Spell> spells = new List<Spell>();

            foreach (Spell spell in Spells)
            {
                if (spell.SpellLevel == spellLevel)
                {
                    spells.Add(spell);
                }
            }

            return spells;
        }

        public bool SpellKnown(Spell spell)
        {
            List<Spell> spells = GetSpellsOfSpecificLevel(spell.SpellLevel);

            foreach (Spell knownSpell in spells)
            {
                if (knownSpell.Name == spell.Name)
                {
                    return true;
                }
            }

            return false;
        }

        public List<Spell> GetSpellsFromNameList(List<string> spellNames)
        {
            List<Spell> spells = new List<Spell>();

            if (spellNames == null)
            {
                return spells;
            }

            foreach (string name in spellNames)
            {
                foreach (Spell spell in Spells)
                {
                    if (spell.Name == name)
                    {
                        spells.Add(spell);
                    }
                }
            }

            return spells;
        }

        public List<Spell> SortSpells(MagicSchool magicSchool, int spellLevel)
        {
            var spells = SpellSubset(Spells, spellLevel);
            spells = SpellSubset(spells, magicSchool);
            SetSortedListAndCurrentSpell(spells);
            return spells;
        }

        public List<Spell> SortSpells(Combatant combatant, MagicSchool magicSchool, int spellLevel)
        {
            var spells = SpellSubset(Spells, spellLevel);
            spells = SpellSubset(spells, magicSchool);

            if (combatant != null)
            {
                spells = SpellSubset(spells, combatant);
            }

            SetSortedListAndCurrentSpell(spells);
            return spells;
        }

        public List<Spell> SortSpells(MagicSchool magicSchool)
        {
            var spells = SpellSubset(Spells, magicSchool);
            SetSortedListAndCurrentSpell(spells);
            return spells;
        }

        public List<Spell> SortSpells(Combatant combatant, MagicSchool magicSchool)
        {
            var spells = SpellSubset(Spells, magicSchool);

            if (combatant != null)
            {
                spells = SpellSubset(spells, combatant);
            }

            SetSortedListAndCurrentSpell(spells);
            return spells;
        }

        public List<Spell> SortSpells(int spellLevel)
        {
            var spells = SpellSubset(Spells, spellLevel);
            SetSortedListAndCurrentSpell(spells);
            return spells;
        }

        public List<Spell> SortSpells(Combatant combatant, int spellLevel)
        {
            var spells = SpellSubset(Spells, spellLevel);

            if (combatant != null)
            {
                spells = SpellSubset(spells, combatant);
            }

            SetSortedListAndCurrentSpell(spells);
            return spells;
        }

        public List<Spell> SortSpells()
        {
            SetSortedListAndCurrentSpell(Spells);
            return Spells;
        }

        public List<Spell> SortSpells(Combatant combatant)
        {
            var spells = Spells;

            if (combatant != null)
            {
                spells = SpellSubset(Spells, combatant);
            }

            SetSortedListAndCurrentSpell(spells);
            return spells;
        }

        private void SetSortedListAndCurrentSpell(List<Spell> spells) 
        {
            SortedSpellList.Clear();
            foreach (Spell s in spells)
            {
                SortedSpellList.Add(s);
            }

            if (spells.Count > 0)
            {
                CurrentSpell = spells[0];
            }
        }

        private List<Spell> SpellSubset(List<Spell> initialSpellList, MagicSchool magicSchool)
        {
            List<Spell> spells = new List<Spell>();

            foreach (Spell s in initialSpellList)
            {
                if (s.MagicSchool == magicSchool)
                {
                    spells.Add(s);
                }
            }

            return spells;
        }

        private List<Spell> SpellSubset(List<Spell> initialSpellList, int spellLevel)
        {
            List<Spell> spells = new List<Spell>();

            foreach (Spell s in initialSpellList)
            {
                if (s.SpellLevel == spellLevel)
                {
                    spells.Add(s);
                }
            }

            return spells;
        }

        public List<Spell> SpellSubset(List<Spell> initialSpellList, Combatant combatant)
        {
            List<Spell> spells = new List<Spell>();

            foreach (Spell spell in initialSpellList)
            {
                foreach (KitTemplate kit in spell.KitsThatHaveAccess)
                {
                    if ( (combatant as Character).KitTemplate == kit)
                    {
                        spells.Add(spell);
                    }
                }
            }

            return spells;
        }

        public void RemoveSpell(Spell spellToRemove)
        {
            for(int i=0;i< Spells.Count;i++)
            {
                if (Spells[i].Name == spellToRemove.Name)
                {
                    Spells.RemoveAt(i);
                    return;
                }
            }
        }

        [XmlElement]
        public List<int> MaxSpellSlots;

        [XmlIgnore]
        private List<int> _currentSpellSlots;

        [XmlIgnore]
        public List<int> CurrentSpellSlots
        {
            get
            {
                if (_currentSpellSlots == null)
                {
                    _currentSpellSlots = MaxSpellSlots;
                }

                return _currentSpellSlots;
            }
        }
    }
}
