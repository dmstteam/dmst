﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Collections.ObjectModel;

namespace DMSTCore.Items
{
    [Serializable]
    public class Inventory : PropertyChangedNotifier
    {
        [XmlElement]
        public List<string> EquipmentKeys = new List<string>();

        [XmlIgnore]
        public ObservableCollection<Item> Equipment
        {
            get
            {
                ObservableCollection<Item> equipment = new ObservableCollection<Item>();
                foreach (string key in EquipmentKeys)
                {
                    equipment.Add(ApplicationData.Database.GetItemFromKey(key));
                }

                return equipment;
            }
        }

        public void AddEquipment(Item item)
        {
            if (item == null) { return; }

            EquipmentKeys.Add(item.Key);
            NotifyPropertyChanged("Equipment");
        }

        public void RemoveEquipment(Item item)
        {
            for(int i=0;i<EquipmentKeys.Count;i++)
            {
                if (item.Key == EquipmentKeys[i])
                {
                    EquipmentKeys.RemoveAt(i);
                    break;
                }
            }

            NotifyPropertyChanged("Equipment");
        }

        [XmlIgnore]
        private Purse _purse = new Purse();

        [XmlElement]
        public Purse Purse
        {
            get
            {
                return _purse;
            }
            set
            {
                _purse = value;
                NotifyPropertyChanged();
            }
        }
    }
}
