﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Items
{
    [XmlInclude(typeof(Armor))]
    [XmlInclude(typeof(Weapon))]
    [Serializable]
    public class EquippableItem : Item
    {
        [XmlElement]
        public virtual ItemType EquipLocation { get; set; }

        public override string IconPath
        {
            get
            {
                switch (EquipLocation)
                {
                    case ItemType.Back:
                        return @"..\Images\CapeItemIcon.png";
                    case ItemType.Feet:
                        return @"..\Images\BootsItemIcon.png";
                    case ItemType.Finger:
                        return @"..\Images\RingItemIcon.png";
                    case ItemType.Hands:
                        return @"..\Images\GloveItemIcon.png";
                    case ItemType.Head:
                        return @"..\Images\HelmetItemIcon.png";
                    case ItemType.Legs:
                        return @"..\Images\PantsItemIcon.png";
                    case ItemType.Neck:
                        return @"..\Images\NecklaceItemIcon.png";
                    case ItemType.Waist:
                        return @"..\Images\BeltItemIcon.png";
                    case ItemType.Wrist:
                        return @"..\Images\BracersItemIcon.png";
                    default:
                        return base.IconPath;
                }
            }
        }
    }
}
