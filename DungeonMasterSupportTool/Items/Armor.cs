﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Characters;

namespace DMSTCore.Items
{
    [Serializable]
    public class Armor : EquippableItem
    {
        public Armor()
            : base()
        {
            BaseAC = 0;
            Type = ArmorType.Light;
            RequiredStrength = 0;
            OnStealth = AOrD.Normal;
        }

        public override ItemType EquipLocation
        {
            get
            {
                return ItemType.Armor;
            }
        }

        public override string IconPath
        {
            get
            {
                return @"..\Images\ArmorItemIcon.png";
            }
        }

        [XmlIgnore]
        private int _baseAC;

        [XmlElement]
        public int BaseAC
        {
            get 
            { 
                return _baseAC; 
            }
            set
            {
                _baseAC = value;
                NotifyPropertyChanged();
                
            }
        }

        public override string ExtraInformation1
        {
            get
            {
                return String.Format("AC {0} ({1})", BaseAC, Type);
            }
        }

        public override string ExtraInformation2
        {
            get
            {
                return String.Format("Required Strength: {0}", RequiredStrength);
            }
        }

        public override string ExtraInformation3
        {
            get
            {
                string value = String.Empty;
                if (OnStealth == AOrD.Advantage)
                {
                    value = "Advantage On Stealth Checks";
                }
                else if (OnStealth == AOrD.Disadvantage)
                {
                    value = "Disadvantage On Stealth Checks";
                }

                return value;
            }
        }

        [XmlElement]
        public ArmorType Type { get; set; }

        [XmlElement]
        public int RequiredStrength { get; set; }

        [XmlElement]
        public AOrD OnStealth { get; set; }
    }
}
