﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Characters;

namespace DMSTCore.Items
{
    [XmlInclude(typeof(EquippableItem))]
    [Serializable]
    public class Item : NameDescription
    {
        public Item()
        {
            Name = "Item";
            Cost = 0;
            Weight = 0;
            Description = "none";
        }

        [XmlElement]
        public int Cost { get; set; }

        [XmlElement]
        public int Weight { get; set; }

        public override string ToString()
        {
            return Name;
        }

        public virtual string ExtraInformation1
        {
            get
            {
                return String.Empty;
            }
        }

        public virtual string ExtraInformation2
        {
            get
            {
                return String.Empty;
            }
        }

        public virtual string ExtraInformation3
        {
            get
            {
                return String.Empty;
            }
        }

        public virtual string ExtraInformation4
        {
            get
            {
                return String.Empty;
            }
        }

        public virtual string MicroDescriptionDisplay
        {
            get
            {
                return Name;
            }
        }
    }
}
