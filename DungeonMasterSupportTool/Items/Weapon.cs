﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using DMSTCore.Characters;

namespace DMSTCore.Items
{
    [XmlInclude(typeof(Shield))]
    [Serializable]
    public class Weapon : EquippableItem
    {
        public Weapon()
        {
            Name = "New Weapon";
            Description = "A very big and scary weapon";
            DamageDie = 6;
            DamageType = DMSTCore.DamageType.Slashing;
            AddedDamage = 1;
            TimesToRoll = 2;
            Properties = new List<WeaponProperty>();
        }

        public override ItemType EquipLocation
        {
            get
            {
                return ItemType.Weapon;
            }
        }

        public override string IconPath
        {
            get
            {
                return @"..\Images\SwordItemIcon.png";
            }
        }

        [XmlElement]
        public int DamageDie { get; set; }

        [XmlElement]
        public int TimesToRoll { get; set; }

        [XmlElement]
        public int AddedDamage { get; set; }

        [XmlIgnore]
        private int _range;

        [XmlElement]
        public int Range
        {
            get
            {
                return _range;
            }
            set
            {
                _range = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsTwoHanded()
        {
            foreach (WeaponProperty p in Properties) 
            {
                if (p == WeaponProperty.TwoHanded)
                {
                    return true;
                }
            }

            return false;
        }

        public override string ExtraInformation1
        {
            get
            {
                string diceString = Dice.FormatDiceString(TimesToRoll, DamageDie, AddedDamage);
                string s = String.Format("{0} ({1})", DamageType, diceString);

                return s;
            }
        }

        public override string ExtraInformation2
        {
            get
            {
                string value = "";
                for (int i = 0; i < Properties.Count; i++)
                {
                    if (i == 0)
                    {
                        value = Properties[i].ToString();
                    }
                    else
                    {
                        value = String.Format("{0}, {1}", value, Properties[i]);
                    }
                }
                return value;
            }
        }

        [XmlIgnore]
        private WeaponType _weapType;

        [XmlElement]
        public WeaponType WeapType
        {
            get
            {
                return _weapType;
            }
            set
            {
                _weapType = value;
                NotifyPropertyChanged();
            }
        }

        public string WeaponTypeAbbreviation
        {
            get
            {
                switch (WeapType)
                {
                    case WeaponType.SimpleMeleeWeapon :
                        return "SMW";
                    case WeaponType.SimpleRangedWeapon :
                        return "SRW";
                    case WeaponType.MartialMeleeWeapon :
                        return "MMW";
                    case WeaponType.MartialRangedWeapon :
                        return "MRW";
                    default :
                        return "Exotic";
                }
            }
        }

        [XmlElement]
        public DamageType DamageType { get; set; }

        [XmlElement]
        public List<WeaponProperty> Properties { get; set; }

        public override string ToString()
        {
            if (AddedDamage == 0)
            {
                return String.Format("{0} ({1}d{2})", Name, TimesToRoll, DamageDie);
            }
            else if(AddedDamage > 0)
            {
                return String.Format("{0} ({1}d{2} + {3})", Name, TimesToRoll, DamageDie,AddedDamage);
            }
            else
            {
                return String.Format("{0} ({1}d{2} - {3})", Name, TimesToRoll, DamageDie, -AddedDamage);
            }
        }
    }
}
