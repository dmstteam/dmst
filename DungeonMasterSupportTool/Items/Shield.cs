﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Items
{
    [Serializable]
    public class Shield : Weapon
    {
        public override string IconPath
        {
            get
            {
                return @"..\Images\ShieldItemIcon.png";
            }
        }

        public override ItemType EquipLocation
        {
            get
            {
                return ItemType.Shield;
            }
        }

        [XmlIgnore]
        private int _additionalAC;

        [XmlElement]
        public int AdditionalAC
        {
            get
            {
                return _additionalAC;
            }
            set
            {
                _additionalAC = value;
                NotifyPropertyChanged();

            }
        }

        public override string ExtraInformation1
        {
            get
            {
                return String.Format("AC +{0}", AdditionalAC);
            }
        }
    }
}
