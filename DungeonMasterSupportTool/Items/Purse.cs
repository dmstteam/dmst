﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DMSTCore.Items
{
    [Serializable]
    public class Purse : PropertyChangedNotifier
    {
        public Purse()
        {
            Copper = 80;
            Silver = 5;
            Gold = 40;
            Platinum = 0;
        }

        [XmlIgnore]
        private int _copper;

        [XmlElement]
        public int Copper
        {
            get
            {
                return _copper;
            }
            set
            {
                _copper = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _silver;

        [XmlElement]
        public int Silver
        {
            get
            {
                return _silver;
            }
            set
            {
                _silver = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _gold;

        [XmlElement]
        public int Gold
        {
            get
            {
                return _gold;
            }
            set
            {
                _gold = value;
                NotifyPropertyChanged();
            }
        }

        [XmlIgnore]
        private int _platinum;

        [XmlElement]
        public int Platinum
        {
            get
            {
                return _platinum;
            }
            set
            {
                _platinum = value;
                NotifyPropertyChanged();
            }
        }
    }
}
